/*
  Get arcive data of a CPT
 */

export default async function (context) {
  const { route } = context
  const data = context.app.router.app.getPageLogic(route)

  // Fetch posts on every every page
  if (!context.app.store.state.options) {
    await context.app.store.dispatch({ type: 'getOptions' })
  }

  if (!context.app.store.state.sitemap) {
    await context.app.store.dispatch({ type: 'getSitemap' })
  }

  if (!context.app.store.state.bearerToken.tokenid) {
    // await context.app.store.dispatch({ type: 'bearerToken/getToken' })
  }

  if (!context.app.store.state.bearerToken.events) {
    console.log('LJHLKFJEN')
    await context.app.store.dispatch({ type: 'bearerToken/getEvents' })
  }
  if (!context.app.store.state.bearerToken.coursetemplates) {
    console.log('LJHLKFJEN')
    await context.app.store.dispatch({ type: 'bearerToken/getCourseTemplates' })
    console.log('LJHLKFJEN')
  }

  if (context.route.name.includes('vara-tjanster')) {
    await context.app.store.dispatch({ type: 'cf7/getContactform', id: 7186 })
  }

  /* Pagebuilder */

  const page = context.app.store.state.pages[data.type + '/' + data.name]

  if (page && page.custom_fields && route.name.includes('vara-tjanster') && page.custom_fields.apply_options) {
    const formId = page.custom_fields.apply_options.form
    if (!context.app.store.state.cf7.cf[formId]) {
      await context.app.store.dispatch({ type: 'cf7/getContactform', id: formId })
    }
  }

  if (page && page.custom_fields && route.name.includes('kontakta-oss')) {
    const formId = page.custom_fields.form
    if (!context.app.store.state.cf7.cf[formId]) {
      await context.app.store.dispatch({ type: 'cf7/getContactform', id: formId })
    }
  }

  if (page && page.custom_fields && page.custom_fields.pagebuilder) {
    for (let i = 0; i < page.custom_fields.pagebuilder.length; i++) {
      const block = page.custom_fields.pagebuilder[i]

      if (block.acf_fc_layout === 'contact') {
        if (!context.app.store.state.cf7.cf[block.cf7_id]) {
          await context.app.store.dispatch({ type: 'cf7/getContactform', id: block.cf7_id })
        }
      }

      if (block.acf_fc_layout === 'course_featured') {
        if (!context.app.store.state.cpt.programme) {
          // await context.app.store.dispatch({ type: 'getCpt', postType: 'programme' })
        }

        if (!context.app.store.state.cpt.course) {
          // await context.app.store.dispatch({ type: 'getCpt', postType: 'course' })
        }
      }
    }
  }
}
