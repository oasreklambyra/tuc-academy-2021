export default async function (context) {
  const { store, redirect, route } = context

  if (!route.path.endsWith('/')) {
    let query = ''

    if (Object.keys(route.query).length > 0) {
      query = '?'
      query += Object.keys(route.query)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(route.query[key])}`)
        .join('&')
    }

    return redirect(route.path + '/' + query)
  }

  const meta = store.state.meta.meta[route.path]

  /* Format to json */
  const res = {
    meta: [],
    link: [],
    script: []
  }

  if (meta && meta.html) {
    const metaArr = meta.html.split('\n')

    metaArr.forEach((tag) => {
      if ((tag.includes('<meta') || tag.includes('<link')) && tag.includes('/>')) {
        let type = false

        if (tag.includes('meta')) {
          type = 'meta'
          tag = tag.replace('meta', '')
        }

        if (tag.includes('link')) {
          type = 'link'
          tag = tag.replace('link', '')
        }

        tag = tag.replace(/</g, '{')
        tag = tag.replace(/\/>/g, '}')
        tag = tag.replace(/=/g, '":')
        tag = tag.replace(/" /g, '","')
        tag = tag.replace(/","}/g, '"}')
        tag = tag.replace(/{ /g, '{"')

        res[type].push(JSON.parse(tag))
      }
    })

    /* Add hid */
    const resFormated = { ...context.app.head }

    resFormated.link = [
      { rel: 'icon', type: 'image/x-icon', href: '/static/favicon.jpg' },
      { rel: 'stylesheet', href: 'https://use.typekit.net/zve0qfl.css' }
    ]

    resFormated.meta = [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ]

    res.meta.forEach((tag) => {
      tag.hid = tag.name || tag.property
      resFormated.meta.push(tag)

      if (tag.hid === 'og:title') {
        resFormated.title = tag.content.replace('amp;', '').replace('amp;', '')

        if (resFormated.title === 'TUC Academy - TUC Academy') {
          resFormated.title = 'Startsida - TUC Academy'
        }
      }
    })

    res.link.forEach((tag) => {
      tag.hid = tag.rel
      resFormated.link.push(tag)
    })

    await store.dispatch({ type: 'meta/setMetaFormated', path: route.path, data: resFormated })

    context.app.head = resFormated
  }
}
