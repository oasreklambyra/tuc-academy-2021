// const http = require('http')
// import axios from 'axios'

/* const getToken = (context) => {
  const eduadmintoken = axios.post('https://api.eduadmin.se/token', 'username=d7131b39139b30b5&password=48eabc0a8c7a44598f22f404b3a89745&grant_type=password')
  return eduadmintoken
} */

export default function (context) {
  if (process.server) {
    const fileName = './token'
    // const timestamp = Date.now()
    const fs = require('fs')

    const promise = new Promise(async (resolve, reject) => {
      try {
        // get our stored files timestamps.
        /* const data = fs.statSync(fileName) */
        // is our file timestamp in the future?
        const token = await fs.readFileSync(fileName).toString()
        const tokenjson = JSON.parse(token)
        resolve(tokenjson.access_token)
        return
        /*
        if (Date.now() < data.mtimeMs) {
          // resolve our token from our file.
          resolve(fs.readFileSync(fileName).toString())
          // return
        }
        */
      } catch (err) {
        // ignore this error (file not found)
        console.log(err)
      }

      /*       try {
        // grab our token from the oAuth server,
        // we'll reduce the expiry timestamp by 10 mins
        // to reduce near expiring tokens.
        const res = await getToken(context)

        const expiryTime = timestamp + (res.data.expires_in - (60 * 10))

        // write our file and sync the times.
        await fs.writeFileSync(fileName, res.data.access_token)
        await fs.utimesSync(fileName, new Date(expiryTime), new Date(expiryTime))

        await resolve(res.data.access_token)
        // return
        // resolve(res.data.access_token)
      } catch (err) {
        // ToDo: handle token retrieval failure...
        console.log(err)
      } */
    })

    // once our promise is resolved, set it against our store.
    promise.then(
      promise => context.store.commit('bearerToken/getToken', promise),
      err => console.log(err)
    )
  }
}
