export const state = () => ({
  meta: {},
  metaFormated: {},
  wordpress: 'https://wordpress.tucacademy.se/wp-json/',
  // wordpress: 'https://wordpress.tucacademy.dev.oas.nu/wp-json/',
  baseUrl: 'https://www.tucacademy.se'
})

export const mutations = {

  getMeta (state, data) {
    const temporaryHolder = state.meta
    state.meta = [] // Updates the structure
    temporaryHolder[data.path] = data.data
    state.meta = temporaryHolder
  },

  setMetaFormated (state, data) {
    const temporaryHolder = state.metaFormated
    state.metaFormated = [] // Updates the structure
    temporaryHolder[data.path] = data.data
    state.metaFormated = temporaryHolder
    console.log('1')
    console.log(state.metaFormated)
  }
}

export const getters = { // Good to use if multi lang page
  getMeta (state) {
    return state.meta
  }
}

export const actions = {

  async getMeta (context, payload) {
    const url = context.state.baseUrl + payload.path

    try {
      const res = await this.$axios.get(context.state.wordpress + 'yoast/v1/get_head?url=' + url)
      console.log('res')
      console.log(res)
      context.commit('getMeta', {
        path: payload.path,
        data: res.data
      })
    } catch (error) {
      context.commit('getMeta', {
        path: payload.path,
        data: error.data
      })
    }
  },

  setMetaFormated (context, payload) {
    context.commit('setMetaFormated', {
      path: payload.path,
      data: payload.data
    })
    console.log(
      payload.data
    )
    console.log('2')
  }
}
