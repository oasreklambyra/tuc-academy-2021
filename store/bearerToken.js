export const state = () => ({
  edutoken: {},
  events: {},
  coursetemplates: {},
  categories: {}
})

export const mutations = {
  getToken (state, token) {
    // console.log(token)
    const temporaryHolder = state.edutoken
    state.edutoken = [] // Updates the structure
    temporaryHolder.tokenid = token
    // state.edutoken = temporaryHolder.tokenid
    state.edutoken = temporaryHolder
  },
  getEvents (state, data) {
    const temporaryHolder = state.events
    state.events = [] // Updates the structure
    temporaryHolder.events = data.value
    state.events = temporaryHolder
  },
  getCourseTemplates (state, data) {
    const temporaryHolder = state.coursetemplates
    state.coursetemplates = [] // Updates the structure
    temporaryHolder.templates = data.value
    state.coursetemplates = temporaryHolder
  },
  getCategories (state, data) {
    const temporaryHolder = state.categories
    state.categories = [] // Updates the structure
    temporaryHolder.categories = data.value
    state.categories = temporaryHolder
  }
}

export const getters = { // Good to use if multi lang page
  getToken (state) {
    return state.edutoken.tokenid
  },
  getEvents (state) {
    return state.events
  },
  getCourseTemplates (state) {
    return state.coursetemplates
  },
  getCategories (state) {
    return state.categories
  }
}

export const actions = {
  // this.$store.state.bearerToken.edutoken.tokenid
  async getEvents (context, payload) {
    this.$http.setHeader('Content-Type', 'application/json')
    this.$http.setToken(context.getters.getToken, 'Bearer')
    const resp = await this.$http.get('/v1/odata/Events?$expand=EventDates,PriceNames')
    const text = await resp.json()
    context.commit('getEvents', text)
  },

  async getCourseTemplates (context, payload) {
    this.$http.setHeader('Content-Type', 'application/json')
    this.$http.setToken(context.getters.getToken, 'Bearer')
    const resp = await this.$http.get('/v1/odata/CourseTemplates')
    const text = await resp.json()
    context.commit('getCourseTemplates', text)
  },

  async getCategories (context, payload) {
    this.$http.setHeader('Content-Type', 'application/json')
    this.$http.setToken(context.getters.getToken, 'Bearer')
    const resp = await this.$http.get('/v1/odata/Categories')
    const text = await resp.json()
    context.commit('getCategories', text)
  }

}
