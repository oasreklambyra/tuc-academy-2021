import Vue from 'vue'

Vue.mixin({
  data () {
    return {
      vueWindow: {
        scroll: 0,
        width: 0
      }
    }
  },
  mounted () {
    this.vueWindow.width = window.innerWidth

    window.addEventListener('scroll', () => {
      this.vueWindow.scroll = window.pageYOffset
    })
  },
  methods: {

    isIE () {
      if (process.client && navigator.userAgent.match(/Trident\/7\./)) {
        return true
      } else {
        return false
      }
    },

    getImageSize (maxSize) {
      const imageSizes = {
        mobile: 0,
        tablet: 576,
        laptop: 768,
        desktop: 992,
        max: 1080
      }

      let res = 'tablet'

      if (process.browser) {
        Object.keys(imageSizes).forEach((key) => {
          if (imageSizes[maxSize] >= imageSizes[key] && window.innerWidth >= imageSizes[key]) {
            res = key
          }
        })
      }

      if (res === 'mobile') {
        // res = 'laptop'
      }

      return res
    },
    formatUrlBind (url) {
      const res = {}

      const bind = (url && url.startsWith('http')) ? 'href' : 'to'

      res[bind] = url

      return res
    },

    addVariantToButtons (buttons) {
      const res = []

      if (buttons && Array.isArray(buttons)) {
        buttons.forEach((button) => {
          res.push(Object.assign({ variant: button.variation }, button.button))
        })
      }

      // console.log(res)

      return res
    },

    isMobile () {
      if (process.client) {
        return window.innerWidth < 576
      } else {
        return false
      }
    },

    isTablet () {
      if (process.client) {
        return window.innerWidth < 991
      } else {
        return false
      }
    },

    range (start, end) {
      const res = []
      for (let i = start; i <= end; i++) {
        res.push(i)
      }
      return res
    },

    scrolled () {
      if (process.client) {
        return this.vueWindow.scroll || 0
      } else {
        return 0
      }
    },

    getPageLogic (route) {
    // what should be fetched.
    // Used for singlepages that is not using the posttype 'page'
      const res = {
        name: 'startsida',
        type: 'page'
      }

      // Name

      let path = route.path.toLowerCase()

      path = (path.endsWith('/')) ? path.slice(0, -1) : path

      const routeArr = path.split('/')
      res.name = routeArr[routeArr.length - 1]

      if (res.name === '') {
        res.name = 'startsida'
      }

      // Type
      const params = Object.keys(route.params)

      if (params.length > 0 && params[0] !== 'category') {
        res.type = params[0]
      }

      if (route.params.post) {
        res.type = 'post'
      }

      return res
    },

    formatCategories (taxs) {
      const res = []

      taxs.forEach((cats) => {
        if (cats && Array.isArray(cats)) {
          cats.forEach((cat) => {
            res.push({
              name: cat.name,
              value: cat.slug,
              category: cat.taxonomy
            })
          })
        }
      })
      return res
    },

    getGroupName (slug, area) {
      area.forEach((cat) => {
        slug = slug.replace('-' + cat.slug, '')
      })

      let res = slug

      // Catch dupes
      for (let i = 0; i < 10; i++) {
        res = res.replace('-' + i, '')
      }

      return res
    },

    formatLocations (locations, grouped = false) {
      if (grouped) {
        return [{ name: 'Flera orter', slug: false, taxonomy: 'programme_course_area' }]
      }

      if (locations) {
        if (locations.length > 2) {
          return [{ name: 'Flera orter', slug: false, taxonomy: 'programme_course_area' }]
        }

        let distance = false
        locations.forEach((location) => {
          if (location.slug === 'distans') {
            distance = location
          }
        })

        if (distance) {
          return [distance]
        } else {
          return locations
        }
      } else {
        return false
      }
    },

    getCourseGroups (courses) {
      const res = {}

      if (courses) {
        courses.forEach((item) => {
          if (
            item.custom_fields.group_course &&
          item.post_taxs.programme_course_area
          ) {
            const group = this.getGroupName(
              item.post_name,
              item.post_taxs.programme_course_area
            )

            if (!res[group]) {
              res[group] = []
            }

            let title = ''
            let categories = ''

            const primaryCategory = item.post_taxs.programme_course_area.filter((cat) => {
              return cat.primary
            })

            const secoundaryCategories = item.post_taxs.programme_course_area.filter((cat) => {
              return !cat.primary && !cat.name.includes('Satellitort')
            })

            const tetriaryCategories = item.post_taxs.programme_course_area.filter((cat) => {
              return !cat.primary && cat.name.includes('Satellitort')
            })

            categories += primaryCategory.map((cat) => {
              return cat.name
            }).join(', ')

            if (secoundaryCategories.length > 0 || tetriaryCategories.length > 0) {
              categories += ' <small>('

              categories += secoundaryCategories.map((cat) => {
                return cat.name
              }).join(', ')

              if (secoundaryCategories.length > 0 && tetriaryCategories.length > 0) {
                categories += ', '
              }

              categories += tetriaryCategories.map((cat) => {
                return cat.name
              }).join(', ')

              categories += ')</small>'
            }

            title = categories

            /* if (item.post_taxs.programme_course_area.length > 1) {
              item.post_taxs.programme_course_area.forEach((tax) => {
                if (tax.primary && tax.slug === 'distans') {
                  title = tax.name + ' (' + item.post_taxs.programme_course_area[1].name + ')'
                } else if (tax.primary) {
                  title = tax.name
                }
              })
            }

            if (title === '') {
              if (item.post_taxs.programme_course_area.length > 1 && item.post_taxs.programme_course_area[0].slug === 'distans') {
                const locations = ''
                item.post_taxs.programme_course_area.forEach((location,index) => {
                  if(index > 0) {
                    location
                  }
                })

                console.log(locations)
                title = item.post_taxs.programme_course_area[0].name + ' (' + '' + ')'
              } else {
                title = item.post_taxs.programme_course_area[0].name
              }
            } */

            res[group].push({
              url: item.permalink,
              title,
              post_taxs: item.post_taxs,
              can_apply: item.custom_fields.apply
            })
          }
        })
      }

      return res
    }
  }
})
