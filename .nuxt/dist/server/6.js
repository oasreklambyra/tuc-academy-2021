exports.ids = [6];
exports.modules = {

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseFilter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(79);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseFilter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseFilter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseFilter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseFilter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 171:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.filter-wrapper label{font-size:inherit}.filter-wrapper hr{max-width:250px;border-top:1px solid rgba(160,169,186,.5);margin-left:0}@media (max-width:767.98px){.filter-wrapper hr{max-width:100%}}.filter-wrapper .clear-filter-btn-wrapper{width:100%;height:auto;display:flex;justify-content:flex-start;align-items:center;flex-wrap:wrap;margin-bottom:2rem}.filter-wrapper .clear-filter-btn-wrapper label{width:100%}.filter-wrapper .clear-filter-btn-wrapper .btn{display:flex;width:50px;height:50px;justify-content:center;align-items:center;padding:0}.filter-wrapper .clear-filter-btn-wrapper .btn svg{width:25px;height:25px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseFilter.vue?vue&type=template&id=8037cb2e&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"filter-wrapper mr-sm-3"},[_vm._l((_vm.filterOptions),function(filterOption,index){return _vm._ssrNode("<div>","</div>",[(filterOption.options.length > 0)?_vm._ssrNode("<div class=\"filter-section-wrapper\">","</div>",[_vm._ssrNode(((filterOption.title)?("<label class=\"font-weight-bold mb-1\">"+_vm._ssrEscape(_vm._s(filterOption.title))+"</label>"):"<!---->")+" "),_c('b-form-checkbox-group',{attrs:{"id":index,"name":index,"options":filterOption.options,"stacked":""},on:{"input":function($event){return _vm.input(index, $event)}},model:{value:(_vm.filterActive[index]),callback:function ($$v) {_vm.$set(_vm.filterActive, index, $$v)},expression:"filterActive[index]"}}),_vm._ssrNode(" <hr>")],2):_vm._e()])}),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"clear-filter-btn-wrapper\">","</div>",[_vm._ssrNode("<label class=\"font-weight-bold mb-1\">Rensa filter</label> "),_c('b-button',{attrs:{"variant":"primary","size":"sm"},on:{"click":function($event){return _vm.clearFilter()}}},[_c('svg-icon',{attrs:{"name":"filter"}})],1)],2)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseFilter.vue?vue&type=template&id=8037cb2e&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseFilter.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseFiltervue_type_script_lang_js_ = ({
  props: {
    filterOptions: {
      type: Object
    },
    filterActive: {
      type: Object
    }
  },
  methods: {
    input(input, value) {
      const filterActive = this.filterActive;
      filterActive[input] = value;
      this.$emit('updatefilterActive', filterActive);
    },

    clearFilter() {
      const filterActive = this.filterActive;
      Object.keys(filterActive).forEach(index => {
        filterActive[index] = [];
      });
      filterActive.search = null;
      this.$emit('updatefilterActive', filterActive);
    }

  }
});
// CONCATENATED MODULE: ./components/BaseFilter.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseFiltervue_type_script_lang_js_ = (BaseFiltervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseFilter.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(170)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseFiltervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "e5aa3546"
  
)

/* harmony default export */ var BaseFilter = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 79:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(171);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("7ad7e947", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=6.js.map