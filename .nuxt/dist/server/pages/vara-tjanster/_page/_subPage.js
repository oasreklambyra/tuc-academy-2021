exports.ids = [32,23];
exports.modules = {

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./pages/_page/index.vue + 4 modules
var _page = __webpack_require__(60);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/vara-tjanster/_page/_subPage.vue?vue&type=script&lang=js&

/* harmony default export */ var _subPagevue_type_script_lang_js_ = (_page["default"]);
// CONCATENATED MODULE: ./pages/vara-tjanster/_page/_subPage.vue?vue&type=script&lang=js&
 /* harmony default export */ var _page_subPagevue_type_script_lang_js_ = (_subPagevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./pages/vara-tjanster/_page/_subPage.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  _page_subPagevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "0d208edc"
  
)

/* harmony default export */ var _subPage = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 57:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(59);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("b8bab528", content, true, context)
};

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(57);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.page-wrapper .hero-content-wrapper.content-wrapper{margin-top:12vmin;position:relative}.page-wrapper .hero-content-wrapper.content-wrapper .content-inner{text-align:center;background-color:#fff;padding:3.5rem;max-width:none}@media (max-width:767.98px){.page-wrapper .hero-content-wrapper.content-wrapper .content-inner{padding:1rem}}.page-wrapper .hero-content-wrapper.content-wrapper .content-inner p{max-width:920px;margin-left:auto;margin-right:auto}.page-wrapper .hero-content-wrapper.content-wrapper .buttons-wrapper.button{margin-top:0;margin-bottom:4.5rem;display:flex;justify-content:center}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./pages/_page/index.vue?vue&type=template&id=4e268484&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"page-wrapper"},[(_vm.hero)?_c('BaseHero',{attrs:{"title":(_vm.hero.text_in_image)? _vm.introTitle: false,"content":(_vm.hero.text_in_image)? _vm.introContent : false,"image":(!_vm.hero.auto_hero && _vm.hero.image)? _vm.hero.image.sizes : _vm.content.featured_image,"iframe":(!_vm.hero.auto_hero && _vm.hero.video)? _vm.hero.video : false,"buttonsLabel":(_vm.hero.text_in_image)? _vm.hero.label : false,"buttons":(_vm.hero.text_in_image)? _vm.hero.buttons : false,"search":this.$route.path === '/',"overlap":!_vm.hero.text_in_image}}):_vm._e(),_vm._ssrNode(" "),(_vm.hero && !_vm.hero.text_in_image)?_c('BaseContent',{staticClass:"hero-content-wrapper",attrs:{"content":_vm.belowHeroContent,"button-options":{
      type: 'button',
      link: false,
      buttonsLabel: (!_vm.hero.text_in_image)? _vm.hero.label : false,
      buttons: (!_vm.hero.text_in_image)? _vm.hero.buttons : false,
    }}}):_vm._e(),_vm._ssrNode(" "),(_vm.content && _vm.content.custom_fields.pagebuilder)?_c('ThePagebuilder',{attrs:{"blocks":_vm.content.custom_fields.pagebuilder}}):_vm._e()],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/_page/index.vue?vue&type=template&id=4e268484&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/_page/index.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var _pagevue_type_script_lang_js_ = ({
  validate({
    route,
    params,
    query,
    store,
    $content,
    redirect
  }) {
    const page = store.state.pages[route.path];
    let category = null;

    if (route.params.category) {
      category = route.params.category;
    }

    let pageCategoryPrimary = null;

    if (category && page && page.post_taxs) {
      if (page.post_taxs.category && page.post_taxs.category[0]) {
        pageCategoryPrimary = page.post_taxs.category[0];
      }
    }

    if (pageCategoryPrimary && category && pageCategoryPrimary.slug !== category) {
      redirect(route.path.replace(category, pageCategoryPrimary.slug));
    }

    if (page) {
      return true;
    } else {
      return false;
    }
  },

  components: {
    BaseHero: () => __webpack_require__.e(/* import() */ 7).then(__webpack_require__.bind(null, 297)),
    BaseContent: () => __webpack_require__.e(/* import() */ 2).then(__webpack_require__.bind(null, 295)),
    ThePagebuilder: () => __webpack_require__.e(/* import() */ 11).then(__webpack_require__.bind(null, 301))
  },
  computed: {
    introTitle() {
      const hero = this.hero;
      return !hero.auto_hero && hero.title !== '' ? hero.title : this.content.post_title;
    },

    introContent() {
      const hero = this.hero;
      return !hero.auto_hero && hero.excerpt !== '' ? hero.excerpt : this.content.post_excerpt;
    },

    belowHeroContent() {
      return '<h1>' + this.introTitle + '</h1>' + '<div>' + this.introContent + '</div>';
    },

    hero() {
      let useHero = this.content.custom_fields.use_hero;

      if (this.content.custom_fields.use_hero === undefined) {
        useHero = true;
      }

      if (useHero) {
        return this.content.custom_fields.hero || false;
      } else {
        return false;
      }
    },

    content() {
      return this.$store.state.pages[this.$route.path] || false;
    }

  }
});
// CONCATENATED MODULE: ./pages/_page/index.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_pagevue_type_script_lang_js_ = (_pagevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./pages/_page/index.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(58)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_pagevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "68f0f3a1"
  
)

/* harmony default export */ var _page = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=_subPage.js.map