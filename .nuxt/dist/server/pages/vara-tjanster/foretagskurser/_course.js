exports.ids = [35];
exports.modules = {

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_course_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(64);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_course_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_course_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_course_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_course_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 141:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.course-wrapper .container .content-inner{background-color:rgba(225,228,235,.25);padding:3.5vmin 8vmin}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./pages/vara-tjanster/foretagskurser/_course.vue?vue&type=template&id=6b082592&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"course-wrapper"},[_c('BaseCourseIntro',{attrs:{"category":_vm.primaryCategory,"title":(_vm.useDefaultTitle)? _vm.content.post_title : _vm.content.custom_fields.intro.title,"sub-title":_vm.content.custom_fields.intro.sub_title,"content":(_vm.useDefaultTitle) ? _vm.content.post_excerpt : _vm.content.custom_fields.intro.excerpt,"image":_vm.content.featured_image[_vm.getImageSize('max')],"video":_vm.content.custom_fields.intro.youtube_video,"applyactive":_vm.content.custom_fields.apply,"applylink":_vm.content.custom_fields.anmalningslank,"quote":_vm.content.custom_fields.quote}}),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"container\">","</div>",[_c('BaseShare',{staticClass:"justify-content-end",attrs:{"url":_vm.content.permalink,"title":_vm.content.post_title}})],1),_vm._ssrNode(" "),(_vm.content.custom_fields.pagebuilder)?_c('ThePagebuilder',{attrs:{"blocks":_vm.content.custom_fields.pagebuilder,"content":_vm.content}}):_vm._e(),_vm._ssrNode(" "),(_vm.content.custom_fields.course_related.related_courses.length > 0)?_c('BaseArchiveCourse',{attrs:{"title":_vm.content.custom_fields.course_related.title,"courses":(_vm.content.custom_fields.course_related.related_courses.length > 0)? _vm.content.custom_fields.course_related.related_courses : false,"button-options":{
      type: _vm.content.custom_fields.course_related.link_or_button,
      link: _vm.content.custom_fields.course_related.link,
      buttonsLabel: _vm.content.custom_fields.course_related.label,
      buttons: _vm.content.custom_fields.course_related.buttons
    },"alignment":"center"}}):_vm._e(),_vm._ssrNode(" "),(_vm.content.custom_fields.post_related.related_posts.length)?_c('BaseArchivePost',{attrs:{"title":_vm.content.custom_fields.post_related.title,"posts":(_vm.content.custom_fields.post_related.related_posts.length > 0)? _vm.content.custom_fields.post_related.related_posts : false,"button-options":{
      type: _vm.content.custom_fields.post_related.link_or_button,
      link: _vm.content.custom_fields.post_related.link,
      buttonsLabel: _vm.content.custom_fields.post_related.label,
      buttons: _vm.content.custom_fields.post_related.buttons
    },"alignment":"center"}}):_vm._e(),_vm._ssrNode(" "),(false)?undefined:_vm._e()],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/vara-tjanster/foretagskurser/_course.vue?vue&type=template&id=6b082592&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/vara-tjanster/foretagskurser/_course.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var _coursevue_type_script_lang_js_ = ({
  components: {
    BaseCourseIntro: () => __webpack_require__.e(/* import() */ 14).then(__webpack_require__.bind(null, 306)),
    ThePagebuilder: () => __webpack_require__.e(/* import() */ 11).then(__webpack_require__.bind(null, 301)),
    // BaseApply: () => import('~/components/BaseApply.vue'),
    BaseArchiveCourse: () => __webpack_require__.e(/* import() */ 13).then(__webpack_require__.bind(null, 307)),
    BaseArchivePost: () => __webpack_require__.e(/* import() */ 40).then(__webpack_require__.bind(null, 308)),
    BaseShare: () => __webpack_require__.e(/* import() */ 58).then(__webpack_require__.bind(null, 309))
  },

  validate({
    route,
    params,
    query,
    store,
    $content
  }) {
    const page = store.state.pages[route.path];

    if (page) {
      return true;
    } else {
      return false;
    }
  },

  computed: {
    useDefaultTitle() {
      return !this.content.custom_fields.intro.use_title;
    },

    primaryCategory() {
      const cats = this.content.post_taxs;

      if (cats.programme_course_category && cats.programme_course_category[0]) {
        return cats.programme_course_category[0].name;
      } else {
        return false;
      }
    },

    content() {
      return this.$store.state.pages[this.$route.path];
    }

  }
});
// CONCATENATED MODULE: ./pages/vara-tjanster/foretagskurser/_course.vue?vue&type=script&lang=js&
 /* harmony default export */ var foretagskurser_coursevue_type_script_lang_js_ = (_coursevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./pages/vara-tjanster/foretagskurser/_course.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(140)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  foretagskurser_coursevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "cf4972fa"
  
)

/* harmony default export */ var _course = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 64:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(141);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("8edfa27c", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=_course.js.map