exports.ids = [30];
exports.modules = {

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./pages/utbildning/kategori/_kategori.vue?vue&type=template&id=5d1fb4fb&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"page-wrapper default-wrapper"},[(_vm.content)?_c('BaseHero',{attrs:{"image":_vm.content.ImageUrl}}):_vm._e(),_vm._ssrNode(" "),(_vm.content)?_c('BaseContent',{staticClass:"hero-content-wrapper",attrs:{"content":_vm.content.CategoryNotes,"title":_vm.content.CategoryName}}):_vm._e(),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"container\">","</div>",[_c('BaseCourseArchive',{attrs:{"include-course":true,"include-programme":false,"category":_vm.currentCategoryId}})],1)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/utbildning/kategori/_kategori.vue?vue&type=template&id=5d1fb4fb&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/utbildning/kategori/_kategori.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var _kategorivue_type_script_lang_js_ = ({
  components: {
    BaseHero: () => __webpack_require__.e(/* import() */ 49).then(__webpack_require__.bind(null, 303)),
    BaseContent: () => __webpack_require__.e(/* import() */ 42).then(__webpack_require__.bind(null, 304)),
    BaseCourseArchive: () => __webpack_require__.e(/* import() */ 43).then(__webpack_require__.bind(null, 305))
  },

  head() {
    let catTitle = 'TUC Academy';

    if (this.content) {
      catTitle = 'Våra kurser inom ' + this.content.CategoryName + ' - TUC Academy';
    }

    return {
      title: catTitle
    };
  },

  computed: {
    currentCategoryId() {
      const parts = this.$route.path.split('-');
      const lastItem = parts.pop();
      const categoryid = lastItem.replace('/', '');
      return categoryid;
    },

    content() {
      let result = {};

      if (this.$store.state.bearerToken.categories.categories) {
        result = this.$store.state.bearerToken.categories.categories.filter(item => {
          // console.log(item)
          return item.CategoryId === parseInt(this.currentCategoryId);
        });
      } // console.log(result)


      return result[0]; // return this.$store.state.pages[this.$route.path]
    }

  },

  mounted() {
    /*     this.$store.dispatch('bearerToken/getEvents')
    this.$store.dispatch('bearerToken/getCourseTemplates')
    this.$store.dispatch('bearerToken/getCategories') */
  }

});
// CONCATENATED MODULE: ./pages/utbildning/kategori/_kategori.vue?vue&type=script&lang=js&
 /* harmony default export */ var kategori_kategorivue_type_script_lang_js_ = (_kategorivue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./pages/utbildning/kategori/_kategori.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  kategori_kategorivue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "015a4d2e"
  
)

/* harmony default export */ var _kategori = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=_kategori.js.map