exports.ids = [29];
exports.modules = {

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_utbildning_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(67);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_utbildning_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_utbildning_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_utbildning_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_utbildning_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 147:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.utbildning-wrapper .event-container{padding:2.5rem 8rem;background-image:linear-gradient(289deg,#12499b 53%,#934d98);color:#fff}@media screen and (max-width:1080px){.utbildning-wrapper .event-container{padding:2.5rem 4rem}}@media screen and (max-width:600px){.utbildning-wrapper .event-container{padding:1.5rem}}@media screen and (max-width:420px){.utbildning-wrapper .event-container{padding:1.5rem 1rem}}.utbildning-wrapper .event-container a{color:#fff}.utbildning-wrapper .event-container a:hover{color:hsla(0,0%,100%,.8)!important}.utbildning-wrapper .event-container .status{font-size:.75rem;line-height:1;float:right;position:relative;top:-.75rem;right:-.75rem;color:#666}.utbildning-wrapper .event-container .status:before{content:\"\";display:inline-block;height:1rem;width:1rem;border-radius:50%;background:#00f;border:1px solid #fff;vertical-align:middle;position:relative;top:-2px}.utbildning-wrapper .event-container .status.open:before{background:#228b22}.utbildning-wrapper .event-container .status.few:before{background:orange}.utbildning-wrapper .event-container .status.none:before{background:#ff4500}.utbildning-wrapper .event-container ul.event-wrapper{margin:2rem 0 0;padding:0;display:flex;flex-wrap:wrap}.utbildning-wrapper .event-container ul.event-wrapper li{box-shadow:0 3px 9px -2px rgba(29,29,29,.25);width:350px;margin-right:2rem;max-width:350px;display:block;background:#fff;margin-bottom:2rem;border-radius:1rem;padding:2rem 2rem 1rem;color:#000}.utbildning-wrapper .event-container ul.event-wrapper li small{display:block;text-align:right;margin-top:.875rem;color:#666}.utbildning-wrapper .event-container ul.event-wrapper li h4{font-size:1.25rem;margin:0 0 .5rem;font-weight:700;letter-spacing:0}.utbildning-wrapper .event-container ul.event-wrapper li h5{font-size:.75rem;font-weight:700;margin:0;letter-spacing:0;color:#010b19}.utbildning-wrapper .event-container ul.event-wrapper li p.eventdate{font-size:.75rem;margin:0}.utbildning-wrapper .event-container ul.event-wrapper li p.price{font-size:1rem;color:#12499b;font-weight:700;border:1px solid #2c77e7;display:inline-block;padding:.25rem .625rem;border-radius:4px;margin:0 .5rem 0 0;text-align:left}.utbildning-wrapper .event-container ul.event-wrapper li p.price span{color:#666;font-size:.75rem;font-weight:400;display:block;line-height:1}.utbildning-wrapper .event-container ul.event-wrapper li p.price span.taxinfo{color:#666;margin-top:-.25rem}.utbildning-wrapper .event-container ul.event-wrapper li h3{font-size:1.125rem;margin:0}.utbildning-wrapper .event-container ul.event-wrapper li p{margin:.5em 0;font-size:.875rem}.utbildning-wrapper .event-container ul.event-wrapper li dl{margin:0}.utbildning-wrapper .event-container ul.event-wrapper li .application{display:flex;justify-content:space-between;align-items:center;margin-top:1rem;margin-bottom:.75rem}.utbildning-wrapper .event-container ul.event-wrapper li .application a.applicationlink{display:block;font-size:1rem;font-weight:400;letter-spacing:0}.utbildning-wrapper .event-container ul.event-wrapper li .application a.applicationlink.disabeled{opacity:.7;box-shadow:none;pointer-events:none}.utbildning-wrapper .container .content-inner{background-color:rgba(225,228,235,.25);padding:3.5vmin 8vmin}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./pages/utbildning/_utbildning.vue?vue&type=template&id=3dec5017&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"utbildning-wrapper"},[(_vm.content)?_c('BaseCourseIntro',{attrs:{"category":_vm.content.CategoryName,"title":_vm.content.CourseName,"image":_vm.content.ImageUrl,"shortDescription":_vm.content.CourseDescriptionShort}}):_vm._e(),_vm._ssrNode(" <div class=\"container event-container\">"+((Object.keys(_vm.currentEvents).length > 0 && _vm.content)?("<h4 class=\"font-weight-bold\">"+_vm._ssrEscape("\n      "+_vm._s(_vm.content.CourseName)+" - kommande kurstillfällen\n    ")+"</h4>"):(_vm.content)?("<div><h4 class=\"font-weight-bold\">"+_vm._ssrEscape("\n        "+_vm._s(_vm.content.CourseName)+" - Intresseanmälan\n      ")+"</h4> <p><strong>För intresseanmälan: </strong>Skicka ett e-brev till <a"+(_vm._ssrAttr("href",'mailto:academy@tucsweden.se?subject=Intresseanmälan - '+_vm.content.CourseName))+">academy@tucsweden.se</a>, och beskriv era utbildningsbehov och önskemål.</p></div>"):"<!---->")+" <ul class=\"event-wrapper\">"+(_vm._ssrList((_vm.currentEvents),function(Event){return ("<li>"+((Event.ParticipantNumberLeft > 5 && Event.StartDate.substring(0, 4) !== '2500' && Event.StartDate.substring(0, 4) !== '1853' )?("<div class=\"status open\">\n          Många platser kvar\n        </div>"):"<!---->")+" "+((Event.ParticipantNumberLeft < 6 && Event.ParticipantNumberLeft > 0 && Event.StartDate.substring(0, 4) !== '2500' && Event.StartDate.substring(0, 4) !== '1853')?("<div class=\"status few\">\n          Fåtal platser kvar\n        </div>"):"<!---->")+" "+((Event.ParticipantNumberLeft < 1 && Event.StartDate.substring(0, 4) !== '2500' && Event.StartDate.substring(0, 4) !== '1853')?("<div class=\"status none\">\n          Fullbokad\n        </div>"):"<!---->")+" <h4>"+_vm._ssrEscape(_vm._s(Event.City))+"</h4> "+((Event.StartDate.substring(0, 4) === '2500' || Event.StartDate.substring(0, 4) === '1853' )?("<h3>Valfri kursstart</h3>"):(Event.EventDates[0])?("<h5>Kursdagar</h5> "+(_vm._ssrList((Event.EventDates),function(Eventdate){return ("<p class=\"eventdate\">"+_vm._ssrEscape("\n            → "+_vm._s(Eventdate.StartDate.substring(0, 10))+" kl.  "+_vm._s(Eventdate.StartDate.substring(11, 16))+" – "+_vm._s(Eventdate.EndDate.substring(11, 16))+"\n          ")+"</p>")}))):(((Event.StartDate.substring(0, 10) == Event.EndDate.substring(0, 10))?("<h3>"+_vm._ssrEscape("\n            "+_vm._s(Event.StartDate.substring(0, 10))+"\n          ")+"</h3>"):("<h3>"+_vm._ssrEscape("\n            "+_vm._s(Event.StartDate.substring(0, 10))+" – "+_vm._s(Event.EndDate.substring(0, 10))+"\n          ")+"</h3>"))))+" "+((Event.StartDate.substring(0, 4) !== '2500' && Event.StartDate.substring(0, 4) !== '1853')?("<p>"+_vm._ssrEscape("\n          Totalt "+_vm._s(_vm.content.Days)+" ")+((_vm.content.Days == '1')?("<span>dag</span>"):("<span>dagar</span>"))+"</p>"):"<!---->")+" <div class=\"application\">"+((Event.PriceNames[0])?("<div class=\"pricing\">"+(_vm._ssrList((Event.PriceNames),function(Price){return ("<p class=\"price\"><span>"+_vm._ssrEscape(_vm._s(Price.PriceNameDescription))+"</span>"+_vm._ssrEscape("\n              "+_vm._s(new Intl.NumberFormat('sv-SE').format(Price.Price))+" kr\n              ")+"<span class=\"taxinfo\">ex. moms</span></p>")}))+"</div>"):"<!---->")+" "+((Event.ParticipantNumberLeft < 1 && Event.StartDate.substring(0, 4) !== '2500' && Event.StartDate.substring(0, 4) !== '1853')?("<a"+(_vm._ssrAttr("href",Event.BookingFormUrl))+" target=\"_blank\" class=\"applicationlink btn btn-primary disabeled\">Anmälan</a>"):("<a"+(_vm._ssrAttr("href",Event.BookingFormUrl))+" target=\"_blank\" class=\"applicationlink btn btn-primary\">Anmälan</a>"))+"</div> "+((Event.StartDate.substring(0, 4) !== '2500' && Event.StartDate.substring(0, 4) !== '1853')?("<small>"+_vm._ssrEscape("Sista dag för anmälan: "+_vm._s(Event.LastApplicationDate.substring(0, 10)))+"</small>"):"<!---->")+"</li>")}))+"</ul></div> <div class=\"container\">"+((_vm.content)?("<div class=\"content-inner\"><h2>"+_vm._ssrEscape(_vm._s(_vm.content.CourseName))+"</h2> <div>"+(_vm._s(_vm.content.CourseDescription))+"</div></div>"):"<!---->")+"</div>")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/utbildning/_utbildning.vue?vue&type=template&id=3dec5017&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/utbildning/_utbildning.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var _utbildningvue_type_script_lang_js_ = ({
  components: {
    BaseCourseIntro: () => __webpack_require__.e(/* import() */ 46).then(__webpack_require__.bind(null, 311)) // ThePagebuilder: () => import('~/components/ThePagebuilder.vue'),
    // BaseApply: () => import('~/components/BaseApply.vue'),
    // cBaseArchiveCourse: () => import('~/components/BaseArchiveCourse.vue'),
    // BaseArchivePost: () => import('~/components/BaseArchivePost.vue'),
    // BaseShare: () => import('~/components/BaseShare.vue')

  },

  head() {
    let catTitle = 'TUC Academy';

    if (this.content) {
      catTitle = this.content.CourseName + ' - TUC Academy';
    }

    return {
      title: catTitle
    };
  },

  computed: {
    /*     useDefaultTitle () {
      return !this.content.custom_fields.intro.use_title
    },
    primaryCategory () {
      const cats = this.content.post_taxs
      if (cats.programme_course_category && cats.programme_course_category[0]) {
        return cats.programme_course_category[0].name
      } else {
        return false
      }
    }, */
    currentEvents() {
      let res = {}; // console.log(this.$store.state.bearerToken.events.events)

      if (this.content && this.content.CourseTemplateId && this.$store.state.bearerToken.events.events) {
        const currentTemplate = this.content.CourseTemplateId;
        res = this.$store.state.bearerToken.events.events.filter(item => {
          return item.CourseTemplateId === parseInt(currentTemplate);
        });
      }

      return res;
    },

    content() {
      const parts = this.$route.path.split('-');
      const lastItem = parts.pop();
      const templateid = lastItem.replace('/', ''); // console.log(templateid)

      let result = {}; // console.log(this.$store.state.bearerToken.coursetemplates.templates)

      if (this.$store.state.bearerToken.coursetemplates.templates) {
        result = this.$store.state.bearerToken.coursetemplates.templates.filter(item => {
          // console.log(item)
          return item.CourseTemplateId === parseInt(templateid);
        });
      } // console.log(result)


      return result[0]; // return this.$store.state.pages[this.$route.path]
    }

  },

  /*   validate ({ route, params, query, store, $content }) {
    const page = store.state.pages[route.path]
     if (page) {
      return true
    } else {
      return false
    }
  }, */
  mounted() {
    /*     this.$store.dispatch('bearerToken/getEvents')
    this.$store.dispatch('bearerToken/getCourseTemplates')
    this.$store.dispatch('bearerToken/getCategories') */
  }

});
// CONCATENATED MODULE: ./pages/utbildning/_utbildning.vue?vue&type=script&lang=js&
 /* harmony default export */ var utbildning_utbildningvue_type_script_lang_js_ = (_utbildningvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./pages/utbildning/_utbildning.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(146)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  utbildning_utbildningvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "16e097b4"
  
)

/* harmony default export */ var _utbildning = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(147);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("a42d4cec", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=_utbildning.js.map