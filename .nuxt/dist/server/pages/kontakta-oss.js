exports.ids = [28];
exports.modules = {

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_kontakta_oss_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(62);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_kontakta_oss_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_kontakta_oss_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_kontakta_oss_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_kontakta_oss_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 137:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.kontakta-oss-wrapper .container .content-inner{padding:1rem 0!important}.kontakta-oss-wrapper .hero-content-wrapper{margin-top:12vmin;position:relative;background-color:#fff;padding:3.5rem}.kontakta-oss-wrapper .hero-content-wrapper h1{margin-bottom:1rem;margin-top:0;padding:0 1rem;text-align:center}@media (max-width:767px){.kontakta-oss-wrapper .hero-content-wrapper .filter-columns-1{display:flex;order:1;flex-wrap:wrap}.kontakta-oss-wrapper .hero-content-wrapper .filter-columns-1 *{width:100%}}@media (max-width:767px){.kontakta-oss-wrapper .hero-content-wrapper .filter-columns-2{display:flex;flex-wrap:wrap;order:2}}.kontakta-oss-wrapper .hero-content-wrapper .filter-columns-2 .filter-wrapper,.kontakta-oss-wrapper .hero-content-wrapper .filter-columns-2 .search-archive-wrapper{width:100%}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./pages/kontakta-oss.vue?vue&type=template&id=c4b4bc62&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"kontakta-oss-wrapper"},[(_vm.hero)?_c('BaseHero',{attrs:{"title":_vm.hero.text_in_image ? _vm.introTitle : false,"content":_vm.hero.text_in_image ? _vm.introContent : false,"image":!_vm.hero.auto_hero && _vm.hero.image
        ? _vm.hero.image.sizes
        : _vm.content.featured_image,"button-options":{
      type: 'button',
      buttonsLabel: _vm.hero.text_in_image ? _vm.hero.label : false,
      buttons: _vm.hero.text_in_image ? _vm.hero.buttons : false,
    },"search":this.$route.path === '/',"overlap":!_vm.hero.text_in_image}}):_vm._e(),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"hero-content-wrapper\">","</div>",[_vm._ssrNode("<div class=\"row\">","</div>",[(false)?undefined:_vm._e(),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-md-12 filter-columns-1\">","</div>",[_vm._ssrNode("<h1>"+_vm._ssrEscape("\n            "+_vm._s(_vm.introTitle)+"\n          ")+"</h1> "),(!_vm.hero.text_in_image)?_c('BaseContent',{attrs:{"content":_vm.introContent}}):_vm._e()],2)],2)])]),_vm._ssrNode(" "),_c('BaseArchiveEmployee',{attrs:{"employees":_vm.employeesSorted,"groupItems":!Array.isArray(_vm.employeesSorted)}}),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"container mt-3\">","</div>",[_vm._ssrNode("<div class=\"contact-wrapper\">","</div>",[(_vm.formId)?_c('BaseContactform',{attrs:{"id":_vm.formId}}):_vm._e()],1)])],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/kontakta-oss.vue?vue&type=template&id=c4b4bc62&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/kontakta-oss.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var kontakta_ossvue_type_script_lang_js_ = ({
  components: {
    BaseHero: () => __webpack_require__.e(/* import() */ 7).then(__webpack_require__.bind(null, 297)),
    BaseContent: () => __webpack_require__.e(/* import() */ 2).then(__webpack_require__.bind(null, 295)),
    BaseArchiveEmployee: () => __webpack_require__.e(/* import() */ 39).then(__webpack_require__.bind(null, 298)),
    BaseFilter: () => __webpack_require__.e(/* import() */ 6).then(__webpack_require__.bind(null, 299)),
    BaseContactform: () => __webpack_require__.e(/* import() */ 3).then(__webpack_require__.bind(null, 300))
  },

  data() {
    return {
      filterActive: {
        employee_area: [],
        programme_course_area: [],
        search: null
      }
    };
  },

  validate({
    route,
    params,
    query,
    store,
    $content
  }) {
    const page = store.state.pages[route.path];

    if (page) {
      return true;
    } else {
      return false;
    }
  },

  computed: {
    formId() {
      let res = false;

      if (this.content.custom_fields.form) {
        res = this.content.custom_fields.form;
      }

      return res;
    },

    filter() {
      const res = {
        employee_area: {
          title: 'Avdelning',
          options: []
        },
        programme_course_area: {
          title: 'Utbildningsort',
          options: []
        }
      };
      const uniqueCheck = [];
      const items = this.employees || [];
      items.forEach(item => {
        Object.keys(res).forEach(cat => {
          if (item.post_taxs[cat]) {
            item.post_taxs[cat].forEach(itemCat => {
              if (!uniqueCheck.includes(itemCat.slug)) {
                res[cat].options.push({
                  text: itemCat.name,
                  value: itemCat.slug
                });
                uniqueCheck.push(itemCat.slug);
              }
            });
          }
        });
      });
      res.employee_area.options.sort((a, b) => a.text > b.text ? 1 : b.text > a.text ? -1 : 0);
      res.programme_course_area.options.sort((a, b) => a.text > b.text ? 1 : b.text > a.text ? -1 : 0);
      return res;
    },

    employeesSorted() {
      let res = {};
      const employees = this.employees || [];
      const filterActive = this.filterActive;
      employees.sort((a, b) => a.post_title > b.post_title ? 1 : b.post_title > a.post_title ? -1 : 0);

      if (filterActive.search && filterActive.search !== '') {
        res = [];
      }

      employees.forEach(employee => {
        // setup
        let employeeAreas = false;
        let employeeLocation = false;

        if (employee.post_taxs.employee_area) {
          employeeAreas = employee.post_taxs.employee_area.map(cat => {
            return {
              name: cat.name.toLowerCase(),
              slug: cat.slug
            };
          });
        }

        if (employee.post_taxs.programme_course_area) {
          employeeLocation = employee.post_taxs.programme_course_area.map(cat => {
            return {
              name: cat.name.toLowerCase(),
              slug: cat.slug
            };
          });
        } // end setup


        if (employee.post_taxs && employee.post_taxs.employee_area) {
          // Create all sections
          employee.post_taxs.employee_area.forEach(cat => {
            let validArea = true;

            if (filterActive.employee_area.length > 0) {
              validArea = false;

              if (cat && filterActive.employee_area.includes(cat.slug)) {
                validArea = true;
              }
            }

            if (validArea) {
              if (!res[cat.slug]) {
                res[cat.slug] = {
                  title: cat.name,
                  id: cat.slug,
                  employees: []
                };
              }

              if (employee.post_taxs.programme_course_area && cat.slug === 'utbildningsledare') {
                if (!res[cat.slug].area) {
                  res[cat.slug].area = {};
                }

                employee.post_taxs.programme_course_area.forEach(area => {
                  let validLocation = true;

                  if (filterActive.programme_course_area.length > 0) {
                    validLocation = false;

                    if (area && filterActive.programme_course_area.includes(area.slug)) {
                      validLocation = true;
                    }
                  }

                  if (validLocation) {
                    if (!res[cat.slug].area[area.slug]) {
                      res[cat.slug].area[area.slug] = {
                        title: area.name,
                        id: area.slug,
                        employees: []
                      };
                    }
                  }
                });
              }
            }
          }); // add / remove from filter

          const valid = {
            area: true,
            location: true,
            search: true
          };

          if (filterActive.employee_area.length > 0) {
            valid.area = false;

            if (employeeAreas) {
              employeeAreas.forEach(area => {
                if (filterActive.employee_area.includes(area.slug)) {
                  valid.area = true;
                }
              });
            }
          }

          if (filterActive.programme_course_area.length > 0) {
            valid.location = false;

            if (employeeLocation) {
              employeeLocation.forEach(area => {
                if (filterActive.programme_course_area.includes(area.slug)) {
                  valid.location = true;
                }
              });
            }
          }

          const search = filterActive.search ? filterActive.search.toLowerCase() : false;
          const employeeName = employee.post_title.toLowerCase();
          const employeeRole = employee.custom_fields.title.toLowerCase();

          if (filterActive.search && filterActive.search !== '') {
            valid.search = false;
            const name = employeeName.includes(search);
            const role = employeeRole.includes(search);
            let areas = false;

            if (employeeAreas) {
              areas = employeeAreas.map(area => {
                return area.name;
              }).join(' ');
            }

            let location = false;

            if (employeeLocation) {
              location = employeeLocation.map(location => {
                return location.name;
              }).join(' ');
            }

            if (name || role || areas.includes(search) || location.includes(search)) {
              valid.search = true;
            } // Group empluees

          }

          if (valid.area && valid.location && valid.search) {
            if (filterActive.search && filterActive.search !== '') {
              res.push(employee);
            } else if (employeeAreas) {
              employeeAreas.forEach(area => {
                if (employeeLocation && area.slug === 'utbildningsledare') {
                  employeeLocation.forEach(location => {
                    if (res[area.slug] && res[area.slug].area[location.slug]) {
                      res[area.slug].area[location.slug].employees.push(employee);
                    }
                  });
                } else if (res[area.slug]) {
                  res[area.slug].employees.push(employee);
                }
              });
            }
          }
        }
      });
      return res;
    },

    employees() {
      return this.$store.state.cpt.employee;
    },

    introTitle() {
      const hero = this.hero;
      return !hero.auto_hero && hero.title !== '' ? hero.title : this.content.post_title;
    },

    introContent() {
      const hero = this.hero;
      return !hero.auto_hero && hero.excerpt !== '' ? hero.excerpt : this.content.post_excerpt;
    },

    hero() {
      return this.content.custom_fields.hero || false;
    },

    content() {
      return this.$store.state.pages[this.$route.path] || false;
    }

  },

  mounted() {
    if (!this.$store.state.cpt.employee) {
      this.$store.dispatch({
        type: 'getCpt',
        postType: 'employee'
      });
    }
  },

  methods: {
    updatefilterActiveFromFilter(filterActive) {
      this.filterActive = filterActive;
    }

  }
});
// CONCATENATED MODULE: ./pages/kontakta-oss.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_kontakta_ossvue_type_script_lang_js_ = (kontakta_ossvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./pages/kontakta-oss.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(136)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_kontakta_ossvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "0754abfa"
  
)

/* harmony default export */ var kontakta_oss = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(137);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("4857f49f", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=kontakta-oss.js.map