exports.ids = [43];
exports.modules = {

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseArchive_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(85);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseArchive_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseArchive_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseArchive_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseArchive_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 183:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}@media (max-width:991px){.order-m-1{order:1}.order-m-2{order:2}}.course-archive-wrapper{padding:0 0 70px}.course-archive-wrapper .items-wrapper .row{align-items:stretch}.course-archive-wrapper .items-wrapper .row .oneitem{margin-bottom:3rem}.course-archive-wrapper .items-wrapper .post-course-wrapper{margin-bottom:0;margin-left:.75rem;margin-right:.75rem;box-shadow:0 3px 9px -2px rgba(29,29,29,.25);border-radius:1rem;padding:0 1.5rem 1.5rem;height:100%}.course-archive-wrapper .items-wrapper .post-course-wrapper a .background-image-wrapper{margin-left:-1.5rem;margin-right:-1.5rem}.course-archive-wrapper .items-wrapper .post-course-wrapper a .background-image-wrapper .background-image{padding-top:75%;border-radius:1rem 1rem 0 0}.course-archive-wrapper .search-archive-wrapper{margin-bottom:3.7rem}.course-archive-wrapper .categories-wrapper .list-inline{border-top:1px solid rgba(160,169,186,.5);border-bottom:1px solid rgba(160,169,186,.5);padding:.7rem 0}.course-archive-wrapper .categories-wrapper .list-inline-item{cursor:pointer;margin:0;padding:1rem 1.2rem;color:#6c7175;transition:.2s}.course-archive-wrapper .categories-wrapper .list-inline-item:hover{opacity:.5}.course-archive-wrapper .categories-wrapper .list-inline-item.active{cursor:auto;color:#12499b}.course-archive-wrapper .categories-wrapper .list-inline-item.active:hover{opacity:1}.course-archive-wrapper .categories-wrapper .list-inline-item.active svg{fill:#12499b}.course-archive-wrapper .categories-wrapper .list-inline-item svg{margin-right:.5rem;width:1.5rem;height:1.5rem;vertical-align:text-top;fill:#6c7175}@media (max-width:767.98px){.course-archive-wrapper .categories-wrapper .list-inline-item{width:100%}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCourseArchive-edu.vue?vue&type=template&id=795df788&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"course-archive-wrapper"},[_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-12\">","</div>",[_vm._ssrNode("<div class=\"items-wrapper\">","</div>",[_vm._ssrNode("<div class=\"row\">","</div>",_vm._l((_vm.itemsFilterd),function(item){return _vm._ssrNode("<div class=\"col-12 col-sm-6 col-md-4 col-lg-3 oneitem\">","</div>",[_c('BasePostCourse',{attrs:{"title":item.CourseName,"image":item.ImageUrl,"content":item.CourseDescriptionShort,"yh":item.custom_fields ? item.custom_fields['yh-course'] : true,"isGroup":false,"siblings":item.siblings,"permalink":item.permalink}})],1)}),0)])])])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseCourseArchive-edu.vue?vue&type=template&id=795df788&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCourseArchive-edu.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseCourseArchive_eduvue_type_script_lang_js_ = ({
  components: {
    BasePostCourse: () => __webpack_require__.e(/* import() */ 52).then(__webpack_require__.bind(null, 339)) // BaseLoading: () => import('~/components/BaseLoading.vue')
    // BaseFilter: () => import('~/components/BaseFilter-edu.vue')

  },
  props: {
    includeCourse: {
      type: Boolean,
      default: false
    },
    includeProgramme: {
      type: Boolean,
      default: false
    },
    isFilterActive: {
      type: Boolean,
      default: true
    },
    category: {
      type: String,
      default: ''
    }
  },

  data() {
    return {
      filterActive: {
        programme_course_category: [],
        programme_course_length: [],
        programme_course_area: [],
        programme_course_flex: [],
        programme_course_period: [],
        programme_course_reskilling: [],
        programme_course_upskilling: [],
        programme_course_form: [],
        other: [],
        search: null
      },
      categoryIcons: {
        'ekonomi-administration-forsaljning': 'book-open',
        'bygg-anlaggning-fastighet': 'jackhammer',
        'data-it': 'processor',
        'hotell-restaurang-besoksnaring': 'thumbs-up',
        'teknik-tillverkning-drift-underhall': 'cog',
        'halso-sjukvard-socialt-arbete': 'medical-square',
        'ledarskap-kommunikation': 'comments',
        'behorighetsutbildning': 'check',
        kontakttolk: 'sign-alt'
      }
    };
  },

  computed: {
    /*     loading () {
      let res = false
       // console.log(this.programmes)
       if (this.items) {
        res = true
      }
       return res
    }, */
    items() {
      let items = [];
      const res = [];

      if (this.includeCourse) {// items = items.concat(this.CourseTemplates.value || [])
      }

      items = items.concat(this.$store.state.bearerToken.coursetemplates.templates || []); // const itemGroups = this.getCourseGroups(items)

      items.forEach(item => {
        let valid = true;

        if (item.ShowOnWeb === false) {
          valid = false;
        }

        if (this.category) {
          if (item.CategoryId !== parseInt(this.category)) {
            valid = false;
          }
        }

        if (valid) {
          res.push(item);
        }
      });
      return res.sort((a, b) => a.CourseName > b.CourseName ? 1 : b.CourseName > a.CourseName ? -1 : 0);
    },

    categories() {
      const res = [];
      const uniqueCheck = [];
      this.items.forEach(item => {
        if (item.CategoryName) {
          let slug = item.CategoryName.toLowerCase().replace(/[&/\\#,+()$~%.'":*?<>{}]/g, '').replace('å', 'a').replace('ä', 'a').replace('ö', 'o');
          slug = encodeURI(slug);

          if (!uniqueCheck.includes(slug)) {
            res.push({
              name: item.CategoryName,
              value: slug
            });
            uniqueCheck.push(slug);
          }
        }
      });
      return res;
    },

    /*     filter () {
      const res = {
         programme_course_form: {
          title: 'Utbildningsform',
          options: []
        },
         other: {
          title: false,
          options: []
        }
      }
       const uniqueCheck = []
      this.items.forEach((item) => {
        Object.keys(res).forEach((cat) => {
          if (item.post_taxs[cat]) {
            item.post_taxs[cat].forEach((itemCat) => {
              if (
                !uniqueCheck.includes(itemCat.slug) &&
                itemCat.slug !== 'foretagskurser'
              ) {
                res[cat].options.push({
                  text: itemCat.name,
                  value: itemCat.slug
                })
                uniqueCheck.push(itemCat.slug)
              }
            })
          }
        })
         if (item.siblings) {
          item.siblings.forEach((sibling) => {
            Object.keys(res).forEach((cat) => {
              if (sibling.post_taxs[cat]) {
                sibling.post_taxs[cat].forEach((siblingCat) => {
                  if (
                    !uniqueCheck.includes(siblingCat.slug) &&
                    siblingCat.slug !== 'foretagskurser'
                  ) {
                    res[cat].options.push({
                      text: siblingCat.name,
                      value: siblingCat.slug
                    })
                    uniqueCheck.push(siblingCat.slug)
                  }
                })
              }
            })
          })
        }
      })
       Object.keys(res).forEach((cat) => {
        if (cat !== 'course_type') {
          res[cat].options = res[cat].options.sort((a, b) => {
            // Move Satellitort to last
            const c = (a.text.includes('Satellitort')) ? 'z' + a.text : a.text
            const d = (b.text.includes('Satellitort')) ? 'z' + b.text : b.text
             return c > d ? 1 : d > c ? -1 : 0
          })
        }
      })
       return res
    }, */
    itemsFilterd() {
      const res = [];
      const filterActive = this.filterActive;
      this.items.forEach(item => {
        const valid = {}; // item.permalink = item.CourseName

        item.permalink = item.CourseName.toLowerCase().replaceAll(/[&/\\#,+()$~%.'":*?<>{}]/g, '').replaceAll('å', 'a').replaceAll('ä', 'a').replaceAll('ö', 'o').replaceAll(' ', '-') + '-' + item.CourseTemplateId;
        item.permalink = '/utbildning/' + encodeURI(item.permalink);
        Object.keys(filterActive).forEach(filter => {
          // console.log(filter)
          const filterSection = filterActive[filter];

          if (filterSection && filterSection.length > 0) {
            valid[filter] = false; // Special for course_type

            if (filter === 'course_type' && filterSection.includes('yh-utbildningar') && item.post_type === 'programme') {
              valid[filter] = true;
            } // Special for apply


            if (filter === 'other' && filterSection.includes('apply')) {
              /*               if (item.custom_fields.apply) {
                valid[filter] = true
              } */

              /*               if (item.siblings) {
                item.siblings.forEach((sibling) => {
                  if (sibling.can_apply) {
                    valid[filter] = true
                  }
                })
              } */
            }

            if (filter !== 'search') {
              /*            if (item.post_taxs[filter]) {
                item.post_taxs[filter].forEach((category) => {
                  if (filterSection.includes(category.slug.replace('satellitort-', ''))) {
                    valid[filter] = true
                  }
                })
              } */

              /*               if (item.siblings) {
                item.siblings.forEach((sibling) => {
                  if (sibling.post_taxs[filter]) {
                    sibling.post_taxs[filter].forEach((category) => {
                      if (filterSection.includes(category.slug)) {
                        valid[filter] = true
                      }
                    })
                  }
                })
              } */
            }
            /*             if (filter === 'search') {
              // title, excerpt, keyword
              const searchWord = filterSection.toLowerCase()
               let keyword = false
               if (item.custom_fields && item.custom_fields.keywords) {
                keyword = item.custom_fields.keywords
                  .toLowerCase()
                  .includes(searchWord)
              }
               if (
                item.post_title.toLowerCase().includes(searchWord) ||
                item.post_excerpt.toLowerCase().includes(searchWord) ||
                keyword
              ) {
                valid[filter] = true
              }
            } */

          }
        });
        let allTrue = true;
        Object.keys(valid).forEach(item => {
          if (!valid[item]) {
            allTrue = false;
          }
        });

        if (allTrue) {
          res.push(item);
        }
      });
      return res;
    },

    courses() {
      return this.$store.state.cpt.course;
    }

  },
  watch: {
    filterActive: {
      handler(filter) {
        const res = {};
        Object.keys(filter).forEach(filterKey => {
          const item = filter[filterKey];

          if (item && item.length > 0) {
            res[filterKey] = item;
          }
        });
        this.$router.push({
          query: res
        });
      },

      deep: true
    },

    $route(route) {
      this.populateFilterFromQuery(route.query);
    }

  },

  /*   async fetch () {
    this.$http.setHeader('Content-Type', 'application/json')
    this.$http.setToken(this.eduToken, 'Bearer')
    if (!this.CourseTemplates.value) {
      this.CourseTemplates = await this.$http.$get('https://api.eduadmin.se/v1/odata/CourseTemplates')
      // console.log(this.CourseTemplates)
    }
    if (!this.Events.value) {
      this.Events = await this.$http.$get('https://api.eduadmin.se/v1/odata/Events')
      // console.log(this.Events)
    }
  }, */
  mounted() {
    /*     this.$store.dispatch('bearerToken/getEvents')
    this.$store.dispatch('bearerToken/getCourseTemplates')
    this.$store.dispatch('bearerToken/getCategories') */
    if (!this.$store.state.cpt.programme && this.includeProgramme) {
      this.$store.dispatch({
        type: 'getCpt',
        postType: 'programme'
      });
    }
    /*     if (!this.$store.state.coursetemplates) {
      this.$store.dispatch({ type: 'getCourseTemplates' })
    } */


    if (!this.$store.state.cpt.course && this.includeCourse) {
      this.$store.dispatch({
        type: 'getCpt',
        postType: 'course'
      });
    }

    const query = this.$route.query;

    if (Object.keys(query).length > 0) {
      this.populateFilterFromQuery(query);
    }

    if (this.$route.name.includes('utbildning-')) {
      this.populateFilterFromQuery({
        course_type: [this.$route.name.replace('utbildning-', '')]
      });
    }
  },

  methods: {
    updatefilterActiveFromFilter(filterActive) {
      this.filterActive = filterActive;
    },

    populateFilterFromQuery(query) {
      Object.keys(query).forEach(key => {
        if (this.filterActive[key] || key === 'search') {
          if (typeof query[key] === 'string' && key !== 'search') {
            query[key] = [query[key]];
          }

          this.filterActive[key] = query[key];
        }
      });
    },

    toggleFilterItem(item, value) {
      if (!this.filterActive[item].includes(value)) {
        this.filterActive[item].push(value);
      } else {
        const index = this.filterActive[item].indexOf(value);

        if (index > -1) {
          this.filterActive[item].splice(index, 1);
        }
      }
    }

  }
});
// CONCATENATED MODULE: ./components/BaseCourseArchive-edu.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseCourseArchive_eduvue_type_script_lang_js_ = (BaseCourseArchive_eduvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseCourseArchive-edu.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(182)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseCourseArchive_eduvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "ea92eb8a"
  
)

/* harmony default export */ var BaseCourseArchive_edu = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 85:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(183);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("612c7496", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=43.js.map