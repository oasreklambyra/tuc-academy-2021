exports.ids = [61];
exports.modules = {

/***/ 102:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(217);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("00d3a217", content, true, context)
};

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderArchiveCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(102);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderArchiveCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderArchiveCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderArchiveCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderArchiveCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 217:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.pagebuilder-archive-course-wrapper{background-color:rgba(225,228,235,.25)}@media (max-width:575.98px){.pagebuilder-archive-course-wrapper .archive-item{margin-bottom:4rem}}.pagebuilder-archive-course-wrapper .slot-wrapper{overflow:hidden}.pagebuilder-archive-course-wrapper .no-wrap{flex-wrap:nowrap}.pagebuilder-archive-course-wrapper .nav-wrapper{margin-top:5.1rem;margin-bottom:1rem;display:flex;align-items:center}@media (max-width:767.98px){.pagebuilder-archive-course-wrapper .nav-wrapper{display:block}}.pagebuilder-archive-course-wrapper .nav-wrapper .prev-wrapper svg{transform:rotate(45deg);fill:#1d1d1d}.pagebuilder-archive-course-wrapper .nav-wrapper .next-wrapper svg{transform:rotate(-45deg);fill:#1d1d1d}@media (max-width:767.98px){.pagebuilder-archive-course-wrapper .nav-wrapper .next-wrapper,.pagebuilder-archive-course-wrapper .nav-wrapper .prev-wrapper{display:inline-block;width:49%}.pagebuilder-archive-course-wrapper .nav-wrapper .next-wrapper .btn,.pagebuilder-archive-course-wrapper .nav-wrapper .prev-wrapper .btn{display:block;width:100%}}.pagebuilder-archive-course-wrapper .nav-wrapper .slide-wrapper{margin:0 1rem}@media (max-width:767.98px){.pagebuilder-archive-course-wrapper .nav-wrapper .slide-wrapper{display:none}}.pagebuilder-archive-course-wrapper .nav-wrapper .slide-wrapper .list-inline-item{border-radius:50%;width:10px;height:10px;background-color:rgba(1,11,25,.25)}.pagebuilder-archive-course-wrapper .nav-wrapper .slide-wrapper .list-inline-item.active{background-color:#12499b}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/PagebuilderArchiveCourse.vue?vue&type=template&id=fa5b0bba&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"pagebuilder-archive-course-wrapper"},[_c('BaseBlockWrapper',{attrs:{"title":_vm.data.title,"sub-title":_vm.data.sub_title,"content":_vm.data.content,"button-options":{
      type: _vm.data.link_or_button,
      link: _vm.data.link,
      buttonsLabel: _vm.data.label,
      buttons: _vm.data.buttons,
    },"alignment":_vm.data.text_pos}},[_c('div',{class:['row no-wrap', {'justify-content-center' : _vm.totalPages === 0 }],style:({ transform: 'translateX(' + _vm.translate + '%)' })},_vm._l((_vm.coursesFormated),function(course){return _c('div',{key:course.ID,staticClass:"col-12 col-sm-3 archive-item"},[_c('BasePostCourse',{attrs:{"title":course.post_title,"image":course.featured_image,"image-pos":course.custom_fields.img_pos,"content":course.post_excerpt,"upper-tags":_vm.formatCategories([course.post_taxs.programme_course_internship]),"tags":_vm.formatCategories([
              course.post_taxs.programme_course_category,
              course.post_taxs.programme_course_speed,
              course.post_taxs.programme_course_form,
              _vm.formatLocations(course.post_taxs.programme_course_area) ]),"yh":course.custom_fields ? course.custom_fields['yh-course'] : false,"isGroup":(course.custom_fields.group_course)? course.groupName : false,"siblings":course.siblings,"permalink":course.permalink,"aa":""}})],1)}),0),_vm._v(" "),(_vm.totalPages > 0)?_c('div',{staticClass:"nav-wrapper"},[_c('div',{staticClass:"prev-wrapper",on:{"click":function($event){return _vm.prevPage()}}},[_c('b-button',{attrs:{"variant":"light"}},[_c('svg-icon',{attrs:{"name":"arrow-down-left"}})],1)],1),_vm._v(" "),_c('ul',{staticClass:"slide-wrapper list-inline"},_vm._l((_vm.range(0, _vm.totalPages)),function(page){return _c('li',{class:['list-inline-item', { active: _vm.currentPage >= page }],on:{"click":function($event){return _vm.setPage(page)}}})}),0),_vm._v(" "),_c('div',{staticClass:"next-wrapper",on:{"click":function($event){return _vm.nextPage()}}},[_c('b-button',{attrs:{"variant":"light"}},[_c('svg-icon',{attrs:{"name":"arrow-down-right"}})],1)],1)]):_vm._e()])],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/PagebuilderArchiveCourse.vue?vue&type=template&id=fa5b0bba&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/PagebuilderArchiveCourse.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var PagebuilderArchiveCoursevue_type_script_lang_js_ = ({
  components: {
    BaseBlockWrapper: () => __webpack_require__.e(/* import() */ 0).then(__webpack_require__.bind(null, 340)),
    BasePostCourse: () => __webpack_require__.e(/* import() */ 4).then(__webpack_require__.bind(null, 341))
  },
  props: {
    data: {
      type: Object
    }
  },

  data() {
    return {
      currentPage: 0
    };
  },

  computed: {
    translate() {
      return -100 * this.currentPage;
    },

    totalPages() {
      let cardsPerPage = 4;

      if (this.isTablet()) {
        cardsPerPage = 3;
      }

      if (this.isMobile()) {
        cardsPerPage = 1;
      }

      const res = this.coursesFormated.length / cardsPerPage;
      return Math.ceil(res) - 1;
    },

    coursesFormated() {
      const res = [];
      const items = this.data.course;
      const groupCourseActive = this.data.group_course_active === undefined ? true : this.data.group_course_active;
      const itemGroups = this.getCourseGroups(items);
      items.forEach(item => {
        if (groupCourseActive && item.custom_fields.group_course) {
          if (item.custom_fields.show_in_archive) {
            // Only show 1 in group
            const group = this.getGroupName(item.post_name, item.post_taxs.programme_course_area);
            const temp = Object.assign({
              groupName: group,
              siblings: itemGroups[group]
            }, item);
            res.push(temp);
          }
        } else {
          res.push(item);
        }
      });
      return res.sort((a, b) => a.post_title > b.post_title ? 1 : b.post_title > a.post_title ? -1 : 0);
    }

  },
  methods: {
    setPage(page) {
      this.currentPage = page;
    },

    nextPage() {
      if (this.currentPage === this.totalPages) {
        this.currentPage = 0;
      } else if (this.currentPage < this.totalPages) {
        this.currentPage++;
      }
    },

    prevPage() {
      if (this.currentPage === 0) {
        this.currentPage = this.totalPages;
      } else if (this.currentPage > 0) {
        this.currentPage--;
      }
    }

  }
});
// CONCATENATED MODULE: ./components/PagebuilderArchiveCourse.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_PagebuilderArchiveCoursevue_type_script_lang_js_ = (PagebuilderArchiveCoursevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/PagebuilderArchiveCourse.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(216)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_PagebuilderArchiveCoursevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "0adc4f65"
  
)

/* harmony default export */ var PagebuilderArchiveCourse = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=61.js.map