exports.ids = [1];
exports.modules = {

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseButtons_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(94);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseButtons_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseButtons_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseButtons_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseButtons_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 201:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.buttons-wrapper label{color:inherit;margin-bottom:.25rem}.buttons-wrapper .btn{margin-right:1vmin}.buttons-wrapper .h5{color:#12499b}.buttons-wrapper .h5 svg{margin-left:.5em;fill:#12499b;width:1em;height:1em;transform:rotate(90deg) translateX(-.1em)}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseButtons.vue?vue&type=template&id=ee8fe0ee&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:['buttons-wrapper', { 'text-left': _vm.type === 'link' }]},[(_vm.type === 'link' && _vm.link && _vm.link.url)?_c((_vm.link.url && _vm.link.url.startsWith('http')) ? 'a' : 'nuxt-link',_vm._b({tag:"a",staticClass:"h5 font-weight-bold"},'a',[
      { target: _vm.link.target },
      _vm.formatUrlBind(_vm.link.url) ],false),[_vm._v("\n    "+_vm._s(_vm.link.title)+"\n    "),_c('svg-icon',{attrs:{"name":"arrow-up"}})],1):_vm._e(),_vm._ssrNode(" "+((_vm.type === 'button' && _vm.label && _vm.label !== '')?("<div><label class=\"p\">"+_vm._ssrEscape(_vm._s(_vm.label))+"</label></div>"):"<!---->")+" "),_vm._l((_vm.buttonsFormated),function(button,i){return _c('b-button',_vm._b({key:i,staticClass:"mb-2",attrs:{"size":"sm"},domProps:{"innerHTML":_vm._s(button.title)}},'b-button',[
      { variant: button.variant },
      { target: button.target },
      _vm.formatUrlBind(button.url) ],false))})],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseButtons.vue?vue&type=template&id=ee8fe0ee&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseButtons.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseButtonsvue_type_script_lang_js_ = ({
  props: {
    type: {
      type: String,
      default: 'button'
    },
    link: {
      type: [Object, Boolean],
      default: false
    },
    label: {
      type: String
    },
    buttons: {
      type: [Object, Array, Boolean],
      default: false
    }
  },
  computed: {
    buttonsFormated() {
      if (this.type === 'button' && this.buttons) {
        if (Array.isArray(this.buttons)) {
          return this.buttons;
        } else {
          return [this.buttons];
        }
      } else {
        return [];
      }
    }

  }
});
// CONCATENATED MODULE: ./components/BaseButtons.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseButtonsvue_type_script_lang_js_ = (BaseButtonsvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseButtons.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(200)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseButtonsvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "2feb6028"
  
)

/* harmony default export */ var BaseButtons = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 94:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(201);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("35260456", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=1.js.map