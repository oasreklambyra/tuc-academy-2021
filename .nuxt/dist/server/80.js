exports.ids = [80];
exports.modules = {

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFaqbtn_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(72);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFaqbtn_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFaqbtn_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFaqbtn_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFaqbtn_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 157:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.faq-btn-wrapper{position:fixed;bottom:3vmin;left:3vmin;transition:.4s;z-index:999}.faq-btn-wrapper .title{font-weight:700;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);width:100%}.faq-btn-wrapper .btn{transform:scale(1);-webkit-animation:pulse 5s infinite;animation:pulse 5s infinite}.faq-btn-wrapper:before{pointer-events:none;content:\" \";position:absolute;height:100vmax;width:100vmax;background-color:#12499b;top:50%;left:50%;border-radius:50%;transform:translate(-50%,-50%);opacity:0}.faqBtn-enter-active{transition:1s}.faqBtn-enter-active:before{transition:1.2s}.faqBtn-leave-active{transition:1s}.faqBtn-enter,.faqBtn-leave-to{opacity:0}.faqBtn-enter:before,.faqBtn-leave-to:before{opacity:1;height:0;width:0}@-webkit-keyframes pulse{0%{transform:scale(1);box-shadow:0 0 0 0 transparent}5%{transform:scale(.99);box-shadow:0 0 0 0 transparent}10%{transform:scale(1.01);box-shadow:0 2px 4px 0 rgba(0,0,0,.7)}15%{transform:scale(.99);box-shadow:0 0 0 0 transparent}20%{transform:scale(1.01);box-shadow:0 2px 4px 0 rgba(0,0,0,.7)}25%{transform:scale(1);box-shadow:0 0 0 0 transparent}to{transform:scale(1);box-shadow:0 0 0 0 transparent}}@keyframes pulse{0%{transform:scale(1);box-shadow:0 0 0 0 transparent}5%{transform:scale(.99);box-shadow:0 0 0 0 transparent}10%{transform:scale(1.01);box-shadow:0 2px 4px 0 rgba(0,0,0,.7)}15%{transform:scale(.99);box-shadow:0 0 0 0 transparent}20%{transform:scale(1.01);box-shadow:0 2px 4px 0 rgba(0,0,0,.7)}25%{transform:scale(1);box-shadow:0 0 0 0 transparent}to{transform:scale(1);box-shadow:0 0 0 0 transparent}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/TheFaqbtn.vue?vue&type=template&id=13af13ce&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('transition',{attrs:{"name":"faqBtn"}},[(_vm.show)?_c('div',{class:['faq-btn-wrapper']},[_c('b-button',{attrs:{"size":"sm","to":"/fragehornan","variant":"primary"}},[_c('span',[_vm._v("\n        Vanliga frågor\n        "),_c('svg-icon',{attrs:{"name":"question-circle"}})],1)])],1):_vm._e()])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/TheFaqbtn.vue?vue&type=template&id=13af13ce&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/TheFaqbtn.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var TheFaqbtnvue_type_script_lang_js_ = ({
  data() {
    return {
      show: false,
      timeout: null
    };
  },

  mounted() {
    window.addEventListener('scroll', e => {
      clearTimeout(this.timeout); // this.show = false

      if (this.vueWindow.scroll > window.innerHeight / 2) {
        this.timeout = setTimeout(() => {
          this.show = true;
        }, 3000);
      } else {
        this.show = false;
      }
    });
  }

});
// CONCATENATED MODULE: ./components/TheFaqbtn.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_TheFaqbtnvue_type_script_lang_js_ = (TheFaqbtnvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/TheFaqbtn.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(156)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_TheFaqbtnvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "f9903d5a"
  
)

/* harmony default export */ var TheFaqbtn = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 72:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(157);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("cbbfdd96", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=80.js.map