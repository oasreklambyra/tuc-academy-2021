exports.ids = [71];
exports.modules = {

/***/ 105:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(223);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("109412f5", content, true, context)
};

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderFeaturedCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(105);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderFeaturedCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderFeaturedCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderFeaturedCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderFeaturedCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 223:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.pagebuilder-featured-course-wrapper,.pagebuilder-featured-course-wrapper>.container{overflow:hidden}.pagebuilder-featured-course-wrapper .no-wrap{flex-wrap:nowrap}.pagebuilder-featured-course-wrapper .nav-wrapper{margin-top:5.1rem;margin-bottom:1rem;display:flex;align-items:center}@media (max-width:767.98px){.pagebuilder-featured-course-wrapper .nav-wrapper{display:block}}.pagebuilder-featured-course-wrapper .nav-wrapper .prev-wrapper svg{transform:rotate(45deg);fill:#1d1d1d}.pagebuilder-featured-course-wrapper .nav-wrapper .next-wrapper svg{transform:rotate(-45deg);fill:#1d1d1d}@media (max-width:767.98px){.pagebuilder-featured-course-wrapper .nav-wrapper .next-wrapper,.pagebuilder-featured-course-wrapper .nav-wrapper .prev-wrapper{display:inline-block;width:49%}.pagebuilder-featured-course-wrapper .nav-wrapper .next-wrapper .btn,.pagebuilder-featured-course-wrapper .nav-wrapper .prev-wrapper .btn{display:block;width:100%}}.pagebuilder-featured-course-wrapper .nav-wrapper .slide-wrapper{margin:0 1rem}@media (max-width:767.98px){.pagebuilder-featured-course-wrapper .nav-wrapper .slide-wrapper{display:none}}.pagebuilder-featured-course-wrapper .nav-wrapper .slide-wrapper .list-inline-item{border-radius:50%;width:10px;height:10px;background-color:rgba(1,11,25,.25)}.pagebuilder-featured-course-wrapper .nav-wrapper .slide-wrapper .list-inline-item.active{background-color:#12499b}.pagebuilder-featured-course-wrapper .nav-wrapper .all-wrapper{margin-left:2rem}@media (max-width:767.98px){.pagebuilder-featured-course-wrapper .nav-wrapper .all-wrapper{margin-left:0;margin-top:2.5rem}}.pagebuilder-featured-course-wrapper .nav-wrapper .all-wrapper a{font-weight:700;color:#1d1d1d}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/PagebuilderFeaturedCourse.vue?vue&type=template&id=459680c1&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"pagebuilder-featured-course-wrapper"},[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"title-wrapper\"><div class=\"row\"><div class=\"col-12\"><h1 class=\"mt-5 mb-4\">"+_vm._ssrEscape("\n            "+_vm._s(_vm.data.title)+"\n          ")+"</h1></div></div></div> "),(_vm.loading)?_vm._ssrNode("<div>","</div>",[_vm._ssrNode("<div class=\"row no-wrap\""+(_vm._ssrStyle(null,{ transform: 'translateX(' + _vm.translate + '%)' }, null))+">","</div>",_vm._l((_vm.featuredPosts),function(featuredPost){return _vm._ssrNode("<div class=\"col-12 col-sm-4 col-lg-3\">","</div>",[_c('BasePostCourse',{attrs:{"title":featuredPost.post_title,"image":featuredPost.featured_image,"image-pos":featuredPost.custom_fields.img_pos,"content":featuredPost.post_excerpt,"upper-tags":_vm.formatCategories([featuredPost.post_taxs.programme_course_internship]),"tags":_vm.formatCategories([
                  featuredPost.post_taxs.programme_course_category,
                  featuredPost.post_taxs.programme_course_speed,
                  featuredPost.post_taxs.programme_course_form,
                  _vm.formatLocations(
                    featuredPost.post_taxs.programme_course_area,
                    featuredPost.custom_fields.group_course
                  ) ]),"yh":featuredPost.custom_fields ? featuredPost.custom_fields['yh-course'] : false,"isGroup":featuredPost.custom_fields.group_course ? featuredPost.groupName : false,"siblings":featuredPost.siblings,"permalink":featuredPost.permalink}})],1)}),0)]):_c('BaseLoading'),_vm._ssrNode(" "),(_vm.totalPages > 0)?_vm._ssrNode("<div class=\"nav-wrapper\">","</div>",[_vm._ssrNode("<div class=\"prev-wrapper\">","</div>",[_c('b-button',{attrs:{"variant":"light"}},[_c('svg-icon',{attrs:{"name":"arrow-down-left"}})],1)],1),_vm._ssrNode(" <ul class=\"slide-wrapper list-inline\">"+(_vm._ssrList((_vm.range(0, _vm.totalPages)),function(page){return ("<li"+(_vm._ssrClass(null,['list-inline-item', { active: _vm.currentPage >= page }]))+"></li>")}))+"</ul> "),_vm._ssrNode("<div class=\"next-wrapper\">","</div>",[_c('b-button',{attrs:{"variant":"light"}},[_c('svg-icon',{attrs:{"name":"arrow-down-right"}})],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"all-wrapper\">","</div>",[_c('nuxt-link',{staticClass:"description",attrs:{"to":_vm.typeUrl}},[_vm._v("\n          Se alla "+_vm._s(_vm.typeNiceName)+"\n        ")])],1)],2):_vm._e()],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/PagebuilderFeaturedCourse.vue?vue&type=template&id=459680c1&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/PagebuilderFeaturedCourse.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var PagebuilderFeaturedCoursevue_type_script_lang_js_ = ({
  components: {
    BasePostCourse: () => __webpack_require__.e(/* import() */ 4).then(__webpack_require__.bind(null, 341)),
    BaseLoading: () => __webpack_require__.e(/* import() */ 8).then(__webpack_require__.bind(null, 316))
  },
  props: {
    data: {
      type: Object
    }
  },

  data() {
    return {
      currentPage: 0
    };
  },

  computed: {
    translate() {
      return -100 * this.currentPage;
    },

    totalPages() {
      let cardsPerPage = 4;

      if (this.isTablet()) {
        cardsPerPage = 3;
      }

      if (this.isMobile()) {
        cardsPerPage = 1;
      }

      const res = this.featuredPosts.length / cardsPerPage;
      return Math.ceil(res) - 1;
    },

    typeNiceName() {
      let res = '';
      const options = this.data.show_options;

      if (options.includes('programme') && options.includes('course')) {
        res = 'utbildningar';
      } else if (options.includes('programme')) {
        res = 'YH-utbildningar';
      } else if (options.includes('course')) {
        res = 'kurser';
      }

      return res;
    },

    typeUrl() {
      let res = '#';
      const options = this.data.show_options;

      if (options.includes('programme') && options.includes('course')) {
        res = '/utbildning/';
      } else if (options.includes('programme')) {
        res = '/utbildning/yh-utbildningar/';
      } else if (options.includes('course')) {
        res = '/vara-tjanster/fardiga-kurser/';
      }

      return res;
    },

    loading() {
      let res = false;

      if (this.courses) {
        res = true;
      }

      if (this.programmes) {
        res = true;
      }

      return res;
    },

    featuredPosts() {
      let res = [];
      const options = this.data.show_options;
      let items = [];

      if (this.programmes && options.includes('programme')) {
        items = items.concat(this.programmes);
      }

      if (this.courses && options.includes('course')) {
        items = items.concat(this.courses);
      }

      const itemGroups = this.getCourseGroups(items);
      items.forEach(item => {
        if (item.custom_fields.featured) {
          if (item.custom_fields.group_course) {
            if (item.custom_fields.show_in_archive) {
              // Only show 1 in group
              const group = this.getGroupName(item.post_name, item.post_taxs.programme_course_area);
              const temp = Object.assign({
                groupName: group,
                siblings: itemGroups[group]
              }, item);
              res.push(temp);
            }
          } else {
            res.push(item);
          }
        }
      });

      if (res.length > 0) {
        res = res.sort((a, b) => a.post_title > b.post_title ? 1 : b.post_title > a.post_title ? -1 : 0);
        res = res.sort((a, b) => a.custom_fields.featured_priority < b.custom_fields.featured_priority ? 1 : b.custom_fields.featured_priority < a.custom_fields.featured_priority ? -1 : 0);
      }

      return res;
    },

    programmes() {
      return this.$store.state.cpt.programme;
    },

    courses() {
      return this.$store.state.cpt.course;
    }

  },

  mounted() {
    const options = this.data.show_options;

    if (!this.$store.state.cpt.programme && options.includes('programme')) {
      this.$store.dispatch({
        type: 'getCpt',
        postType: 'programme'
      });
    }

    if (!this.$store.state.cpt.course && options.includes('course')) {
      this.$store.dispatch({
        type: 'getCpt',
        postType: 'course'
      });
    }
  },

  methods: {
    setPage(page) {
      this.currentPage = page;
    },

    nextPage() {
      if (this.currentPage === this.totalPages) {
        this.currentPage = 0;
      } else if (this.currentPage < this.totalPages) {
        this.currentPage++;
      }
    },

    prevPage() {
      if (this.currentPage === 0) {
        this.currentPage = this.totalPages;
      } else if (this.currentPage > 0) {
        this.currentPage--;
      }
    }

  }
});
// CONCATENATED MODULE: ./components/PagebuilderFeaturedCourse.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_PagebuilderFeaturedCoursevue_type_script_lang_js_ = (PagebuilderFeaturedCoursevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/PagebuilderFeaturedCourse.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(222)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_PagebuilderFeaturedCoursevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "372dd631"
  
)

/* harmony default export */ var PagebuilderFeaturedCourse = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=71.js.map