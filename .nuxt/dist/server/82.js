exports.ids = [82];
exports.modules = {

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheHeader_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(69);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheHeader_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheHeader_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheHeader_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheHeader_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 151:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.header-wrapper{display:flex;align-items:center;width:100%;height:70px;position:fixed;top:0;left:0;z-index:1000;background:#fff}.header-wrapper .logotype{height:2.9rem;width:9rem;margin:1rem 0}.header-wrapper .message{position:absolute;top:100%;left:0;width:100%;display:flex;justify-content:center;align-items:center;z-index:-1}.header-wrapper .message .message-inner .container{background-image:linear-gradient(289deg,#12499b 53%,#934d98);padding:3em 5em;position:relative}@media (max-width:1440px){.header-wrapper .message .message-inner .container{width:90%}}.header-wrapper .message .message-inner .container svg{position:absolute;top:1vmin;right:1vmin;width:30px;height:30px;fill:#fff;z-index:2;transform:rotate(45deg);transition:all .15s ease;cursor:pointer}.header-wrapper .message .message-inner .container h5{font-size:21px;font-weight:700;margin:0 0 1vmin;color:#fff}.header-wrapper .message .message-inner .container p{font-size:16px;color:#fff}.header-wrapper .message .message-inner .container .buttons-wrapper{margin-top:3vmin}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/TheHeader.vue?vue&type=template&id=854d5ef8&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:['header-wrapper']},[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-2\">","</div>",[_c('nuxt-link',{attrs:{"to":"/"}},[_c('svg-icon',{staticClass:"logotype",attrs:{"name":"tuc/logotype"}})],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-10 align-self-center\">","</div>",[_c('TheMenu')],1),_vm._ssrNode(" "),_c('client-only',[_c('div',{staticClass:"col-12 message"},[_c('div',{staticClass:"message-inner"},[(_vm.showMessage)?_c('div',{staticClass:"container"},[_c('span',{on:{"click":function($event){return _vm.checkMessage(_vm.options.message.rubrik)}}},[_c('svg-icon',{staticClass:"close",attrs:{"block":"","name":"tuc/plus"}})],1),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-12"},[_c('h5',[_vm._v("\n                    "+_vm._s(_vm.options.message.rubrik)+"\n                  ")]),_vm._v(" "),_c('p',[_vm._v("\n                    "+_vm._s(_vm.options.message.text)+"\n                  ")]),_vm._v(" "),(_vm.options.message.buttons)?_c('BaseButtons',{ref:"buttons",attrs:{"label":_vm.options.message.label,"buttons":_vm.addVariantToButtons(_vm.options.message.buttons),"type":"button"}}):_vm._e()],1)])]):_vm._e()])])])],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/TheHeader.vue?vue&type=template&id=854d5ef8&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/TheHeader.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var TheHeadervue_type_script_lang_js_ = ({
  components: {
    TheMenu: () => __webpack_require__.e(/* import() */ 83).then(__webpack_require__.bind(null, 313)),
    BaseButtons: () => __webpack_require__.e(/* import() */ 1).then(__webpack_require__.bind(null, 314))
  },

  data() {
    return {
      messageSubject: ''
    };
  },

  computed: {
    showMessage() {
      if (this.options.message.show) {
        return this.messageSubject !== this.options.message.rubrik;
      }

      return false;
    },

    options() {
      return this.$store.getters.getOptions || [];
    }

  },

  mounted() {
    this.messageSubject = window.localStorage.messageSubject;
  },

  methods: {
    checkMessage(subject) {
      window.localStorage.messageSubject = subject;
      this.messageSubject = subject;
    }

  }
});
// CONCATENATED MODULE: ./components/TheHeader.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_TheHeadervue_type_script_lang_js_ = (TheHeadervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/TheHeader.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(150)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_TheHeadervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "e194564c"
  
)

/* harmony default export */ var TheHeader = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 69:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(151);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("c1f18bd6", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=82.js.map