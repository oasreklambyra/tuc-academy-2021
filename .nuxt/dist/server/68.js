exports.ids = [68];
exports.modules = {

/***/ 118:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(249);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("55ad119a", content, true, context)
};

/***/ }),

/***/ 248:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderCourseSummary_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(118);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderCourseSummary_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderCourseSummary_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderCourseSummary_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderCourseSummary_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 249:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner{padding:2.5rem 8rem;background-image:linear-gradient(289deg,#12499b 53%,#934d98);color:#fff}@media (max-width:960px){.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner{padding:2.5rem}}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner .content,.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner .content *{color:#fff}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner .main{border-bottom:1px solid #fff;margin-bottom:1rem;padding-bottom:1rem;position:relative}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner .tab-content{border-top:1px solid #fff;padding-top:1rem}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner .tab-content label{color:#fff;font-weight:700}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner a{color:#fff;text-decoration:underline}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner a:hover{color:#fff!important;opacity:.5}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner .nav-tabs{justify-content:flex-start;border:none}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner .nav-tabs .nav-item .nav-link{text-decoration:none;color:#fff;opacity:.5;border:none}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner .nav-tabs .nav-item .nav-link:hover{opacity:.9;background:none;color:#fff!important}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner .nav-tabs .nav-item .nav-link.active{opacity:1;background:none}.pagebuilder-course-summary-wrapper .pagebuilder-course-summary-inner .nav-tabs .nav-item .nav-link a{padding:0;text-decoration:none}.pagebuilder-course-summary-wrapper .tags{position:absolute;top:1.3em;right:0}.pagebuilder-course-summary-wrapper .logo-wrapper{display:flex;flex-wrap:wrap;justify-content:flex-end}.pagebuilder-course-summary-wrapper .partners-wrapper img{background:#fff;width:100%;height:150px;-o-object-fit:contain;object-fit:contain;-o-object-position:center;object-position:center;margin-bottom:.5rem}@media (max-width:1440px){.pagebuilder-course-summary-wrapper .partners-wrapper img{height:75px}}@media (max-width:600px){.pagebuilder-course-summary-wrapper .footer a{margin-top:1rem;display:flex}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/PagebuilderCourseSummary.vue?vue&type=template&id=5d954775&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"pagebuilder-course-summary-wrapper"},[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"pagebuilder-course-summary-inner\">","</div>",[_vm._ssrNode("<div class=\"header\"><h4 class=\"font-weight-bold\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.currentPage.post_title)+" - Sammanfattning\n        ")+"</h4></div> "),_vm._ssrNode("<div class=\"main\">","</div>",[_vm._ssrNode("<div class=\"tags d-none d-md-block\">","</div>",[_c('BaseTags',{attrs:{"tags":_vm.secoundaryTags,"variant":"dark"}})],1),_vm._ssrNode(" "),_c('client-only',[_c('b-tabs',[(_vm.data.info.length > 0)?_c('b-tab',{attrs:{"title":_vm.tabTitles.facts,"active":""}},[_c('div',{staticClass:"row"},[_vm._l((_vm.data.info),function(infoChunk){return _c('div',{class:[
                    'col-12 mb-4',
                    {
                      'col-md-6':
                        _vm.data.facts_cols === '2' && _vm.data.facts_cols > '1',
                    },
                    { 'col-lg-4': _vm.data.facts_cols === '3' },
                    { 'col-lg-3': _vm.data.facts_cols === '4' } ]},[_c('label',{staticClass:"p",domProps:{"innerHTML":_vm._s(infoChunk.title)}}),_vm._v(" "),_c('p',{domProps:{"innerHTML":_vm._s(_vm.forceTargetBlank(infoChunk.info))}})])}),_vm._v(" "),(_vm.data.other_info !== '')?_c('div',{class:[
                    'col-12 mb-5',
                    {
                      'col-md-6':
                        _vm.data.facts_cols === '2' && _vm.data.facts_cols > '1',
                    },
                    { 'col-lg-4': _vm.data.facts_cols === '3' },
                    { 'col-lg-3': _vm.data.facts_cols === '4' } ]},[_c('div',{staticClass:"content",domProps:{"innerHTML":_vm._s(_vm.data.other_info)}})]):_vm._e()],2)]):_vm._e(),_vm._v(" "),(_vm.data.competence_list.length > 0)?_c('b-tab',{attrs:{"title":_vm.tabTitles.competence}},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-12"},[_c('div',{staticClass:"row"},_vm._l((_vm.data.competence_list),function(infoChunk){return _c('div',{staticClass:"col-12 mb-4"},[_c('label',{staticClass:"p",domProps:{"innerHTML":_vm._s(infoChunk.title)}}),_vm._v(" "),_c('p',{domProps:{"innerHTML":_vm._s(_vm.forceTargetBlank(infoChunk.info))}})])}),0)])])]):_vm._e(),_vm._v(" "),(_vm.data.course_list.length > 0)?_c('b-tab',{attrs:{"title":_vm.tabTitles.courses}},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-12"},[_c('div',{staticClass:"row"},_vm._l((_vm.data.course_list),function(infoChunk){return _c('div',{staticClass:"col-md-12 mb-4"},[_c('label',{staticClass:"p",domProps:{"innerHTML":_vm._s(infoChunk.title)}}),_vm._v(" "),_c('p',{domProps:{"innerHTML":_vm._s(_vm.forceTargetBlank(infoChunk.course_content))}})])}),0)])])]):_vm._e(),_vm._v(" "),(
                _vm.data.collaborators.length > 0 ||
                  _vm.data.partners_content.length > 0
              )?_c('b-tab',{attrs:{"title":_vm.tabTitles.partners}},[_c('div',{staticClass:"partners-wrapper"},[(_vm.data.partners_content && _vm.data.partners_content !== '')?_c('div',{staticClass:"content mt-2 mb-3",domProps:{"innerHTML":_vm._s(_vm.data.partners_content)}}):_vm._e(),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-12"},[_c('div',{staticClass:"row justify-content-start"},_vm._l((_vm.data.collaborators),function(infoChunk){return _c('div',{staticClass:"col-6 col-sm-3"},[_c('a',{attrs:{"href":infoChunk.link,"target":"_blank"}},[_c('img',{directives:[{name:"lazy",rawName:"v-lazy",value:(
                              (infoChunk.logotype) ? infoChunk.logotype.sizes[_vm.getImageSize('mobile')] : null
                            ),expression:"\n                              (infoChunk.logotype) ? infoChunk.logotype.sizes[getImageSize('mobile')] : null\n                            "}],staticClass:"img-fluid"})])])}),0)])])])]):_vm._e()],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"logo-wrapper\">","</div>",[_vm._ssrNode("<div class=\"logo-inner\">","</div>",[(_vm.currentPage.custom_fields['yh-course'])?_c('img',{directives:[{name:"lazy",rawName:"v-lazy",value:('/myh-logotyp.png'),expression:"'/myh-logotyp.png'"}],staticClass:"img-fluid"},[]):_vm._e(),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"tags-m mt-2 d-block d-md-none\">","</div>",[_c('BaseTags',{attrs:{"tags":_vm.secoundaryTags,"variant":"dark"}})],1)],2)])],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"footer\">","</div>",[_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-12 col-sm-7\">","</div>",[_c('BaseTags',{attrs:{"tags":_vm.primaryTags,"variant":"light"}})],1),_vm._ssrNode(" "+((_vm.responsible)?("<div class=\"col-12 col-sm-5 text-right responsible\"><a href=\"#utbildningsledare\">"+_vm._ssrEscape("Kontaktperson: "+_vm._s(_vm.responsible.post_title))+"</a></div>"):"<!---->"))],2)])],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/PagebuilderCourseSummary.vue?vue&type=template&id=5d954775&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/PagebuilderCourseSummary.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var PagebuilderCourseSummaryvue_type_script_lang_js_ = ({
  components: {
    BaseTags: () => __webpack_require__.e(/* import() */ 10).then(__webpack_require__.bind(null, 352))
  },
  props: {
    data: {
      type: Object
    },
    currentPage: {
      type: Object
    }
  },
  computed: {
    tabTitles() {
      const res = {
        facts: 'Fakta',
        competence: 'Pris',
        courses: 'Innehåll',
        partners: 'Samarbetspartners'
      };
      return res;
    },

    currentCourseTypes() {
      const res = [];
      const taxs = this.currentPage.post_taxs;

      if (taxs && taxs.course_type) {
        taxs.course_type.forEach(type => {
          res.push(type.slug);
        });
      }

      return res;
    },

    primaryTags() {
      let speed = false;
      let category = false;
      let distance = false;
      let area = false;
      Object.keys(this.currentPage.post_taxs).forEach(key => {
        const cat = this.currentPage.post_taxs[key];

        if (cat) {
          cat.forEach(catItem => {
            if (catItem.taxonomy === 'programme_course_speed' && !speed) {
              speed = {
                icon: 'clock',
                name: catItem.name,
                category: catItem.taxonomy,
                value: catItem.slug
              };
            }

            if (catItem.taxonomy === 'programme_course_category' && !category) {
              category = {
                icon: 'bright',
                name: catItem.name,
                category: catItem.taxonomy,
                value: catItem.slug
              };
            }

            if (catItem.taxonomy === 'programme_course_area' && catItem.value !== 'distans' && !area) {
              area = {
                icon: 'location-pin',
                name: catItem.name,
                category: catItem.taxonomy,
                value: catItem.slug
              };
            }

            if (catItem.taxonomy === 'programme_course_area' && catItem.value === 'distans' && !distance) {
              distance = {
                icon: 'books',
                name: catItem.name,
                category: catItem.taxonomy,
                value: catItem.slug
              };
            }
          });
        }
      }); // speed
      // Kategori
      // Distans
      // Ort

      return [speed, category, distance, area];
    },

    secoundaryTags() {
      let internship = false;
      let meets = false;
      Object.keys(this.currentPage.post_taxs).forEach(key => {
        const cat = this.currentPage.post_taxs[key];
        const links = {
          programme_course_internship: '/studera-pa-tuc/lia/',
          programme_course_meets: '/utbildning/studera-pa-tuc/distansstudier/'
        };

        if (cat) {
          cat.forEach(catItem => {
            if (catItem.taxonomy === 'programme_course_internship' && !internship) {
              internship = {
                name: 'Praktik ' + catItem.name,
                category: catItem.taxonomy,
                value: catItem.slug,
                url: links[catItem.taxonomy] ? links[catItem.taxonomy] : false
              };
            }

            if (catItem.taxonomy === 'programme_course_meets' && !meets) {
              meets = {
                name: catItem.name + ' träffar',
                category: catItem.taxonomy,
                value: catItem.slug,
                url: links[catItem.taxonomy] ? links[catItem.taxonomy] : false
              };
            }
          });
        }
      });
      return [meets, internship];
    },

    responsible() {
      let res = false;
      this.currentPage.custom_fields.pagebuilder.forEach(block => {
        if (block.acf_fc_layout === 'responsible') {
          res = block;
        }
      });
      return res.responsible;
    }

  },
  methods: {
    forceTargetBlank(string) {
      return string.replace(/<a /g, '<a target="_blank" ');
    }

  }
});
// CONCATENATED MODULE: ./components/PagebuilderCourseSummary.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_PagebuilderCourseSummaryvue_type_script_lang_js_ = (PagebuilderCourseSummaryvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/PagebuilderCourseSummary.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(248)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_PagebuilderCourseSummaryvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "4cb05ff3"
  
)

/* harmony default export */ var PagebuilderCourseSummary = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=68.js.map