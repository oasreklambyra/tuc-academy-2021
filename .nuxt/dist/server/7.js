exports.ids = [7];
exports.modules = {

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseHero_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(77);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseHero_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseHero_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseHero_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseHero_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 167:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.hero-wrapper{min-height:500px;height:70vh;max-height:2000px;position:relative;background-position:50%;background-size:cover}@media (max-width:575.98px){.hero-wrapper{max-height:0;height:auto;min-height:350px}}.hero-wrapper.next-block-overlap{margin-bottom:-24vmin}.hero-wrapper.overlay:before{content:\" \";background-color:#0e0c0e;opacity:.3}.hero-wrapper .iframe,.hero-wrapper.overlay:before{position:absolute;top:0;left:0;right:0;bottom:0}.hero-wrapper .iframe iframe{width:100%;height:100%}.hero-wrapper .hero-inner{z-index:2;position:absolute;top:50%;left:0;right:0;transform:translateY(-50%);color:#fff}@media (max-width:767.98px){.hero-wrapper .hero-inner{top:60%}}.hero-wrapper .hero-inner .content{max-width:805px;margin:0 auto}.hero-wrapper .hero-inner .content p{color:inherit}.hero-wrapper .hero-inner .buttons-wrapper{margin-top:4vmin;display:flex;justify-content:center;align-items:center;flex-wrap:wrap}.hero-wrapper .hero-inner .buttons-wrapper div{width:100%;margin-bottom:1vmin}.hero-wrapper .hero-inner .animation-overlay{position:relative;height:auto;padding:10px 0}.hero-wrapper .hero-inner .animation-overlay .h2{margin-bottom:0}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseHero.vue?vue&type=template&id=72819f2d&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{directives:[{name:"lazy",rawName:"v-lazy:background-image",value:(_vm.image ? _vm.image[_vm.getImageSize('max')] : false),expression:"image ? image[getImageSize('max')] : false",arg:"background-image"}],ref:"wrapper",class:[
    'hero-wrapper background-image',
    { 'next-block-overlap': _vm.overlap },
    { overlay: !_vm.overlap && !_vm.iframe } ]},[_vm._ssrNode(((_vm.iframe)?("<div class=\"iframe\">"+(_vm._s(_vm.iframe))+"</div>"):"<!---->")+" "),(_vm.title || _vm.content)?_vm._ssrNode("<div class=\"hero-inner\">","</div>",[_vm._ssrNode("<div class=\"container text-center\">","</div>",[_vm._ssrNode("<div class=\"animation-overlay\"><h1 class=\"h2\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.title)+"\n        ")+"</h1></div> "),_vm._ssrNode("<div class=\"animation-overlay\">","</div>",[_vm._ssrNode(((!_vm.search)?("<div class=\"content\">"+(_vm._s(_vm.content))+"</div>"):"<!---->")+" "),(_vm.search)?_c('BaseSearch'):_vm._e()],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"animation-overlay\">","</div>",[_vm._ssrNode("<div class=\"animation-overlay-inner\">","</div>",[(_vm.buttons)?_c('BaseButtons',{attrs:{"label":_vm.buttonsLabel,"buttons":_vm.addVariantToButtons(_vm.buttons)}}):_vm._e()],1)])],2)]):_vm._e()],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseHero.vue?vue&type=template&id=72819f2d&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseHero.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseHerovue_type_script_lang_js_ = ({
  components: {
    BaseButtons: () => __webpack_require__.e(/* import() */ 1).then(__webpack_require__.bind(null, 314)),
    BaseSearch: () => __webpack_require__.e(/* import() */ 57).then(__webpack_require__.bind(null, 317))
  },
  props: {
    title: {
      type: [String, Boolean],
      default: false
    },
    content: {
      type: [String, Boolean],
      default: false
    },
    iframe: {
      type: [String, Boolean],
      default: false
    },
    image: {
      type: Object
    },
    buttonsLabel: {
      type: [String, Boolean],
      default: false
    },
    buttons: {
      type: [Array, Boolean],
      default: false
    },
    search: {
      type: Boolean,
      default: false
    },
    overlap: {
      type: Boolean,
      default: false
    }
  }
});
// CONCATENATED MODULE: ./components/BaseHero.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseHerovue_type_script_lang_js_ = (BaseHerovue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseHero.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(166)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseHerovue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "09a3481f"
  
)

/* harmony default export */ var BaseHero = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 77:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(167);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("eaa12cea", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=7.js.map