exports.ids = [39];
exports.modules = {

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseArchiveEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(78);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseArchiveEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseArchiveEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseArchiveEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseArchiveEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 169:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.archive-employee-wrapper .header-title-wrapper{margin-bottom:3.8rem}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseArchiveEmployee.vue?vue&type=template&id=9ca4d7be&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"archive-employee-wrapper"},[(_vm.groupItems)?_vm._ssrNode("<div class=\"by-area\">","</div>",_vm._l((_vm.employeesSortedByArea),function(employeeSection){return _vm._ssrNode("<div class=\"section-wrapper\">","</div>",[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode(((_vm.employeesSortedByArea.length > 1)?("<h2 class=\"header-title-wrapper text-center\">"+_vm._ssrEscape("\n          "+_vm._s(employeeSection.title)+"\n        ")+"</h2>"):"<!---->")+" "),(employeeSection.id === 'utbildningsledare')?_vm._ssrNode("<div>","</div>",_vm._l((employeeSection.area),function(location){return _vm._ssrNode("<div>","</div>",[(location.employees.length > 0)?_vm._ssrNode("<div class=\"location-wrapper\">","</div>",[_vm._ssrNode("<h3 class=\"header-title-wrapper text-center\">"+_vm._ssrEscape("\n                "+_vm._s(location.title)+"\n              ")+"</h3> "),_vm._ssrNode("<div class=\"row justify-content-center\">","</div>",_vm._l((location.employees),function(employee,index){return _vm._ssrNode("<div"+(_vm._ssrAttr("id",employee.post_name))+" class=\"col-md-3\">","</div>",[_c('BasePostEmployee',{attrs:{"image":employee.featured_image[_vm.getImageSize('tablet')],"name":employee.post_title,"title":employee.custom_fields.title,"phone":employee.custom_fields.phone,"mail":employee.custom_fields['e-post']}})],1)}),0)],2):_vm._e()])}),0):_vm._ssrNode("<div>","</div>",[_vm._ssrNode("<div class=\"row justify-content-center\">","</div>",_vm._l((employeeSection.employees),function(employee,index){return _vm._ssrNode("<div"+(_vm._ssrAttr("id",employee.post_title))+" class=\"col-md-3\">","</div>",[_c('BasePostEmployee',{attrs:{"id":employee.post_name,"image":employee.featured_image[_vm.getImageSize('tablet')],"name":employee.post_title,"title":employee.custom_fields.title,"phone":employee.custom_fields.phone,"mail":employee.custom_fields['e-post']}})],1)}),0)])],2)])}),0):_vm._ssrNode("<div class=\"list container\">","</div>",[_vm._ssrNode("<div class=\"row\">","</div>",_vm._l((_vm.employees),function(employee,index){return _vm._ssrNode("<div"+(_vm._ssrAttr("id",employee.post_title))+" class=\"col-md-3\">","</div>",[_c('BasePostEmployee',{attrs:{"id":employee.post_name,"image":employee.featured_image[_vm.getImageSize('tablet')],"name":employee.post_title,"title":employee.custom_fields.title,"phone":employee.custom_fields.phone,"mail":employee.custom_fields['e-post']}})],1)}),0)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseArchiveEmployee.vue?vue&type=template&id=9ca4d7be&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseArchiveEmployee.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseArchiveEmployeevue_type_script_lang_js_ = ({
  components: {
    BasePostEmployee: () => __webpack_require__.e(/* import() */ 53).then(__webpack_require__.bind(null, 318))
  },
  props: {
    employees: {
      type: [Object, Array]
    },
    groupItems: {
      type: Boolean,
      default: true
    }
  },
  computed: {
    employeesSortedByArea() {
      const res = [];
      const employees = this.employees;
      const employeesSorted = ['administration', 'utbildningsledare', 'affarsutveckling', 'marknad', 'hr', 'inkop-och-ekonomi', 'antagning', 'urmakarskolan', 'ledning'];
      Object.keys(employees).forEach(item => {
        if (!employeesSorted.includes(item)) {
          employeesSorted.push(item);
        }
      }); // Går inte att skapa nya områden pga ovan

      employeesSorted.forEach(key => {
        let valid = false;

        if (employees[key]) {
          if (!employees[key].area && employees[key].employees.length > 0) {
            valid = true;
          }

          if (employees[key].area) {
            Object.keys(employees[key].area).forEach(item => {
              if (employees[key].area[item].employees.length) {
                valid = true;
              }
            });
          }
        }

        if (valid) {
          res.push(employees[key]);
        }
      });
      return res;
    }

  },

  mounted() {
    const hash = window.location.hash;
    console.log(hash);

    if (hash !== '') {
      setTimeout(function () {
        window.location.href = hash;
      }, 500);
    }
  }

});
// CONCATENATED MODULE: ./components/BaseArchiveEmployee.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseArchiveEmployeevue_type_script_lang_js_ = (BaseArchiveEmployeevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseArchiveEmployee.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(168)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseArchiveEmployeevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "6552377b"
  
)

/* harmony default export */ var BaseArchiveEmployee = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(169);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("0519bc75", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=39.js.map