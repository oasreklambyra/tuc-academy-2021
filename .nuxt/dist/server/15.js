exports.ids = [15];
exports.modules = {

/***/ 125:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(263);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("1fa7c4f5", content, true, context)
};

/***/ }),

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCta_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(125);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCta_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCta_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCta_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCta_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 263:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.cta-wrapper .row{box-shadow:0 50px 60px -60px rgba(0,0,0,.4)}.cta-wrapper .content{min-height:450px;padding:5vmin;background-color:rgba(225,228,235,.25)}@media (max-width:767.98px){.cta-wrapper .content{min-height:0}}.cta-wrapper iframe{width:100%;height:100%}.cta-wrapper .background-image{height:100%;padding-top:50%;background-size:cover;background-position:50%}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCta.vue?vue&type=template&id=5eb1c370&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{ref:"wrapper",class:['cta-wrapper']},[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"row no-gutters box-shadow\">","</div>",[_vm._ssrNode("<div"+(_vm._ssrClass(null,['col-12 col-sm-7', { 'order-sm-1': _vm.textPos === 'left' }]))+">","</div>",[_c('client-only',[(_vm.youtube)?_c('iframe',{attrs:{"src":'https://www.youtube.com/embed/' + _vm.youtube,"frameborder":"0","allow":"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture","allowfullscreen":""}}):_vm._e(),_vm._v(" "),(_vm.image && !_vm.youtube)?_c('div',{directives:[{name:"lazy",rawName:"v-lazy:background-image",value:(_vm.image[_vm.getImageSize('laptop')]),expression:"image[getImageSize('laptop')]",arg:"background-image"}],ref:"image",staticClass:"background-image"},[(_vm.imageAttr && (_vm.imageAttr.author || _vm.imageAttr.logotype))?_c('BaseAuthor',{attrs:{"background":_vm.imageAttr.background,"logotype":_vm.imageAttr.logotype,"author":_vm.imageAttr.author}}):_vm._e()],1):_vm._e()])],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12 col-sm-5 \">","</div>",[_vm._ssrNode("<div"+(_vm._ssrClass(null,['content', 'bg-' + _vm.background]))+">","</div>",[_vm._ssrNode("<h5>"+_vm._ssrEscape(" "+_vm._s(_vm.subTitle))+"</h5> <h3>"+_vm._ssrEscape("\n            "+_vm._s(_vm.title)+"\n          ")+"</h3> <div>"+(_vm._s(_vm.content))+"</div> "),(_vm.buttonOptions)?_c('BaseButtons',{ref:"buttons",attrs:{"type":_vm.buttonOptions.type,"link":_vm.buttonOptions.link,"label":_vm.buttonOptions.label,"buttons":_vm.addVariantToButtons(_vm.buttonOptions.buttons)}}):_vm._e()],2)])],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseCta.vue?vue&type=template&id=5eb1c370&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCta.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseCtavue_type_script_lang_js_ = ({
  components: {
    BaseButtons: () => __webpack_require__.e(/* import() */ 1).then(__webpack_require__.bind(null, 314)),
    BaseAuthor: () => __webpack_require__.e(/* import() */ 5).then(__webpack_require__.bind(null, 353))
  },
  props: {
    background: {
      type: String,
      default: ''
    },
    textPos: {
      type: String,
      default: 'right'
    },
    title: {
      type: String
    },
    subTitle: {
      type: String
    },
    content: {
      type: String
    },
    youtube: {
      type: [String, Boolean],
      default: false
    },
    image: {
      type: [Object, Boolean],
      default: false
    },
    imageAttr: {
      type: [Object, Boolean],
      default: false
    },
    buttonOptions: {
      type: Object
    },
    buttons: {
      type: [Array, Boolean],
      default: false
    },
    buttonsLabel: {
      type: String
    }
  }
});
// CONCATENATED MODULE: ./components/BaseCta.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseCtavue_type_script_lang_js_ = (BaseCtavue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseCta.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(262)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseCtavue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "3165547b"
  
)

/* harmony default export */ var BaseCta = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=15.js.map