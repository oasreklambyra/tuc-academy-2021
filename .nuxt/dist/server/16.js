exports.ids = [16];
exports.modules = {

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BasePostArchive_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(76);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BasePostArchive_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BasePostArchive_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BasePostArchive_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BasePostArchive_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 165:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.post-archive-wrapper{padding:0 0 70px}.post-archive-wrapper .items-wrapper .post-course-wrapper{margin-bottom:5rem}.post-archive-wrapper .search-archive-wrapper{margin-bottom:3.7rem}.post-archive-wrapper .categories-wrapper .list-inline{border-top:1px solid rgba(160,169,186,.5);border-bottom:1px solid rgba(160,169,186,.5);padding:.7rem 0}.post-archive-wrapper .categories-wrapper .list-inline-item{cursor:pointer;margin:0;padding:1rem 1.2rem;color:#6c7175;transition:.2s}.post-archive-wrapper .categories-wrapper .list-inline-item:hover{opacity:.5}.post-archive-wrapper .categories-wrapper .list-inline-item.active{cursor:auto;color:#12499b}.post-archive-wrapper .categories-wrapper .list-inline-item.active:hover{opacity:1}.post-archive-wrapper .categories-wrapper .list-inline-item.active svg{fill:#12499b}.post-archive-wrapper .categories-wrapper .list-inline-item svg{margin-right:.5rem;width:1.5rem;height:1.5rem;vertical-align:text-top;fill:#6c7175}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BasePostArchive.vue?vue&type=template&id=1626c9cd&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"post-archive-wrapper"},[(!_vm.loading)?_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-3\">","</div>",[_c('BaseFilter',{attrs:{"filterOptions":_vm.filter,"filterActive":_vm.filterActive,"updatefilterActive":_vm.updatefilterActiveFromFilter}})],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-9\">","</div>",[_vm._ssrNode("<div class=\"search-archive-wrapper\">","</div>",[_c('b-form-input',{attrs:{"placeholder":"Sök utbildning"},model:{value:(_vm.filterActive.search),callback:function ($$v) {_vm.$set(_vm.filterActive, "search", $$v)},expression:"filterActive.search"}})],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"items-wrapper\">","</div>",[_vm._ssrNode("<div class=\"row\">","</div>",_vm._l((_vm.itemsFilterd),function(item){return _vm._ssrNode("<div class=\"col-12 col-sm-4\">","</div>",[_c('client-only',[_c('BasePost',{attrs:{"title":item.post_title,"content":item.post_excerpt,"image":item.featured_image,"url":item.permalink,"image-pos":item.custom_fields.img_pos}})],1)],1)}),0)])],2)],2):_c('BaseLoading')],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BasePostArchive.vue?vue&type=template&id=1626c9cd&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BasePostArchive.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BasePostArchivevue_type_script_lang_js_ = ({
  components: {
    BasePost: () => __webpack_require__.e(/* import() */ 9).then(__webpack_require__.bind(null, 315)),
    BaseLoading: () => __webpack_require__.e(/* import() */ 8).then(__webpack_require__.bind(null, 316)),
    BaseFilter: () => __webpack_require__.e(/* import() */ 6).then(__webpack_require__.bind(null, 299))
  },
  props: {
    category: {
      type: [Boolean, String],
      default: false
    }
  },

  data() {
    return {
      filterActive: {
        category: [],
        search: null
      }
    };
  },

  computed: {
    loading() {
      let res = false;

      if (!this.posts) {
        res = true;
      }

      return res;
    },

    items() {
      const res = [].concat(this.posts);
      return res.sort((a, b) => a.post_date < b.post_date ? 1 : b.post_date < a.post_date ? -1 : 0);
    },

    categories() {
      const res = [];
      const uniqueCheck = [];
      this.items.forEach(item => {
        if (item.post_taxs.programme_course_category) {
          item.post_taxs.programme_course_category.forEach(cat => {
            if (!uniqueCheck.includes(cat.slug)) {
              res.push({
                name: cat.name,
                value: cat.slug
              });
              uniqueCheck.push(cat.slug);
            }
          });
        }
      });
      return res;
    },

    filter() {
      const res = {
        category: {
          title: 'Kategori',
          options: []
        }
      }; // const forcedOrder = ['senaste-nytt', 'repotage', 'snabbfakta']

      const exlude = ['lediga-jobb', 'alumni'];
      const uniqueCheck = [];
      this.items.forEach(item => {
        Object.keys(res).forEach(cat => {
          if (item.post_taxs[cat]) {
            item.post_taxs[cat].forEach(itemCat => {
              if (!uniqueCheck.includes(itemCat.slug) && !exlude.includes(itemCat.slug)) {
                res[cat].options.push({
                  text: itemCat.name,
                  value: itemCat.slug
                });
                uniqueCheck.push(itemCat.slug);
              }
            });
          }
        });
      });
      res.category.options.reverse();
      return res;
    },

    itemsFilterd() {
      const res = [];
      const filterActive = this.filterActive;
      this.items.forEach(item => {
        const valid = {};
        Object.keys(filterActive).forEach(filter => {
          const filterSection = filterActive[filter];

          if (filterSection && filterSection.length > 0) {
            valid[filter] = false;

            if (filter !== 'search') {
              if (item.post_taxs[filter]) {
                item.post_taxs[filter].forEach(category => {
                  if (filterSection.includes(category.slug)) {
                    valid[filter] = true;
                  }
                });
              }
            }

            if (filter === 'search') {
              // title, excerpt, keyword
              const searchWord = filterSection.toLowerCase();
              let keyword = false;

              if (item.custom_fields && item.custom_fields.keywords) {
                keyword = item.custom_fields.keywords.toLowerCase().includes(searchWord);
              }

              if (item.post_title.toLowerCase().includes(searchWord) || item.post_excerpt.toLowerCase().includes(searchWord) || keyword) {
                valid[filter] = true;
              }
            }
          }
        });
        let allTrue = true;
        Object.keys(valid).forEach(item => {
          if (!valid[item]) {
            allTrue = false;
          }
        });

        if (allTrue) {
          res.push(item);
        }
      });
      return res;
    },

    posts() {
      return this.$store.state.cpt.post || [];
    }

  },
  watch: {
    filterActive: {
      handler(filter) {
        if (filter.search === null || filter.search === '') {
          delete filter.search;
        }

        console.log(filter);
        this.$router.replace({
          query: filter
        }).catch(() => {});
      },

      deep: true
    },

    $route(route) {
      this.populateFilterFromQuery(route.query);
    }

  },

  mounted() {
    const query = this.$route.query;
    console.log(query);

    if (Object.keys(query).length > 0) {
      this.populateFilterFromQuery(query);
    }

    if (!this.$store.state.cpt.post) {
      this.$store.dispatch({
        type: 'getCpt',
        postType: 'post'
      });
    }
  },

  methods: {
    updatefilterActiveFromFilter(filterActive) {
      console.log(filterActive);
      this.filterActive = filterActive;
    },

    populateFilterFromQuery(query) {
      Object.keys(query).forEach(key => {
        if (typeof query[key] === 'string' && key !== 'search') {
          query[key] = [query[key]];
        }

        this.filterActive[key] = query[key];
      });
    }

  }
});
// CONCATENATED MODULE: ./components/BasePostArchive.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BasePostArchivevue_type_script_lang_js_ = (BasePostArchivevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BasePostArchive.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(164)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BasePostArchivevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "df101b26"
  
)

/* harmony default export */ var BasePostArchive = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(165);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("7c2cc5b5", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=16.js.map