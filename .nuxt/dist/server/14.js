exports.ids = [14];
exports.modules = {

/***/ 184:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseIntro_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(86);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseIntro_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseIntro_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseIntro_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseIntro_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 185:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}#form-popup{opacity:0;position:fixed;top:70px;left:0;width:100%;display:flex;justify-content:center;align-items:center;flex-wrap:wrap;height:calc(100vh - 70px);background:rgba(248,248,255,.95);pointer-events:none;transition:all .15s ease}#form-popup.active{opacity:1;pointer-events:auto;z-index:2}#form-popup.active .cf-wrapper{opacity:1;transform:translateY(0);transition:all .15s ease;transition-delay:.15s}#form-popup .form-popup-inner{width:100%}#form-popup .cf-wrapper{opacity:0;transform:translateY(-20px)}#form-popup .cf-wrapper .form-top{margin-bottom:2rem}#form-popup .cf-wrapper input,#form-popup .cf-wrapper label,#form-popup .cf-wrapper textarea{width:100%;margin-left:0}.fixed-apply-button{position:fixed;bottom:3rem;width:100%;z-index:9;display:flex;justify-content:center;align-items:center}.fixed-apply-button button.btn{width:auto;margin-left:1rem;background:linear-gradient(289deg,#12499b 53%,#934d98);color:#fff;padding:1rem 2rem;font-size:1.25rem}.fixed-apply-button button.btn:hover{color:#fff;text-decoration:none;box-shadow:0 10px 14px -2px rgba(0,0,0,.5)}.fixed-apply-button a.btn{width:auto;background-image:linear-gradient(289deg,#12499b 53%,#934d98);color:#fff;padding:1rem 2rem;font-size:1.25rem}.fixed-apply-button a.btn:hover{color:#fff;text-decoration:none;box-shadow:0 10px 14px -2px rgba(0,0,0,.5)}.course-intro-wrapper{padding-top:70px}.course-intro-wrapper .category-wrapper h4{color:#878787}.course-intro-wrapper .category-wrapper hr{width:20px;border-color:#878787}.course-intro-wrapper .lead-text{margin-bottom:5rem}.course-intro-wrapper .content{color:#878787;margin-bottom:6.5rem}.course-intro-wrapper .content .lead{font-size:inherit;font-weight:inherit;line-height:inherit}.course-intro-wrapper .iframe-wrapper{padding-top:56%;position:relative}.course-intro-wrapper .iframe-wrapper iframe{position:absolute;top:0;left:0;right:0;bottom:0;width:100%!important;height:100%!important}.course-intro-wrapper .background-image{padding-top:52%;background-position:50%;background-size:cover}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCourseIntro.vue?vue&type=template&id=3db17413&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"course-intro-wrapper"},[_vm._ssrNode("<div id=\"form-popup\""+(_vm._ssrClass(null,[_vm.isActive ? 'active' : 'noActive']))+">","</div>",[(true)?_c('BaseContactForm',{attrs:{"id":7186,"theTitle":_vm.title}}):undefined],1),_vm._ssrNode(" "+((_vm.applyactive === true || _vm.quote === true)?("<div class=\"fixed-apply-button\">"+((_vm.applyactive)?("<a"+(_vm._ssrAttr("href",_vm.applylink))+" target=\"_blank\" rel=\"noopener\" class=\"btn\">Anmäl dig till webbutbildning</a>"):"<!---->")+" "+((_vm.quote)?("<button"+(_vm._ssrAttr("href",_vm.applylink))+" target=\"_blank\" rel=\"noopener\" class=\"btn\">"+((_vm.isActive === false)?("<span>Be om en offert</span>"):("<span>Stäng</span>"))+"</button>"):"<!---->")+"</div>"):"<!---->")+" <div class=\"container text-center\">"+_vm._ssrEscape("\n    "+_vm._s(_vm.quote)+"\n    "+_vm._s(_vm.applyactive)+"\n    ")+((_vm.category)?("<div class=\"category-wrapper\"><h4 class=\"category title\">"+(_vm._s(_vm.category))+"</h4> <hr></div>"):"<!---->")+" <h1 class=\"text-primary\">"+_vm._ssrEscape("\n      "+_vm._s(_vm.title)+"\n    ")+"</h1> <h2 class=\"lead-text\">"+_vm._ssrEscape("\n      "+_vm._s(_vm.subTitle)+"\n    ")+"</h2> <div class=\"content title\">"+(_vm._s(_vm.content))+"</div> "+((_vm.video && _vm.video !== '')?("<div class=\"iframe-wrapper\">"+(_vm._s(_vm.video))+"</div>"):("<div class=\"background-image\"></div>"))+"</div>")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseCourseIntro.vue?vue&type=template&id=3db17413&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCourseIntro.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseCourseIntrovue_type_script_lang_js_ = ({
  components: {
    BaseContactForm: () => __webpack_require__.e(/* import() */ 3).then(__webpack_require__.bind(null, 300))
  },
  props: {
    title: {
      type: String,
      default: ''
    },
    category: {
      type: [String, Boolean],
      default: ''
    },
    subTitle: {
      type: String,
      default: ''
    },
    content: {
      type: String,
      default: ''
    },
    image: {
      type: String,
      default: ''
    },
    video: {
      type: String,
      default: ''
    },
    applyactive: {
      type: Boolean,
      default: false
    },
    applylink: {
      type: String,
      default: ''
    },
    quote: {
      type: Boolean,
      default: false
    },
    theTitle: {
      type: [String, Boolean],
      default: false
    }
  },

  data() {
    return {
      isActive: false,
      tieTitle: false
    };
  },

  watch: {
    $route() {
      this.isActive = false;
    }

  },
  methods: {
    toggleClass(event) {
      if (this.isActive) {
        this.isActive = false;
      } else {
        this.isActive = true;
      }
    }

  }
});
// CONCATENATED MODULE: ./components/BaseCourseIntro.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseCourseIntrovue_type_script_lang_js_ = (BaseCourseIntrovue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseCourseIntro.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(184)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseCourseIntrovue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "1ad855fc"
  
)

/* harmony default export */ var BaseCourseIntro = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(185);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("884b2256", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=14.js.map