exports.ids = [3];
exports.modules = {

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContactform_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(80);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContactform_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContactform_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContactform_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContactform_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 173:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.cf-wrapper{box-shadow:0 2px 4px 0 rgba(0,0,0,.5);background:#fff;padding:2rem;position:relative;max-width:920px;margin:0 auto}.cf-wrapper input[type=checkbox]{width:auto}.cf-wrapper label{margin-left:.5rem}.cf-wrapper .wpcf7-list-item{margin:0}.cf-wrapper .wpcf7-file{width:100%}.cf-wrapper .wpcf7-acceptance{margin:1rem 0;display:block}.cf-wrapper .wpcf7-acceptance input{margin:0;border-radius:10px}.cf-wrapper .wpcf7-acceptance *{font-size:1rem}.cf-wrapper .btn{display:block;width:100%}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseContactform.vue?vue&type=template&id=3cfd17da&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"cf-wrapper"},[_c('client-only',[_c('div',{staticClass:"form-wrapper",domProps:{"innerHTML":_vm._s(_vm.formHtml)}})])],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseContactform.vue?vue&type=template&id=3cfd17da&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseContactform.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
/* harmony default export */ var BaseContactformvue_type_script_lang_js_ = ({
  /****************************************************************************************/

  /*                                                                                      */

  /* Add following to middleware/getSupportData.js                                        */

  /* await context.app.store.dispatch({ type: 'getContactform', id: [ID], lang: [LANG] }) */

  /*                                                                                      */

  /****************************************************************************************/
  props: {
    id: {
      type: [Number, String]
    },
    theTitle: String
  },
  computed: {
    form() {
      return this.$store.state.cf7.cf[this.id] || {};
    },

    formHtml() {
      return this.form.html;
    },

    formScripts() {
      return this.form.js;
    },

    formStyle() {
      return this.form.css;
    }

  },
  watch: {
    theTitle: {
      immediate: true,

      handler() {
        if (false) {}
      }

    }
  },

  mounted() {
    // Needs to load after form html
    setTimeout(() => {
      const cfController = document.createElement('script');
      cfController.setAttribute('src', this.formScripts.scripts);
      document.body.appendChild(cfController);
    }, 500);
  },

  head() {
    return {
      link: [{
        rel: 'stylesheet',
        type: 'text/css',
        href: this.formStyle.main
      }],
      script: [{
        hid: 'jquery',
        async: true,
        type: 'text/javascript',
        src: this.formScripts.jquery
      }, {
        hid: 'jquery-migrate',
        async: true,
        type: 'text/javascript',
        src: this.formScripts['jquery-migrate']
      }, {
        hid: 'wpcf7',
        type: 'text/javascript',
        innerHTML: this.formScripts.wpcf7
      }],
      __dangerouslyDisableSanitizersByTagID: {
        wpcf7: ['innerHTML']
      }
    };
  }

});
// CONCATENATED MODULE: ./components/BaseContactform.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseContactformvue_type_script_lang_js_ = (BaseContactformvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseContactform.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(172)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseContactformvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "384e3362"
  
)

/* harmony default export */ var BaseContactform = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 80:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(173);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("405dd916", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=3.js.map