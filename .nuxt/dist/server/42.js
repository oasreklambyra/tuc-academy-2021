exports.ids = [42];
exports.modules = {

/***/ 180:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContent_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(84);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContent_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContent_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContent_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContent_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 181:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.content-wrapper .readmore-link{display:block;margin:1vmin 0;font-weight:700}.content-wrapper .readmore-wrapper{height:0;opacity:0;overflow:hidden}.content-wrapper .readmore-wrapper.active{height:auto;opacity:1}.content-wrapper .container .bg-wrapper{padding:5vmin 0}.content-wrapper .container .bg-wrapper h1{margin-bottom:0}.content-wrapper .container .bg-wrapper.bg-primary{background-image:linear-gradient(289deg,#12499b 53%,#934d98);color:#fff;padding:0 4vmin}.content-wrapper .container .content-inner{padding:0 4vmin;max-width:920px;margin-right:auto;margin-left:auto}.content-wrapper .container .content-inner table{max-width:100%;height:auto!important}@media (max-width:575.98px){.content-wrapper .container .content-inner table,.content-wrapper .container .content-inner table tr{display:block}}.content-wrapper .container .content-inner table th{padding:0 20px}@media (max-width:575.98px){.content-wrapper .container .content-inner table th{width:100%;display:block}}.content-wrapper .container .content-inner table h1{font-size:24px}.content-wrapper .container .content-inner table h4{font-size:16px;margin-top:10px}.content-wrapper .container .buttons-wrapper{max-width:920px;margin:3vmin auto 2vmin;padding:0 4vmin}@media (max-width:575.98px){.content-wrapper .container .buttons-wrapper.button a{width:100%;margin:0 0 1rem}}.content-wrapper .alignleft{float:left;margin-left:0}.content-wrapper .alignright{float:right;margin-right:0}.content-wrapper .wp-caption-text{margin:0 2rem}.content-wrapper img{opacity:1;margin:0 2rem .5rem;max-width:calc(100% - 4rem);height:auto}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 304:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseContent-edu.vue?vue&type=template&id=7083ef6c&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"content-wrapper"},[_vm._ssrNode("<div class=\"container\"><div"+(_vm._ssrClass(null,[{ 'bg-wrapper': _vm.background !== '' && _vm.background !== 'transparent' }, 'bg-' + _vm.background]))+"><div class=\"content-inner\"><p style=\"margin-bottom: -2rem;\">\n          Våra kurser inom\n        </p> <h1>"+_vm._ssrEscape(_vm._s(_vm.title))+"</h1> <p style=\"text-align: center;\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.content)+"\n        ")+"</p></div></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseContent-edu.vue?vue&type=template&id=7083ef6c&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseContent-edu.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseContent_eduvue_type_script_lang_js_ = ({
  components: {// BaseButtons: () => import('~/components/BaseButtons.vue')
  },
  props: {
    content: {
      type: String
    },
    title: {
      type: String
    },
    background: {
      type: String,
      default: ''
    }
  },

  data() {
    return {
      showReadmore: false,
      textAlign: null
    };
  },

  computed: {
    contentFormated() {
      const content = this.content;
      let res = '';

      if (content.includes('<p><!--more--></p>')) {
        const splitedContent = content.split('<p><!--more--></p>');
        res += splitedContent[0];

        if (!this.showReadmore) {
          res += '<a href="#readmore" class="readmore-link">Läs mer</a>';
        }

        res += '<div class="readmore-wrapper ' + (this.showReadmore ? 'active' : '') + '">';
        res += splitedContent[1];
        res += '</div>';

        if (this.showReadmore) {
          res += '<a href="#readless" class="readmore-link">Läs mindre</a>';
        }
      } else {
        res = content;
      }

      if (content.includes('id="')) {
        res = res.replace('id="', 'style="padding-top: 75px; margin-top: -75px;" id="');
      }

      return res;
    },

    hasOptions() {
      const buttonOptions = this.buttonOptions;
      return buttonOptions && (buttonOptions.type === 'button' && buttonOptions.buttons || buttonOptions.type === 'link' && buttonOptions.link);
    }

  },
  watch: {
    $route(route) {
      if (route.hash.includes('readmore')) {
        this.showReadmore = true;
      }

      if (route.hash.includes('readless')) {
        this.showReadmore = false;
      }
    }

  },

  mounted() {
    const content = this.$refs.content;
    let res = null;

    if (content && content.firstChild && content.firstChild.style && content.firstChild.style.textAlign) {
      res = content.firstChild.style.textAlign;
    }

    this.textAlign = res;
  }

});
// CONCATENATED MODULE: ./components/BaseContent-edu.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseContent_eduvue_type_script_lang_js_ = (BaseContent_eduvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseContent-edu.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(180)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseContent_eduvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "6cff22cd"
  
)

/* harmony default export */ var BaseContent_edu = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 84:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(181);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("3c3a99b5", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=42.js.map