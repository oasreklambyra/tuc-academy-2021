exports.ids = [63];
exports.modules = {

/***/ 100:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(213);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("264f4f21", content, true, context)
};

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderArchivePost_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(100);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderArchivePost_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderArchivePost_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderArchivePost_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderArchivePost_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 213:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/PagebuilderArchivePost.vue?vue&type=template&id=f341596a&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"pagebuilder-archive-wrapper"},[_c('BaseBlockWrapper',{attrs:{"title":_vm.data.title,"sub-title":_vm.data.sub_title,"content":_vm.data.content,"button-options":{
      type: _vm.data.link_or_button,
      link: _vm.data.link,
      buttonsLabel: _vm.data.label,
      buttons: _vm.data.buttons
    },"alignment":_vm.data.text_pos}},[_c('div',{staticClass:"row justify-content-center"},_vm._l((_vm.postFilterd),function(post){return _c('div',{key:post.ID,staticClass:"col-12 col-sm-3 slot-item mb-5"},[_c('BasePostPost',{attrs:{"title":post.post_title,"content":post.post_excerpt,"date":post.post_date,"url":post.permalink}})],1)}),0)])],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/PagebuilderArchivePost.vue?vue&type=template&id=f341596a&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/PagebuilderArchivePost.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var PagebuilderArchivePostvue_type_script_lang_js_ = ({
  components: {
    BaseBlockWrapper: () => __webpack_require__.e(/* import() */ 0).then(__webpack_require__.bind(null, 340)),
    BasePostPost: () => __webpack_require__.e(/* import() */ 55).then(__webpack_require__.bind(null, 342))
  },
  props: {
    data: {
      type: Object
    }
  },
  computed: {
    postFilterd() {
      // Setup
      const res = [];
      const activeCategory = this.data.post_category;

      if (activeCategory) {
        this.posts.forEach(post => {
          let valid = false;

          if (post.post_taxs.category) {
            post.post_taxs.category.forEach(cat => {
              activeCategory.forEach(activeCat => {
                if (cat.slug === activeCat.slug) {
                  valid = true;
                }
              });
            });
          }

          if (valid) {
            res.push(post);
          }
        });
      } // return


      return res.sort((a, b) => a.post_date < b.post_date ? 1 : b.post_date < a.post_date ? -1 : 0).slice(0, 4);
    },

    posts() {
      return this.$store.state.cpt.post || [];
    }

  },

  mounted() {
    if (!this.$store.state.cpt.post) {
      this.$store.dispatch({
        type: 'getCpt',
        postType: 'post'
      });
    }
  }

});
// CONCATENATED MODULE: ./components/PagebuilderArchivePost.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_PagebuilderArchivePostvue_type_script_lang_js_ = (PagebuilderArchivePostvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/PagebuilderArchivePost.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(212)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_PagebuilderArchivePostvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "4f99bc2c"
  
)

/* harmony default export */ var PagebuilderArchivePost = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=63.js.map