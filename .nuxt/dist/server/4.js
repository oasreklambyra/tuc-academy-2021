exports.ids = [4];
exports.modules = {

/***/ 121:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(255);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("1e16e84d", content, true, context)
};

/***/ }),

/***/ 254:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BasePostCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(121);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BasePostCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BasePostCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BasePostCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BasePostCourse_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 255:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.post-course-wrapper{transition:.4s ease-in-out;cursor:pointer;margin-bottom:2vmin}.post-course-wrapper a{text-decoration:none}.post-course-wrapper h5{color:#1d1d1d;transition:.4s ease-in-out}.post-course-wrapper .content{transition:.4s ease-in-out;color:#888988}.post-course-wrapper .background-image-wrapper{position:relative}.post-course-wrapper .background-image-wrapper .background-image{transition:.4s ease-in-out;padding-top:126%;background-size:cover;background-position:50%}.post-course-wrapper .background-image-wrapper .tags-wrapper{z-index:10;position:absolute}.post-course-wrapper .background-image-wrapper .tags-wrapper.upper{top:.5rem;right:.5rem}.post-course-wrapper .background-image-wrapper .tags-wrapper.lower{bottom:.5rem;left:.5rem;right:.5rem}.post-course-wrapper:hover .content,.post-course-wrapper:hover h5{color:#12499b;text-decoration:none}.post-course-wrapper:hover .background-image{opacity:.5}.course-modal .btn .badge{position:absolute;top:0;right:0;transform:translate(-50%,50%)}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BasePostCourse.vue?vue&type=template&id=5d429fed&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"post-course-wrapper"},[(!_vm.permalink.includes('%'))?_c(!_vm.isGroup ? 'nuxt-link' : 'a',{tag:"nuxt-link",attrs:{"to":_vm.permalink},on:{"click":function($event){return _vm.openModal()}}},[_c('div',{staticClass:"background-image-wrapper"},[_c('client-only',[_c('div',{directives:[{name:"lazy",rawName:"v-lazy:background-image",value:(
            _vm.image ? _vm.image[_vm.getImageSize('desktop')] : false
          ),expression:"\n            image ? image[getImageSize('desktop')] : false\n          ",arg:"background-image"}],staticClass:"background-image",style:({
            'background-position-x':
              _vm.imagePos && _vm.imagePos.endsWith('%') ? _vm.imagePos : _vm.imagePos + '%',
          })})]),_vm._v(" "),_c('BaseTags',{staticClass:"upper",attrs:{"tags":_vm.formatUpperTags,"variant":"dark"}}),_vm._v(" "),_c('BaseTags',{staticClass:"lower",attrs:{"tags":_vm.formatTags,"variant":"light"}})],1),_vm._v(" "),_c('h5',{staticClass:"font-weight-bold"},[_vm._v("\n      "+_vm._s(_vm.title)+"\n    ")]),_vm._v(" "),_c('div',{staticClass:"content",domProps:{"innerHTML":_vm._s(_vm.content.substring(0, 150))}})]):_c('div',{staticClass:"error-wrapper"},[_vm._v("\n    Error: undefined_category. "),_c('br'),_vm._v("\n    Kursen "+_vm._s(_vm.title)+" saknar kurs typ\n  ")]),_vm._ssrNode(" "),_c('b-modal',{attrs:{"id":'modal-post-course-' + _vm.isGroup,"title":_vm.title + ' finns på flera orter. Vilken är det du söker?',"hide-footer":"","size":"xl"}},[_c('div',{staticClass:"row course-modal "},_vm._l((_vm.siblings),function(sibling){return _c('div',{key:sibling.url,staticClass:"col-12 col-sm-6 mt-3"},[_c('b-button',{attrs:{"to":sibling.url,"block":"","variant":"primary"},domProps:{"innerHTML":_vm._s(sibling.title)}}),_vm._v(" "),(sibling.can_apply || _vm.getPeriod(sibling.post_taxs.programme_course_period))?_c('label',{staticClass:"text-center mt-2 d-block"},[(_vm.getPeriod(sibling.post_taxs.programme_course_period))?_c('span',[_vm._v(_vm._s(_vm.getPeriod(sibling.post_taxs.programme_course_period)))]):_vm._e(),_vm._v(" "),(sibling.can_apply && _vm.getPeriod(sibling.post_taxs.programme_course_period))?_c('span',[_vm._v("-")]):_vm._e(),_vm._v(" "),(sibling.can_apply)?_c('span',[_vm._v("Ansökan är öppen!")]):_vm._e()]):_vm._e()],1)}),0)])],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BasePostCourse.vue?vue&type=template&id=5d429fed&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BasePostCourse.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BasePostCoursevue_type_script_lang_js_ = ({
  components: {
    BaseTags: () => __webpack_require__.e(/* import() */ 10).then(__webpack_require__.bind(null, 352))
  },
  props: {
    title: {
      type: String
    },
    image: {
      type: [Object, Boolean],
      default: false
    },
    imagePos: {
      type: String,
      default: '50%'
    },
    content: {
      type: String
    },
    upperTags: {
      type: Array
    },
    tags: {
      type: Array
    },
    yh: {
      type: Boolean,
      default: false
    },
    isGroup: {
      type: [Boolean, String],
      default: false
    },
    siblings: {
      type: [Boolean, Array],
      default: false
    },
    permalink: {
      type: String,
      default: '#'
    }
  },

  data() {
    return {
      loadModal: false,
      categoryIcons: {
        'ekonomi-administration-forsaljning': 'book-open',
        'bygg-anlaggning-fastighet': 'jackhammer',
        'data-it': 'processor',
        'hotell-restaurang-turism': 'thumbs-up',
        'teknik-tillverkning-drift-underhall': 'cog',
        'halso-sjukvard-socialt-arbete': 'medical-square'
      }
    };
  },

  computed: {
    formatUpperTags() {
      const res = [];
      this.upperTags.forEach(cat => {
        let name = '';

        if (cat.category === 'programme_course_meets') {
          name = cat.name + ' träffar';
        }

        if (cat.category === 'programme_course_internship') {
          name = 'Praktik ' + cat.name;
        }

        res.push({
          icon: false,
          name,
          value: cat.value
        });
      });
      return res;
    },

    formatTags() {
      let speed = false;
      let category = false;
      let distance = false;
      let area = false;
      let yh = false;
      const form = [];
      let forminner = false;
      this.tags.forEach(cat => {
        //        if (cat.category === 'programme_course_form' && !form) {
        if (cat.category === 'programme_course_form') {
          let icon = 'books';
          icon = cat.value === 'distans' ? 'clock-three' : icon;
          icon = cat.value === 'webb' ? 'desktop' : icon;
          icon = cat.value === 'platsbunden' ? 'building' : icon;
          icon = cat.value === 'kombinerad' ? 'file-plus' : icon;
          forminner = {
            icon,
            name: cat.name,
            category: cat.category,
            value: cat.value
          };
          form.push(forminner);
        }

        if (cat.category === 'programme_course_speed' && !speed) {
          speed = {
            icon: 'clock',
            name: cat.name,
            category: cat.category,
            value: cat.value
          };
        }

        if (cat.category === 'programme_course_category' && !category) {
          category = {
            icon: 'bright',
            name: cat.name,
            category: cat.category,
            value: cat.value
          };
        }

        if (cat.category === 'programme_course_area' && cat.value !== 'distans' && !area) {
          area = {
            icon: 'location-pin',
            name: cat.name,
            category: cat.category,
            value: cat.value
          };
        }

        if (cat.category === 'programme_course_area' && cat.value === 'distans' && !distance) {
          distance = {
            icon: 'books',
            name: cat.name,
            category: cat.category,
            value: cat.value
          };
        }
      });

      if (this.yh) {
        yh = {
          icon: 'graduation-cap',
          name: 'YH-Utbildning',
          value: 'yh-utbildning'
        };
      } // speed
      // Kategori
      // Distans
      // Ort
      // YH-Utbildning


      return [speed, category, distance, area, yh].concat(form);
    }

  },
  methods: {
    openModal() {
      this.$root.$emit('bv::show::modal', 'modal-post-course-' + this.isGroup); // this.$bvModal.show('modal-post-course-' + this.isGroup)
    },

    getPeriod(tax) {
      if (tax && tax[0]) {
        return tax[0].name;
      } else {
        return false;
      }
    }

  }
});
// CONCATENATED MODULE: ./components/BasePostCourse.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BasePostCoursevue_type_script_lang_js_ = (BasePostCoursevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BasePostCourse.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(254)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BasePostCoursevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "1c40a280"
  
)

/* harmony default export */ var BasePostCourse = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=4.js.map