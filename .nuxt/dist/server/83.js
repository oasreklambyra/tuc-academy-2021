exports.ids = [83];
exports.modules = {

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(93);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 199:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}@media (max-width:1150px){.menu-wrapper{display:flex;justify-content:flex-end}}.menu-wrapper .menu-buttons-wrapper{display:flex;justify-content:space-between;align-items:center}@media (max-width:575.98px){.menu-wrapper .menu-buttons-wrapper{float:right}}.menu-wrapper .menu-buttons{display:flex;justify-content:flex-end;align-items:center;align-self:flex-end}.menu-wrapper .menu-buttons a{margin-left:1rem;display:flex}@media (max-width:575.98px){.menu-wrapper .menu-buttons a{order:1!important}}.menu-wrapper .menu-buttons .search-btn{width:40px;height:40px;padding:0}.menu-wrapper .menu-buttons #burger{width:80px;height:20px;position:relative;top:0;z-index:999;display:none;overflow:hidden;margin-left:10px;cursor:pointer}@media (max-width:1150px){.menu-wrapper .menu-buttons #burger{display:flex;order:2}}.menu-wrapper .menu-buttons #burger .span-wrapper{width:48%;height:40px;position:absolute;top:0;left:0;transition:all .15s ease}.menu-wrapper .menu-buttons #burger .span-wrapper *{text-transform:uppercase}.menu-wrapper .menu-buttons #burger .span-wrapper .one{position:absolute;width:100%;top:0;left:0;height:20px;display:flex;align-items:center;transition:all .15s ease}.menu-wrapper .menu-buttons #burger .span-wrapper .two{width:100%;position:absolute;bottom:0;left:0;height:20px;display:flex;align-items:center;transition:all .15s ease}.menu-wrapper .menu-buttons #burger .lines-wrapper{width:40%;height:100%;position:absolute;top:0;right:0;overflow:hidden;transform:scale(.6)}.menu-wrapper .menu-buttons #burger .lines-wrapper span.top{position:absolute;top:0;left:0;width:100%;height:2px;background:#1d1d1d;transition:all .15s ease}.menu-wrapper .menu-buttons #burger .lines-wrapper span.middle{position:absolute;top:50%;left:0;margin-top:-1px;width:100%;height:2px;background:#1d1d1d;transition:all .15s ease}.menu-wrapper .menu-buttons #burger .lines-wrapper span.middle:before{content:\"\";position:absolute;top:50%;left:0;width:100%;height:2px;background:#12499b;transform:rotate(0deg);transition:all .15s ease;visibility:hidden}.menu-wrapper .menu-buttons #burger .lines-wrapper span.bottom{position:absolute;bottom:0;left:0;width:100%;height:2px;background:#1d1d1d;transition:all .15s ease}.menu-wrapper .menu-buttons #burger.active span.top{background:#12499b;transform:translateX(100%)}.menu-wrapper .menu-buttons #burger.active span.middle{background:#12499b;transform:rotate(45deg)}.menu-wrapper .menu-buttons #burger.active span.middle:before{visibility:visible;transform:rotate(-90deg)}.menu-wrapper .menu-buttons #burger.active span.bottom{background:#12499b;transform:translateX(-100%)}.menu-wrapper .menu-buttons #burger.active .span-wrapper{transform:translateY(-50%)}@media (max-width:1150px){.menu-wrapper .menu-buttons button{display:none}}.menu-wrapper .menu-items{display:flex}@media (max-width:1150px){.menu-wrapper .menu-items{display:none}}.menu-wrapper .menu-items .menu-item{position:relative}.menu-wrapper .menu-items .menu-item a{color:#1d1d1d;font-weight:700;text-decoration:none;padding:1rem 0}.menu-wrapper .menu-items .menu-item a svg{transform:translateY(-1px)}.menu-wrapper .menu-items .menu-item a:hover{color:#12499b}.menu-wrapper .menu-items .menu-item a:hover svg{fill:#12499b}.menu-wrapper .menu-items .menu-item .show-more-icon{width:1.5rem;height:1.5rem}.menu-wrapper .menu-items .menu-item .sub-menu-items{opacity:0;position:absolute;top:100%;left:-1.5rem;z-index:100;background-color:hsla(0,0%,100%,.95);padding:2rem 1.5rem 1rem;width:250%;pointer-events:none}.menu-wrapper .menu-items .menu-item .sub-menu-items li a{padding:.25rem 0;display:inline-block;color:#12499b}.menu-wrapper .menu-items .menu-item .sub-menu-items li a:hover{text-decoration:underline}.menu-wrapper .menu-items .menu-item:hover .sub-menu-items{opacity:1;pointer-events:all}.menu-wrapper #burger-menu{position:fixed;top:70px;left:0;width:100%;height:calc(100vh - 70px);z-index:-9999;opacity:0;background:#fff;transition:transform .4s ease;display:flex;justify-content:center;align-items:flex-start;padding-top:70px;display:none}.menu-wrapper #burger-menu.active{opacity:1;z-index:1;display:flex}.menu-wrapper #burger-menu .container{display:flex;justify-content:center;align-items:flex-start;flex-wrap:wrap;width:100%}@media (max-width:1440px){.menu-wrapper #burger-menu .container{width:90%;max-width:90%}}.menu-wrapper #burger-menu .container *{text-align:center}.menu-wrapper #burger-menu .container .menu-buttons{width:100%;justify-content:center;margin-top:6vmin}.menu-wrapper #burger-menu .container .menu-buttons button{margin:0 2vmin}@media (max-width:1150px){.menu-wrapper #burger-menu .container .menu-buttons button{display:block}}.menu-wrapper #burger-menu .menu-item{width:33.33%}@media (max-width:960px){.menu-wrapper #burger-menu .menu-item{width:100%}}.menu-wrapper #burger-menu .menu-item *{color:#1d1d1d;z-index:-1;position:relative}.menu-wrapper #burger-menu .menu-item .menu-item-heading{font-size:1.2em;display:flex;margin-bottom:.5vmin;font-weight:700;text-align:center;display:block;position:relative;z-index:2}.menu-wrapper #burger-menu .menu-item ul{display:flex;flex-wrap:wrap;margin-bottom:2vmin;position:relative;z-index:2}.menu-wrapper #burger-menu .menu-item ul li{width:100%;margin-bottom:.3vmin;position:relative;z-index:2}.menu-wrapper #burger-menu .menu-item ul li a{position:relative;z-index:2}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/TheMenu.vue?vue&type=template&id=205780d4&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"menu-wrapper"},[_vm._ssrNode("<div class=\"row menu-buttons-wrapper\">","</div>",[_vm._ssrNode("<div class=\"col-9 align-self-center\">","</div>",[_vm._ssrNode("<div class=\"menu\">","</div>",[_vm._ssrNode("<div class=\"menu-items justify-content-around\">","</div>",_vm._l((_vm.menu.menu_items),function(item,i){return _vm._ssrNode("<div class=\"menu-item\">","</div>",[(item.page)?_c('nuxt-link',{attrs:{"to":item.page.url}},[_vm._v("\n              "+_vm._s(item.page.title)+"\n              "),(item.sub_pages.length > 0)?_c('svg-icon',{staticClass:"show-more-icon",attrs:{"name":"angle-down"}}):_vm._e()],1):_vm._e(),_vm._ssrNode(" "),(item.sub_pages)?_vm._ssrNode("<ul class=\"sub-menu-items list-unstyled\">","</ul>",_vm._l((item.sub_pages),function(subItem,j){return _vm._ssrNode("<li>","</li>",[(subItem.sub_page)?_c('nuxt-link',{attrs:{"to":subItem.sub_page.url}},[_vm._v("\n                  "+_vm._s(subItem.sub_page.title)+"\n                ")]):_vm._e()],1)}),0):_vm._e()],2)}),0)])]),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"menu-buttons-wrapper\">","</div>",[_vm._ssrNode("<div class=\"menu-buttons\">","</div>",[_vm._ssrNode("<div id=\"burger\""+(_vm._ssrClass(null,[_vm.isActive ? 'active' : 'noActive']))+"><div class=\"span-wrapper\"><div class=\"one\"><span>Meny</span></div> <div class=\"two\"><span>Stäng</span></div></div> <div class=\"lines-wrapper\"><span class=\"top\"></span> <span class=\"middle\"></span> <span class=\"bottom\"></span></div></div> "),_c('b-button',{staticClass:"mr-3 btn-round search-btn",attrs:{"variant":"dark","size":"sm"},on:{"click":function($event){return _vm.goToSearch()}}},[_c('svg-icon',{attrs:{"name":"search"}})],1)],2)]),_vm._ssrNode(" "),_vm._ssrNode("<div id=\"burger-menu\""+(_vm._ssrClass(null,[_vm.isActive ? 'active' : 'noActive']))+">","</div>",[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._l((_vm.menu.menu_items),function(item,i){return _vm._ssrNode("<div class=\"menu-item\">","</div>",[(item.page)?_c('nuxt-link',{staticClass:"menu-item-heading",attrs:{"to":item.page.url}},[_vm._v("\n            "+_vm._s(item.page.title)+"\n          ")]):_vm._e(),_vm._ssrNode(" "),(item.sub_pages)?_vm._ssrNode("<ul class=\"sub-menu-items list-unstyled\">","</ul>",_vm._l((item.sub_pages),function(subItem,j){return _vm._ssrNode("<li>","</li>",[(subItem.sub_page)?_c('nuxt-link',{attrs:{"to":subItem.sub_page.url}},[_vm._v("\n                "+_vm._s(subItem.sub_page.title)+"\n              ")]):_vm._e()],1)}),0):_vm._e()],2)}),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"menu-buttons\">","</div>",[_c('b-button',{staticClass:"mr-3 btn-round search-btn",attrs:{"variant":"dark","size":"sm"},on:{"click":function($event){return _vm.goToSearch()}}},[_c('svg-icon',{attrs:{"name":"search"}})],1),_vm._ssrNode(" "),(false)?undefined:_vm._e()],2)],2)])],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/TheMenu.vue?vue&type=template&id=205780d4&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/TheMenu.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var TheMenuvue_type_script_lang_js_ = ({
  data() {
    return {
      isActive: false
    };
  },

  computed: {
    menu() {
      return this.options.header_menu || [];
    },

    options() {
      return this.$store.state.options || {};
    }

  },
  watch: {
    $route() {
      this.isActive = false;
    }

  },
  methods: {
    toggleClass(event) {
      if (this.isActive) {
        this.isActive = false;
      } else {
        this.isActive = true;
      }
    },

    removeClass(event) {
      this.isActive = true;
    },

    goToSearch() {
      this.$router.push('/');
      this.$nextTick(() => {
        setTimeout(() => {
          const input = document.getElementById('search');
          input.focus();
        }, 400);
        setTimeout(() => {
          const input = document.getElementById('search');
          input.focus();
        }, 1000);
      });
    }

  }
});
// CONCATENATED MODULE: ./components/TheMenu.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_TheMenuvue_type_script_lang_js_ = (TheMenuvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/TheMenu.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(198)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_TheMenuvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "7c1257ec"
  
)

/* harmony default export */ var TheMenu = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 93:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(199);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("69e61ad5", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=83.js.map