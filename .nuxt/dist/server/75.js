exports.ids = [75];
exports.modules = {

/***/ 116:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(245);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("abb59656", content, true, context)
};

/***/ }),

/***/ 244:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderProductCards_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(116);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderProductCards_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderProductCards_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderProductCards_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderProductCards_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 245:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.pagebuilder-product-cards-wrapper .card-wrapper{height:100%;border:2px solid transparent;position:relative}.pagebuilder-product-cards-wrapper .card-wrapper.bg{box-shadow:0 2px 4px 0 rgba(0,0,0,.5)}.pagebuilder-product-cards-wrapper .card-wrapper.bg-gradient{background-image:linear-gradient(289deg,#12499b 53%,#934d98);color:#fff}.pagebuilder-product-cards-wrapper .card-wrapper.bg-gradient .buttons-wrapper a:not(.btn),.pagebuilder-product-cards-wrapper .card-wrapper.bg-primary .buttons-wrapper a:not(.btn){color:#fff!important}.pagebuilder-product-cards-wrapper .card-wrapper.bg-gradient .buttons-wrapper a:not(.btn) svg,.pagebuilder-product-cards-wrapper .card-wrapper.bg-primary .buttons-wrapper a:not(.btn) svg{fill:#fff}.pagebuilder-product-cards-wrapper .card-content.margin-button{margin-bottom:7rem}.pagebuilder-product-cards-wrapper .card-content.margin-link{margin-bottom:3rem}.pagebuilder-product-cards-wrapper .buttons-wrapper{position:absolute;bottom:1.5rem;left:1.5rem;right:1.5rem}.pagebuilder-product-cards-wrapper .background-image-wrapper{width:150px;margin:0 auto;max-width:100%}.pagebuilder-product-cards-wrapper .background-image{padding-top:100%;background-size:cover;background-position:50%}.pagebuilder-product-cards-wrapper .background-image.thin{padding-top:50%}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/PagebuilderProductCards.vue?vue&type=template&id=7529a734&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"pagebuilder-product-cards-wrapper"},[_c('BaseBlockWrapper',{attrs:{"title":_vm.data.title,"sub-title":_vm.data.sub_title,"content":_vm.data.content,"button-options":{
      type: _vm.data.link_or_button,
      link: _vm.data.link,
      buttonsLabel: _vm.data.label,
      buttons: _vm.data.buttons
    },"alignment":_vm.data.text_pos}},[_c('div',{staticClass:"row"},_vm._l((_vm.data.cards),function(card,i){return _c('div',{key:i,class:['col-12 col-md-6 mb-4',{'col-xl-3' : _vm.data.cards.length > 2 }]},[_c('div',{class:['card-wrapper p-4', 'border-' + _vm.data.card_border_color, 'bg-' + _vm.data.card_background_color, {'bg': _vm.data.card_background_color !== 'transparent'}]},[_c('div',{staticClass:"background-image-wrapper"},[_c('div',{directives:[{name:"lazy",rawName:"v-lazy:background-image",value:(card.image.sizes[_vm.getImageSize('tablet')]),expression:"card.image.sizes[getImageSize('tablet')]",arg:"background-image"}],ref:"image",refInFor:true,class:['background-image', {'thin' : _vm.data.cards.length <= 2}]})]),_vm._v(" "),(card.title)?_c('h3',{ref:"title",refInFor:true},[_vm._v("\n            "+_vm._s(card.title)+"\n          ")]):_vm._e(),_vm._v(" "),_c('div',{ref:"content",refInFor:true,class:['card-content', 'margin-' + ((card.link || card.buttons)? card.link_or_button :'')],domProps:{"innerHTML":_vm._s(card.content)}}),_vm._v(" "),(card.link || card.buttons)?_c('BaseButtons',{ref:"buttons",refInFor:true,attrs:{"type":card.link_or_button,"link":card.link,"label":card.label,"buttons":_vm.addVariantToButtons(card.buttons)}}):_vm._e()],1)])}),0)])],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/PagebuilderProductCards.vue?vue&type=template&id=7529a734&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/PagebuilderProductCards.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var PagebuilderProductCardsvue_type_script_lang_js_ = ({
  components: {
    BaseBlockWrapper: () => __webpack_require__.e(/* import() */ 0).then(__webpack_require__.bind(null, 340)),
    BaseButtons: () => __webpack_require__.e(/* import() */ 1).then(__webpack_require__.bind(null, 314))
  },
  props: {
    data: {
      type: Object
    }
  }
});
// CONCATENATED MODULE: ./components/PagebuilderProductCards.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_PagebuilderProductCardsvue_type_script_lang_js_ = (PagebuilderProductCardsvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/PagebuilderProductCards.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(244)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_PagebuilderProductCardsvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "5f6c567c"
  
)

/* harmony default export */ var PagebuilderProductCards = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=75.js.map