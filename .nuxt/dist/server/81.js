exports.ids = [81];
exports.modules = {

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(70);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 153:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75,.footer-wrapper{background-color:rgba(1,11,25,.75)}.footer-wrapper{margin-top:5vmin;color:#fff;padding:6.5rem 0}.footer-wrapper h5{margin-top:0}.footer-wrapper a{color:#fff}.footer-wrapper a:hover{color:#fff!important;opacity:.6}.footer-wrapper .menu-block{margin-bottom:1.5rem}.footer-wrapper .menu-block .list-unstyled li a{padding:.5rem 0;display:block}.footer-wrapper .social .logotype{height:70px;width:150px;max-width:100%}.footer-wrapper .social .logotype svg{width:100%}.footer-wrapper .social .list-inline{max-width:150px;margin-top:2rem;display:flex}@media (max-width:1168px){.footer-wrapper .social .list-inline{max-width:100%;justify-content:center}}.footer-wrapper .social .list-inline .list-inline-item a{height:100%;display:block}.footer-wrapper .social .list-inline .list-inline-item svg{margin:8px;width:20px;height:20px;fill:#fff;transition:.4s ease-in-out}.footer-wrapper .social .list-inline .list-inline-item:hover svg{fill:#12499b}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/TheFooter.vue?vue&type=template&id=7ad552cd&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"footer-wrapper"},[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-12 col-lg-2 social\">","</div>",[_c('nuxt-link',{attrs:{"to":"/"}},[_c('svg-icon',{staticClass:"logotype",attrs:{"name":"tuc/logotype-white"}})],1),_vm._ssrNode(" <div class=\"my-3\">"+(_vm._s(_vm.options.footer.text))+"</div> "),(false)?undefined:_vm._e()],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12 col-sm-10 menus\">","</div>",[(_vm.options.footer)?_vm._ssrNode("<div class=\"row\">","</div>",_vm._l((_vm.options.footer.footer_menu),function(menuBlock){return _vm._ssrNode("<div class=\"col-12 col-lg-4 menu-block\">","</div>",[_vm._ssrNode("<h5 class=\"font-weight-bold mb-0\">"+_vm._ssrEscape("\n              "+_vm._s(menuBlock.title)+"\n            ")+"</h5> "),_vm._ssrNode("<ul class=\"list-unstyled\">","</ul>",_vm._l((menuBlock.menu_options),function(menuItem){return _vm._ssrNode("<li>","</li>",[(menuItem.menu_option && menuItem.menu_option !== '')?_vm._ssrNode("<div>","</div>",[(
                      menuItem.menu_option.url &&
                        !menuItem.menu_option.url.startsWith('http')
                    )?_c('nuxt-link',{attrs:{"to":menuItem.menu_option.url}},[_vm._v("\n                    "+_vm._s(menuItem.menu_option.title)+"\n                  ")]):_vm._e(),_vm._ssrNode(" "+((
                      menuItem.menu_option.url &&
                        menuItem.menu_option.url.startsWith('http')
                    )?("<a"+(_vm._ssrAttr("href",menuItem.menu_option.url))+" target=\"blank\">"+_vm._ssrEscape("\n                    "+_vm._s(menuItem.menu_option.title)+"\n                  ")+"</a>"):"<!---->"))],2):_vm._e(),_vm._ssrNode(" "+((!menuItem.menu_option || menuItem.menu_option === '')?("<a>MISSING_MENU_OPTION</a>"):"<!---->"))],2)}),0)],2)}),0):_vm._e()])],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/TheFooter.vue?vue&type=template&id=7ad552cd&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/TheFooter.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var TheFootervue_type_script_lang_js_ = ({
  computed: {
    options() {
      return this.$store.state.options || {};
    }

  },
  methods: {
    chunkArray(array, chunkSize) {
      let index = 0;
      const arrayLength = array.length;
      const tempArray = [];

      for (index = 0; index < arrayLength; index += chunkSize) {
        tempArray.push(array.slice(index, index + chunkSize));
      }

      return tempArray;
    }

  }
});
// CONCATENATED MODULE: ./components/TheFooter.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_TheFootervue_type_script_lang_js_ = (TheFootervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/TheFooter.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(152)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_TheFootervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "6e0370e8"
  
)

/* harmony default export */ var TheFooter = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 70:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(153);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("26342bd5", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=81.js.map