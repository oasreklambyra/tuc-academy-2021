exports.ids = [41];
exports.modules = {

/***/ 128:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(269);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("6cfa9fd7", content, true, context)
};

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCarousel_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(128);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCarousel_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCarousel_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCarousel_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCarousel_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 269:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.carousel-wrapper .carousel-item{transition:.8s ease-in-out;opacity:0;display:block}.carousel-wrapper .carousel-item .carousel-caption{position:static;padding:0}.carousel-wrapper .carousel-item .background-image{padding-top:40%;background-position:50%;background-size:cover}.carousel-wrapper .carousel-item.active{opacity:1}.carousel-wrapper .nav-wrapper{margin-top:2rem;margin-bottom:1rem;display:flex;align-items:center}@media (max-width:767.98px){.carousel-wrapper .nav-wrapper{display:block}}.carousel-wrapper .nav-wrapper .prev-wrapper svg{transform:rotate(45deg);fill:#1d1d1d}.carousel-wrapper .nav-wrapper .next-wrapper svg{transform:rotate(-45deg);fill:#1d1d1d}@media (max-width:767.98px){.carousel-wrapper .nav-wrapper .next-wrapper,.carousel-wrapper .nav-wrapper .prev-wrapper{display:inline-block;width:49%}.carousel-wrapper .nav-wrapper .next-wrapper .btn,.carousel-wrapper .nav-wrapper .prev-wrapper .btn{display:block;width:100%}}.carousel-wrapper .nav-wrapper .slide-wrapper{margin:0 1rem}@media (max-width:767.98px){.carousel-wrapper .nav-wrapper .slide-wrapper{display:none}}.carousel-wrapper .nav-wrapper .slide-wrapper .list-inline-item{border-radius:50%;width:10px;height:10px;background-color:rgba(1,11,25,.25)}.carousel-wrapper .nav-wrapper .slide-wrapper .list-inline-item.active{background-color:#12499b}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 348:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCarousel.vue?vue&type=template&id=6811849f&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:['carousel-wrapper', 'bg-' + _vm.background]},[_vm._ssrNode("<div class=\"container\">","</div>",[_c('b-carousel',{ref:"carousel",attrs:{"interval":8000,"no-animation":""},model:{value:(_vm.slideCurrent),callback:function ($$v) {_vm.slideCurrent=$$v},expression:"slideCurrent"}},_vm._l((_vm.images),function(image){return _c('b-carousel-slide',[_c('div',{directives:[{name:"lazy",rawName:"v-lazy:background-image",value:(image ? image.sizes[_vm.getImageSize('max')] : false),expression:"image ? image.sizes[getImageSize('max')] : false",arg:"background-image"}],staticClass:"background-image"},[(image.custom_fields.author)?_c('BaseAuthor',{attrs:{"author":image.custom_fields.author}}):_vm._e()],1)])}),1),_vm._ssrNode(" "),(_vm.images.length > 0)?_vm._ssrNode("<div class=\"nav-wrapper\">","</div>",[_vm._ssrNode("<div class=\"prev-wrapper\">","</div>",[_c('b-button',{attrs:{"variant":"light"}},[_c('svg-icon',{attrs:{"name":"arrow-down-left"}})],1)],1),_vm._ssrNode(" <ul class=\"slide-wrapper list-inline\">"+(_vm._ssrList((_vm.range(0, _vm.images.length - 1)),function(slide){return ("<li"+(_vm._ssrClass(null,['list-inline-item', { active: _vm.slideCurrent >= slide }]))+"></li>")}))+"</ul> "),_vm._ssrNode("<div class=\"next-wrapper\">","</div>",[_c('b-button',{attrs:{"variant":"light"}},[_c('svg-icon',{attrs:{"name":"arrow-down-right"}})],1)],1)],2):_vm._e()],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseCarousel.vue?vue&type=template&id=6811849f&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCarousel.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseCarouselvue_type_script_lang_js_ = ({
  components: {
    BaseAuthor: () => __webpack_require__.e(/* import() */ 5).then(__webpack_require__.bind(null, 353))
  },
  props: {
    background: {
      type: String
    },
    images: {
      type: Array
    }
  },

  data() {
    return {
      slideCurrent: 0
    };
  },

  methods: {
    slideSet(slide) {
      this.slideCurrent = slide;
    },

    slidePrev() {
      this.$refs.carousel.prev();
    },

    slideNext() {
      this.$refs.carousel.next();
    }

  }
});
// CONCATENATED MODULE: ./components/BaseCarousel.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseCarouselvue_type_script_lang_js_ = (BaseCarouselvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseCarousel.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(268)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseCarouselvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "1455a805"
  
)

/* harmony default export */ var BaseCarousel = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=41.js.map