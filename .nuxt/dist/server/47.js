exports.ids = [47];
exports.modules = {

/***/ 130:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(273);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("1a89f635", content, true, context)
};

/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCtaEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(130);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCtaEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCtaEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCtaEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCtaEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 273:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.cta-employee-wrapper .cta-employee-inner{position:relative;background-color:rgba(225,228,235,.25);padding:4rem}.cta-employee-wrapper .cta-employee-inner .employee-image{padding-top:110%;margin-right:3.8rem;background-position:50%;background-size:cover}.cta-employee-wrapper .cta-employee-inner .info-wrapper{position:relative}.cta-employee-wrapper .cta-employee-inner .info-wrapper h4{margin-bottom:2vmin!important}.cta-employee-wrapper .cta-employee-inner .info-wrapper .upper-info p{margin-bottom:1vmin}.cta-employee-wrapper .cta-employee-inner .lower-info{position:absolute;bottom:0;left:0}@media (max-width:576px){.cta-employee-wrapper .cta-employee-inner .lower-info{position:relative}}.cta-employee-wrapper .cta-employee-inner .job-title{color:#878787}.cta-employee-wrapper .cta-employee-inner .background-icon-wrapper{position:absolute;right:0;top:50%;transform:translateY(-50%);height:100%;min-width:375px;max-width:100%;overflow:hidden;z-index:-1}.cta-employee-wrapper .cta-employee-inner .background-icon-wrapper svg{position:absolute;right:-60%;top:50%;transform:translateY(-50%);height:160%;width:160%;fill:rgba(225,228,235,.25)}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 350:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCtaEmployee.vue?vue&type=template&id=453db53f&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.employee)?_c('div',{staticClass:"cta-employee-wrapper"},[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"cta-employee-inner\">","</div>",[_vm._ssrNode("<div class=\"row no-gutters\">","</div>",[_vm._ssrNode("<div class=\"col-12\">","</div>",[_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-12 col-sm-4\">","</div>",[_c('div',{directives:[{name:"lazy",rawName:"v-lazy:background-image",value:(_vm.employee.featured_image ? _vm.employee.featured_image[_vm.getImageSize('tablet')] : false),expression:"employee.featured_image ? employee.featured_image[getImageSize('tablet')] : false",arg:"background-image"}],staticClass:"employee-image"},[])]),_vm._ssrNode(" <div class=\"col-12 col-sm-8 info-wrapper\"><div class=\"upper-info\"><h4 class=\"font-weight-bold mb-0\">"+_vm._ssrEscape("\n                  "+_vm._s(_vm.title)+"\n                ")+"</h4> <p>"+_vm._ssrEscape("\n                  "+_vm._s(_vm.subTitle)+"\n                ")+"</p> "+((_vm.text !== '' )?("<p>"+(_vm._s(_vm.text))+"</p>"):("<p>"+(_vm._s(_vm.employee.custom_fields.text))+"</p>"))+"</div> <div class=\"lower-info\"><h5 class=\"sub-title font-weight-bold mb-0\">"+_vm._ssrEscape("\n                  "+_vm._s(_vm.employee.post_title)+"\n                ")+"</h5> <p class=\"mb-3 job-title\">"+(_vm._s(_vm.employee.custom_fields.title))+"</p> <a"+(_vm._ssrAttr("href",'tel:' + _vm.employee.custom_fields.phone))+">"+_vm._ssrEscape(_vm._s(_vm.employee.custom_fields.phone))+"</a><br> <a"+(_vm._ssrAttr("href",'mailto:' + _vm.employee.custom_fields['e-post']))+">"+_vm._ssrEscape(_vm._s(_vm.employee.custom_fields['e-post']))+"</a></div></div>")],2)])]),_vm._ssrNode(" "),(_vm.icon)?_vm._ssrNode("<div class=\"background-icon-wrapper\">","</div>",[_c('svg-icon',{staticClass:"background-icon",attrs:{"name":_vm.icon}})],1):_vm._e()],2)])]):_vm._e()}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseCtaEmployee.vue?vue&type=template&id=453db53f&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCtaEmployee.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseCtaEmployeevue_type_script_lang_js_ = ({
  props: {
    employee: {
      type: [Boolean, Object]
    },
    icon: {
      type: [Boolean, String]
    },
    title: {
      type: String
    },
    subTitle: {
      type: String
    },
    text: {
      type: String
    }
  }
});
// CONCATENATED MODULE: ./components/BaseCtaEmployee.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseCtaEmployeevue_type_script_lang_js_ = (BaseCtaEmployeevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseCtaEmployee.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(272)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseCtaEmployeevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "2881f549"
  
)

/* harmony default export */ var BaseCtaEmployee = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=47.js.map