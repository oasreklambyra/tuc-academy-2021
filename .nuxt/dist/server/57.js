exports.ids = [57];
exports.modules = {

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseSearch_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(97);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseSearch_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseSearch_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseSearch_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseSearch_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 207:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.search-wrapper{max-width:700px;margin:0 auto;z-index:700;position:relative}.search-wrapper .autocomplete-input{box-shadow:0 18px 40px -12px #1d1d1d;transition:.4s}.search-wrapper p.description{margin-top:1em}.search-wrapper input{height:100%;width:100%;border-radius:40px;padding:1rem;margin:0;border:none}@media (max-width:767.98px){.search-wrapper input{width:100%;margin-bottom:.5rem}}.search-wrapper input:focus{outline:none;transform:scale(1.05);box-shadow:0 18px 40px -12px #1d1d1d}.search-wrapper .autocomplete-result-list{left:0;list-style:none;background-color:#fff;padding:1rem;box-shadow:0 18px 40px -12px #1d1d1d;border-radius:20px;margin-top:30px;transform:scale(1.05)}.search-wrapper .autocomplete-result-list li{border-radius:4px;transition:.4s;padding:1rem;display:flex;color:#000;text-align:left;margin:.5rem 0;background-color:rgba(225,228,235,.25)}.search-wrapper .autocomplete-result-list li.divider{padding:.9rem 0 .1rem;background-color:transparent;font-weight:700}.search-wrapper .autocomplete-result-list li:hover:not(.divider){cursor:pointer;background-color:rgba(225,228,235,.5)}.search-wrapper .autocomplete-result-list li .content{width:100%}.search-wrapper .autocomplete-result-list li .content .list-title{margin-left:.9rem}.search-wrapper .autocomplete-result-list li .content .list-title .query{text-decoration:underline}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseSearch.vue?vue&type=template&id=7ab73929&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"search-wrapper"},[_c('autocomplete',{ref:"input",attrs:{"id":"search","search":_vm.searchResult,"get-result-value":_vm.getResultValue,"placeholder":_vm.options.search.placeholder,"data-expanded":"true","aria-label":"Sök Tuc sweden"},on:{"submit":_vm.onSubmit},scopedSlots:_vm._u([{key:"result",fn:function(ref){
var result = ref.result;
var props = ref.props;
return [_c('li',_vm._b({class:[
          'autocomplete-result',
          { divider: result.status === 'divider' } ]},'li',props,false),[_c('div',{staticClass:"content"},[_c('div',{class:[
              'list-title',
              { 'sub-title': result.status === 'divider' } ],domProps:{"innerHTML":_vm._s(_vm.searchResultOutput(result))}})])])]}}])}),_vm._ssrNode(" "+((_vm.options.search.description && _vm.options.search.description !== '')?("<p class=\"description\">"+(_vm._s(_vm.options.search.description))+"</p>"):"<!---->"))],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseSearch.vue?vue&type=template&id=7ab73929&

// EXTERNAL MODULE: external "@trevoreyre/autocomplete-vue"
var autocomplete_vue_ = __webpack_require__(56);
var autocomplete_vue_default = /*#__PURE__*/__webpack_require__.n(autocomplete_vue_);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseSearch.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var BaseSearchvue_type_script_lang_js_ = ({
  components: {
    Autocomplete: autocomplete_vue_default.a
  },

  data() {
    return {
      postType: {
        page: 'Sidor',
        post: 'Artiklar',
        faq: 'Vanliga frågor',
        course: 'Kurs',
        programme: 'YH-utbildningar',
        employee: 'Personal'
      }
    };
  },

  computed: {
    options() {
      return this.$store.state.options;
    },

    sitemap() {
      const res = [].concat(this.$store.state.sitemap);
      return res.sort((a, b) => a.post_title > b.post_title ? 1 : b.post_title > a.post_title ? -1 : 0);
    }

  },
  watch: {
    searchVal(val) {
      this.$router.push({
        query: {
          q: val
        }
      });
    }

  },

  mounted() {
    if (this.$route.query.q && this.$route.query.q.length > 2) {
      this.selectSearch(this.$route.query.q);
    }
  },

  methods: {
    searchResultOutput(result) {
      const label = result.post_title;
      let categories = '';

      if ((result.post_type === 'course' || result.post_type === 'programme') && result.post_taxs && result.post_taxs.programme_course_area) {
        categories += ' - ';
        const primaryCategory = result.post_taxs.programme_course_area.filter(cat => {
          return cat.primary;
        });
        const secoundaryCategories = result.post_taxs.programme_course_area.filter(cat => {
          return !cat.primary && !cat.name.includes('Satellitort');
        });
        const tetriaryCategories = result.post_taxs.programme_course_area.filter(cat => {
          return !cat.primary && cat.name.includes('Satellitort');
        });
        categories += primaryCategory.map(cat => {
          return cat.name;
        }).join(', ');

        if (secoundaryCategories.length > 0 || tetriaryCategories.length > 0) {
          categories += ' (';
          categories += secoundaryCategories.map(cat => {
            return cat.name;
          }).join(', ');

          if (secoundaryCategories.length > 0 && tetriaryCategories.length > 0) {
            categories += ', ';
          }

          categories += tetriaryCategories.map(cat => {
            return cat.name;
          }).join(', ');
          categories += ')';
        }
        /* categories += result.post_taxs.programme_course_area.map((cat) => {
          if (cat.primary) {
            return cat.name
          } else {
            return ''
          }
        }) */

      }

      return this.selectSubString(label + categories, this.searchVal);
    },

    selectSubString(string, substring) {
      if (string) {
        const re = new RegExp(substring, 'gi');
        return string.replace(re, '<span class="query">$&</span>');
      } else {
        return string;
      }
    },

    postCategory(result) {
      const res = this.postType[result.post_type] || result.post_type;
      return res;
    },

    selectSearch(val) {
      this.$refs.input.value = val;
      this.focusInput(); // this.$refs.input.search(val)
    },

    focusInput() {
      setTimeout(() => {
        this.$refs.input.$refs.input.focus();
      }, 100);
    },

    getResultValue(result) {
      if (!result.status) {
        return result.post_title;
      } else {
        return '';
      }
    },

    onSubmit(result) {
      if (result.permalink) {
        const res = result.permalink.replace(/\/$/, '').replace('https://academy.tucsweden.se', '');
        console.log(res);
        this.$router.push(res);
      }
    },

    searchResult(input) {
      input = input.toLowerCase();
      const exludedType = ['quotes', 'alumni'];

      if (input.length > 2) {
        this.searchVal = input;
        const posts = [];
        const res = [];

        if (input.length > 0) {
          this.sitemap.forEach(page => {
            if (!exludedType.includes(page.post_type)) {
              // Title
              const title = page.post_title.toLowerCase(); // type

              let type = this.postCategory(page);

              if (type !== undefined) {
                type = type.toLowerCase();
              } // excerpt


              const excerpt = page.post_excerpt.toLowerCase(); // keywords

              const keywords = page.keywords ? page.keywords.toLowerCase() : '';

              if (title.includes(input) || type.includes(input) || excerpt.includes(input) || keywords.includes(input)) {
                posts.push(page);
              }
            }
          });
        }
        /*  group result with dividers */


        if (posts.length > 0) {
          const unsortedRes = {}; // Group objects

          posts.forEach(post => {
            const postCategory = this.postCategory(post);

            if (!unsortedRes[postCategory]) {
              unsortedRes[postCategory] = [];
            }

            unsortedRes[postCategory].push(post);
          }); // Sort by biggest group

          const sortedRes = Object.keys(unsortedRes).sort(function (a, b) {
            return b.length - a.length;
          }); // divide

          let newCategory = false;
          sortedRes.forEach(postCategory => {
            const posts = unsortedRes[postCategory];
            posts.forEach(post => {
              if (postCategory !== newCategory) {
                res.push({
                  status: 'divider',
                  post_title: postCategory
                });
                newCategory = postCategory;
              }

              res.push(post);
            });
          });
        }
        /*  group result with dividers */


        if (input.length > 2) {
          return res.length > 0 ? res : [{
            status: 404,
            post_title: 'Vi hittade tyvärr inga resultat, försök med ett annat sökord. '
          }];
        } else {
          return res;
        }
      } else {
        return [];
      }
    }

  }
});
// CONCATENATED MODULE: ./components/BaseSearch.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseSearchvue_type_script_lang_js_ = (BaseSearchvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseSearch.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(206)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseSearchvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "813830e6"
  
)

/* harmony default export */ var BaseSearch = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 97:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(207);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("b76ebcb2", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=57.js.map