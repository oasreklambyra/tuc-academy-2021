exports.ids = [11];
exports.modules = {

/***/ 174:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_ThePagebuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(81);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_ThePagebuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_ThePagebuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_ThePagebuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_ThePagebuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 175:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.pagebuilder-wrapper{margin-top:-2vmin}.pagebuilder-wrapper .block-wrapper>*{padding:2vmin 0}.pagebuilder-wrapper .block-wrapper:first-child.cta{margin-top:6vmin}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/ThePagebuilder.vue?vue&type=template&id=69eff76e&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"pagebuilder-wrapper"},_vm._l((_vm.blocks),function(block,index){return _vm._ssrNode("<div"+(_vm._ssrAttr("id",block.acf_fc_layout + '-' + index))+(_vm._ssrClass(null,['block-wrapper', block.acf_fc_layout]))+">","</div>",[(block.acf_fc_layout === 'archive')?_c('PagebuilderArchive',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'post_archive')?_c('PagebuilderArchivePost',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'quote_archive')?_c('PagebuilderArchiveQuote',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'course_archive')?_c('PagebuilderArchiveCourse',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'location_archive')?_c('PagebuilderArchiveLocation',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'alumni_featured')?_c('PagebuilderFeaturedAlumni',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'course_featured')?_c('PagebuilderFeaturedCourse',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'hero')?_c('PagebuilderHero',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'cta')?_c('PagebuilderCta',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'content')?_c('PagebuilderContent',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'content_usp')?_c('PagebuilderContentUsp',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'gallery')?_c('PagebuilderGallery',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'usps')?_c('PagebuilderUsp',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'maps')?_c('PagebuilderMaps',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'contact')?_c('PagebuilderContact',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'apply')?_c('PagebuilderApply',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'media')?_c('PagebuilderMedia',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'summary')?_c('PagebuilderCourseSummary',{attrs:{"current-page":_vm.content,"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'responsible')?_c('PagebuilderResponsible',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'product_cards')?_c('PagebuilderProductCards',{attrs:{"data":block}}):_vm._e()],2)}),0)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/ThePagebuilder.vue?vue&type=template&id=69eff76e&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ThePagebuilder.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var ThePagebuildervue_type_script_lang_js_ = ({
  components: {
    PagebuilderArchive: () => __webpack_require__.e(/* import() */ 60).then(__webpack_require__.bind(null, 319)),
    PagebuilderArchivePost: () => __webpack_require__.e(/* import() */ 63).then(__webpack_require__.bind(null, 320)),
    PagebuilderArchiveQuote: () => __webpack_require__.e(/* import() */ 64).then(__webpack_require__.bind(null, 321)),
    PagebuilderArchiveCourse: () => __webpack_require__.e(/* import() */ 61).then(__webpack_require__.bind(null, 322)),
    PagebuilderArchiveLocation: () => __webpack_require__.e(/* import() */ 62).then(__webpack_require__.bind(null, 323)),
    //
    PagebuilderFeaturedAlumni: () => __webpack_require__.e(/* import() */ 70).then(__webpack_require__.bind(null, 324)),
    PagebuilderFeaturedCourse: () => __webpack_require__.e(/* import() */ 71).then(__webpack_require__.bind(null, 325)),
    //
    PagebuilderHero: () => __webpack_require__.e(/* import() */ 72).then(__webpack_require__.bind(null, 326)),
    PagebuilderCta: () => __webpack_require__.e(/* import() */ 69).then(__webpack_require__.bind(null, 327)),
    PagebuilderContent: () => __webpack_require__.e(/* import() */ 66).then(__webpack_require__.bind(null, 328)),
    PagebuilderContentUsp: () => __webpack_require__.e(/* import() */ 67).then(__webpack_require__.bind(null, 329)),
    PagebuilderGallery: () => __webpack_require__.e(/* import() */ 18).then(__webpack_require__.bind(null, 330)),
    PagebuilderUsp: () => __webpack_require__.e(/* import() */ 77).then(__webpack_require__.bind(null, 331)),
    PagebuilderMaps: () => __webpack_require__.e(/* import() */ 73).then(__webpack_require__.bind(null, 332)),
    PagebuilderContact: () => __webpack_require__.e(/* import() */ 65).then(__webpack_require__.bind(null, 333)),
    PagebuilderApply: () => __webpack_require__.e(/* import() */ 59).then(__webpack_require__.bind(null, 334)),
    PagebuilderResponsible: () => __webpack_require__.e(/* import() */ 76).then(__webpack_require__.bind(null, 335)),
    PagebuilderProductCards: () => __webpack_require__.e(/* import() */ 75).then(__webpack_require__.bind(null, 336)),
    PagebuilderMedia: () => __webpack_require__.e(/* import() */ 74).then(__webpack_require__.bind(null, 337)),
    // Course only
    PagebuilderCourseSummary: () => __webpack_require__.e(/* import() */ 68).then(__webpack_require__.bind(null, 338))
  },
  props: {
    blocks: {
      type: [Array, Boolean]
    },
    content: {
      type: Object,
      default: () => {
        return {};
      }
    }
  }
});
// CONCATENATED MODULE: ./components/ThePagebuilder.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ThePagebuildervue_type_script_lang_js_ = (ThePagebuildervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/ThePagebuilder.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(174)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ThePagebuildervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "f49b34a2"
  
)

/* harmony default export */ var ThePagebuilder = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 81:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(175);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("37782eeb", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=11.js.map