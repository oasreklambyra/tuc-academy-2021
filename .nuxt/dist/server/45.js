exports.ids = [45];
exports.modules = {

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseCategories_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(82);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseCategories_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseCategories_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseCategories_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseCourseCategories_edu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 177:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(8);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}@media (max-width:991px){.order-m-1{order:1}.order-m-2{order:2}}.category-item-wrapper{padding-top:76%;background-position:50%;background-size:auto 120%;background-repeat:no-repeat;margin-bottom:.5rem;transition:.2s ease-in-out;position:relative}.category-item-wrapper:hover{background-size:auto 100%}.category-item-wrapper:before{content:\" \";background-color:rgba(1,11,25,.5);transition:.2s ease-in-out}.category-item-wrapper .inner,.category-item-wrapper .inner a,.category-item-wrapper:before{position:absolute;top:0;left:0;right:0;bottom:0}.category-item-wrapper .inner a h4{position:absolute;top:50%;left:0;right:0;transform:translateY(-50%);font-weight:700;color:#fff;margin:0;transition:.2s ease-in-out}.course-archive-wrapper{padding:0 0 70px}.course-archive-wrapper .items-wrapper .post-course-wrapper{margin-bottom:5rem}.course-archive-wrapper .search-archive-wrapper{margin-bottom:3.7rem}.course-archive-wrapper .categories-wrapper .list-inline{border-top:1px solid rgba(160,169,186,.5);border-bottom:1px solid rgba(160,169,186,.5);padding:.7rem 0}.course-archive-wrapper .categories-wrapper .list-inline-item{cursor:pointer;margin:0;padding:1rem 1.2rem;color:#6c7175;transition:.2s}.course-archive-wrapper .categories-wrapper .list-inline-item:hover{opacity:.5}.course-archive-wrapper .categories-wrapper .list-inline-item.active{cursor:auto;color:#12499b}.course-archive-wrapper .categories-wrapper .list-inline-item.active:hover{opacity:1}.course-archive-wrapper .categories-wrapper .list-inline-item.active svg{fill:#12499b}.course-archive-wrapper .categories-wrapper .list-inline-item svg{margin-right:.5rem;width:1.5rem;height:1.5rem;vertical-align:text-top;fill:#6c7175}@media (max-width:767.98px){.course-archive-wrapper .categories-wrapper .list-inline-item{width:100%}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCourseCategories-edu.vue?vue&type=template&id=057e5089&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"course-archive-wrapper"},[(!_vm.loading)?_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-12\"><div class=\"items-wrapper\"><div class=\"row\">"+(_vm._ssrList((_vm.categories),function(item){return ("<div class=\"col-12 col-sm-4 col-md-3\"><div"+(_vm._ssrClass(null,'category-item-wrapper cat-'+item.CategoryId))+(_vm._ssrStyle(null,{ backgroundImage: 'url(' + item.ImageUrl + ')' }, null))+"><div class=\"inner text-center\"><a"+(_vm._ssrAttr("href",'/utbildning/kategori/'+item.permalink))+"><h4>"+_vm._ssrEscape(_vm._s(item.CategoryName))+"</h4></a></div></div></div>")}))+"</div></div></div>")],2):_c('BaseLoading')],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseCourseCategories-edu.vue?vue&type=template&id=057e5089&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseCourseCategories-edu.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseCourseCategories_eduvue_type_script_lang_js_ = ({
  components: {// BasePostCourse: () => import('~/components/BasePostCourse-edu.vue'),
    // BaseLoading: () => import('~/components/BaseLoading.vue')
    // BaseFilter: () => import('~/components/BaseFilter-edu.vue')
  },
  props: {},

  data() {
    return {};
  },

  computed: {
    loading() {
      let res = false; // console.log(this.programmes)

      if (this.includeCourse && !this.courses) {
        res = true;
      }

      return res;
    },

    categories() {
      let items = [];
      const res = [];
      items = items.concat(this.$store.state.bearerToken.categories.categories || []); // console.log(items)
      // const itemGroups = this.getCourseGroups(items)

      items.forEach(item => {
        let valid = true;

        if (item.ShowOnWeb === false) {
          valid = false;
        }

        if (item.ParentCategoryId !== 53911) {
          valid = false;
        }

        item.permalink = item.CategoryName.toLowerCase().replaceAll(/[&/\\#,+()$~%.'":*?<>{}]/g, '').replaceAll('å', 'a').replaceAll('ä', 'a').replaceAll('ö', 'o').replaceAll(' ', '-') + '-' + item.CategoryId;

        if (valid) {
          res.push(item);
        }
      });
      return res.sort((a, b) => a.CategoryName > b.CategoryName ? 1 : b.CategoryName > a.CategoryName ? -1 : 0);
    }

  },
  watch: {},

  mounted() {
    /*     this.$store.dispatch('bearerToken/getEvents')
    this.$store.dispatch('bearerToken/getCourseTemplates')
    this.$store.dispatch('bearerToken/getCategories') */
  }

});
// CONCATENATED MODULE: ./components/BaseCourseCategories-edu.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseCourseCategories_eduvue_type_script_lang_js_ = (BaseCourseCategories_eduvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(6);

// CONCATENATED MODULE: ./components/BaseCourseCategories-edu.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(176)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseCourseCategories_eduvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "02fe40a5"
  
)

/* harmony default export */ var BaseCourseCategories_edu = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 82:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(177);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(9).default
module.exports.__inject__ = function (context) {
  add("5f630497", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=45.js.map