(window.webpackJsonp=window.webpackJsonp||[]).push([[43],{345:function(e,t,o){var content=o(436);content.__esModule&&(content=content.default),"string"==typeof content&&(content=[[e.i,content,""]]),content.locals&&(e.exports=content.locals);(0,o(112).default)("0519bc75",content,!0,{sourceMap:!1})},435:function(e,t,o){"use strict";o(345)},436:function(e,t,o){var n=o(111)(!1);n.push([e.i,".bg-light-grey-25{background-color:rgba(225,228,235,.25)}.bg-black-75{background-color:rgba(1,11,25,.75)}.archive-employee-wrapper .header-title-wrapper{margin-bottom:3.8rem}",""]),e.exports=n},566:function(e,t,o){"use strict";o.r(t);o(18),o(66);var n={components:{BasePostEmployee:function(){return o.e(57).then(o.bind(null,586))}},props:{employees:{type:[Object,Array]},groupItems:{type:Boolean,default:!0}},computed:{employeesSortedByArea:function(){var e=[],t=this.employees,o=["administration","utbildningsledare","affarsutveckling","marknad","hr","inkop-och-ekonomi","antagning","urmakarskolan","ledning"];return Object.keys(t).forEach((function(e){o.includes(e)||o.push(e)})),o.forEach((function(o){var n=!1;t[o]&&(!t[o].area&&t[o].employees.length>0&&(n=!0),t[o].area&&Object.keys(t[o].area).forEach((function(e){t[o].area[e].employees.length&&(n=!0)}))),n&&e.push(t[o])})),e}},mounted:function(){var e=window.location.hash;console.log(e),""!==e&&setTimeout((function(){window.location.href=e}),500)}},l=(o(435),o(92)),component=Object(l.a)(n,(function(){var e=this,t=e.$createElement,o=e._self._c||t;return o("div",{staticClass:"archive-employee-wrapper"},[e.groupItems?o("div",{staticClass:"by-area"},e._l(e.employeesSortedByArea,(function(t){return o("div",{key:t.id,staticClass:"section-wrapper"},[o("div",{staticClass:"container"},[e.employeesSortedByArea.length>1?o("h2",{staticClass:"header-title-wrapper text-center"},[e._v("\n          "+e._s(t.title)+"\n        ")]):e._e(),e._v(" "),"utbildningsledare"===t.id?o("div",e._l(t.area,(function(t){return o("div",{key:t.id},[t.employees.length>0?o("div",{staticClass:"location-wrapper"},[o("h3",{staticClass:"header-title-wrapper text-center"},[e._v("\n                "+e._s(t.title)+"\n              ")]),e._v(" "),o("div",{staticClass:"row justify-content-center"},e._l(t.employees,(function(t,n){return o("div",{key:n,staticClass:"col-md-3",attrs:{id:t.post_name}},[o("BasePostEmployee",{attrs:{image:t.featured_image[e.getImageSize("tablet")],name:t.post_title,title:t.custom_fields.title,phone:t.custom_fields.phone,mail:t.custom_fields["e-post"]}})],1)})),0)]):e._e()])})),0):o("div",[o("div",{staticClass:"row justify-content-center"},e._l(t.employees,(function(t,n){return o("div",{key:n,staticClass:"col-md-3",attrs:{id:t.post_title}},[o("BasePostEmployee",{attrs:{id:t.post_name,image:t.featured_image[e.getImageSize("tablet")],name:t.post_title,title:t.custom_fields.title,phone:t.custom_fields.phone,mail:t.custom_fields["e-post"]}})],1)})),0)])])])})),0):o("div",{staticClass:"list container"},[o("div",{staticClass:"row"},e._l(e.employees,(function(t,n){return o("div",{key:n,staticClass:"col-md-3",attrs:{id:t.post_title}},[o("BasePostEmployee",{attrs:{id:t.post_name,image:t.featured_image[e.getImageSize("tablet")],name:t.post_title,title:t.custom_fields.title,phone:t.custom_fields.phone,mail:t.custom_fields["e-post"]}})],1)})),0)])])}),[],!1,null,null,null);t.default=component.exports}}]);