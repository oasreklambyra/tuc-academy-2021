import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _14d93e66 = () => interopDefault(import('../pages/i-fokus/index.vue' /* webpackChunkName: "pages/i-fokus/index" */))
const _29bb6ab6 = () => interopDefault(import('../pages/kontakta-oss.vue' /* webpackChunkName: "pages/kontakta-oss" */))
const _3fd70695 = () => interopDefault(import('../pages/vara-tjanster/index.vue' /* webpackChunkName: "pages/vara-tjanster/index" */))
const _0060d442 = () => interopDefault(import('../pages/vara-tjanster/fardiga-kurser/index.vue' /* webpackChunkName: "pages/vara-tjanster/fardiga-kurser/index" */))
const _3f01000c = () => interopDefault(import('../pages/vara-tjanster/uppdragsutbildning/index.vue' /* webpackChunkName: "pages/vara-tjanster/uppdragsutbildning/index" */))
const _d8a95df2 = () => interopDefault(import('../pages/utbildning/kategori/_kategori.vue' /* webpackChunkName: "pages/utbildning/kategori/_kategori" */))
const _8c4ebdb6 = () => interopDefault(import('../pages/vara-tjanster/foretagskurser/_course.vue' /* webpackChunkName: "pages/vara-tjanster/foretagskurser/_course" */))
const _650a8953 = () => interopDefault(import('../pages/vara-tjanster/uppdragsutbildning/_programme.vue' /* webpackChunkName: "pages/vara-tjanster/uppdragsutbildning/_programme" */))
const _703623db = () => interopDefault(import('../pages/i-fokus/_category/index.vue' /* webpackChunkName: "pages/i-fokus/_category/index" */))
const _1f1e55d6 = () => interopDefault(import('../pages/utbildning/_utbildning.vue' /* webpackChunkName: "pages/utbildning/_utbildning" */))
const _aecf699e = () => interopDefault(import('../pages/vara-tjanster/_page.vue' /* webpackChunkName: "pages/vara-tjanster/_page" */))
const _7dd1e8f4 = () => interopDefault(import('../pages/vara-tjanster/_page/index.vue' /* webpackChunkName: "pages/vara-tjanster/_page/index" */))
const _1a2bab7e = () => interopDefault(import('../pages/vara-tjanster/_page/_subPage.vue' /* webpackChunkName: "pages/vara-tjanster/_page/_subPage" */))
const _5a47a3f0 = () => interopDefault(import('../pages/i-fokus/_category/_post.vue' /* webpackChunkName: "pages/i-fokus/_category/_post" */))
const _3e16c8a0 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _4f9cb6bf = () => interopDefault(import('../pages/_page/index.vue' /* webpackChunkName: "pages/_page/index" */))
const _323914e0 = () => interopDefault(import('../pages/_page/foretagskurser.vue' /* webpackChunkName: "pages/_page/foretagskurser" */))
const _e834bbd4 = () => interopDefault(import('../pages/_page/_subPage/index.vue' /* webpackChunkName: "pages/_page/_subPage/index" */))
const _97e9d31c = () => interopDefault(import('../pages/_page/_subPage/_subSubPage.vue' /* webpackChunkName: "pages/_page/_subPage/_subSubPage" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/i-fokus/",
    component: _14d93e66,
    pathToRegexpOptions: {"strict":true},
    name: "i-fokus"
  }, {
    path: "/kontakta-oss/",
    component: _29bb6ab6,
    pathToRegexpOptions: {"strict":true},
    name: "kontakta-oss"
  }, {
    path: "/vara-tjanster/",
    component: _3fd70695,
    pathToRegexpOptions: {"strict":true},
    name: "vara-tjanster"
  }, {
    path: "/vara-tjanster/fardiga-kurser/",
    component: _0060d442,
    pathToRegexpOptions: {"strict":true},
    name: "vara-tjanster-fardiga-kurser"
  }, {
    path: "/vara-tjanster/uppdragsutbildning/",
    component: _3f01000c,
    pathToRegexpOptions: {"strict":true},
    name: "vara-tjanster-uppdragsutbildning"
  }, {
    path: "/utbildning/kategori/:kategori?/",
    component: _d8a95df2,
    pathToRegexpOptions: {"strict":true},
    name: "utbildning-kategori-kategori"
  }, {
    path: "/vara-tjanster/foretagskurser/:course/",
    component: _8c4ebdb6,
    pathToRegexpOptions: {"strict":true},
    name: "vara-tjanster-foretagskurser-course"
  }, {
    path: "/vara-tjanster/uppdragsutbildning/:programme/",
    component: _650a8953,
    pathToRegexpOptions: {"strict":true},
    name: "vara-tjanster-uppdragsutbildning-programme"
  }, {
    path: "/i-fokus/:category?/",
    component: _703623db,
    pathToRegexpOptions: {"strict":true},
    name: "i-fokus-category"
  }, {
    path: "/utbildning/:utbildning?/",
    component: _1f1e55d6,
    pathToRegexpOptions: {"strict":true},
    name: "utbildning-utbildning"
  }, {
    path: "/vara-tjanster/:page?/",
    component: _aecf699e,
    pathToRegexpOptions: {"strict":true},
    children: [{
      path: "",
      component: _7dd1e8f4,
      pathToRegexpOptions: {"strict":true},
      name: "vara-tjanster-page"
    }, {
      path: ":subPage/",
      component: _1a2bab7e,
      pathToRegexpOptions: {"strict":true},
      name: "vara-tjanster-page-subPage"
    }]
  }, {
    path: "/i-fokus/:category?/:post/",
    component: _5a47a3f0,
    pathToRegexpOptions: {"strict":true},
    name: "i-fokus-category-post"
  }, {
    path: "/",
    component: _3e16c8a0,
    pathToRegexpOptions: {"strict":true},
    name: "index"
  }, {
    path: "/:page/",
    component: _4f9cb6bf,
    pathToRegexpOptions: {"strict":true},
    name: "page"
  }, {
    path: "/:page/foretagskurser/",
    component: _323914e0,
    pathToRegexpOptions: {"strict":true},
    name: "page-foretagskurser"
  }, {
    path: "/:page/:subPage/",
    component: _e834bbd4,
    pathToRegexpOptions: {"strict":true},
    name: "page-subPage"
  }, {
    path: "/:page/:subPage/:subSubPage/",
    component: _97e9d31c,
    pathToRegexpOptions: {"strict":true},
    name: "page-subPage-subSubPage"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
