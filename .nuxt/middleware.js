const middleware = {}

middleware['bearer-token'] = require('../middleware/bearer-token.js')
middleware['bearer-token'] = middleware['bearer-token'].default || middleware['bearer-token']

middleware['getMeta'] = require('../middleware/getMeta.js')
middleware['getMeta'] = middleware['getMeta'].default || middleware['getMeta']

middleware['getPage'] = require('../middleware/getPage.js')
middleware['getPage'] = middleware['getPage'].default || middleware['getPage']

middleware['getSupportData'] = require('../middleware/getSupportData.js')
middleware['getSupportData'] = middleware['getSupportData'].default || middleware['getSupportData']

export default middleware
