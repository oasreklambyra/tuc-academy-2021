const axios = require('axios')

module.exports = {
  mode: 'universal',
  telemetry: false,
  debug: true,

  server: {
    host: '0.0.0.0'
  },

  /*
   ** Headers of the page
   */
  head: {
    title: '',
    htmlAttrs: {
      lang: 'sv-SE'
    },
    meta: [
      {name: 'facebook-domain-verification', content: 'e7o6w3dxvbtnzqghglikdg2f16m2hs'},
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/static/favicon.jpg' },
      { rel: 'stylesheet', href: 'https://use.typekit.net/zve0qfl.css' }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: '~/components/BaseLoadingPage.vue',
  /*
   ** Global CSS
   */
  css: ['@assets/bootstrap/bootstrap-config.scss', '@assets/scss/fonts.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/vue-lazyload.js' },
    { src: '~/plugins/globalFunctions.js' },

  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    [
      '@nuxtjs/gtm',
      {
        id: 'GTM-MFDTJHJ',
        autoInit: false,
        pageTracking: true
      }
    ]
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    [
      '@nuxtjs/redirect-module',
      [
        { from: '^/yrkeshogskola/larplattform', to: '/ny-studerande/larplattform/' },
        { from: '^/om-tuc/kontakt', to: '/kontakta-oss' },

        { from: '^/wp-admin', to: 'https://wordpress.tucacademy.se/wp-admin' },
        { from: '^/yrkeshogskola/vara-utbildningar/(.*)$', to: '/utbildning/yh-utbildningar/$1' },
        { from: '^/yrkeshogskola/(.*)$', to: '/utbildning/$1' },
        { from: '^/korta-flexibla/(.*)$', to: '/utbildning/korta-flexibla-kurser/$1' },
        { from: '^/utbildning/korta-flexibla-kurser/(.*)$', to: '/utbildning/kurser-och-kurspaket/$1' },
        { from: '^/nyheter/(.*)$', to: '/artiklar/senaste-nytt/$1' },
        { from: '^/repotage/(.*)$', to: '/artiklar/alumni/$1' }
      ]
    ],
    [
      'bootstrap-vue/nuxt',
      {
        bootstrapCSS: false,
        bootstrapVueCSS: false,
        components: ['BButton', 'BFormInput', 'BFormCheckboxGroup', 'BModal', 'BBadge', 'BTabs', 'BTab', 'BFormInvalidFeedback', 'BCarousel', 'BCarouselSlide', 'BBadge'],
        directives: ['BTooltip']
      }
    ],
    'cookie-universal-nuxt',
    '@nuxtjs/axios',
    '@nuxt/http',
    '@nuxtjs/proxy',
    '@nuxtjs/svg-sprite',
    '@nuxtjs/style-resources',
    [
      '@nuxtjs/sitemap',
      {
        gzip: true,

        async routes () {
          const res = []

          const pages = await axios.get(
            'https://wordpress.tucacademy.se/wp-json/core/sitemap'
          )

          pages.data.forEach((page) => {
            if (page.post_type !== 'alumni' && page.post_type !== 'quotes' && page.post_type !== 'employee') {
              res.push('/' + page.permalink)
            }
          })

          return res
        }
      }
    ],
    [
      '@nuxtjs/robots',
      [
        {
          UserAgent: '*',
          Allow: '/'
        }
      ]
    ]
  ],
  http: {
    debug: false,
    proxy: true,
    retry: true,
    serverTimeout: 50,
  },
  proxy: [
    [ 'https://api.eduadmin.se/v1', { ws: false } ],
    [ 'https://api.eduadmin.se/token', { ws: false } ]

  ],
  styleResources: {
    scss: ['./assets/scss/variables.scss']
  },
  middleware: 'bearer-token',
  router: {
    middleware: ['getMeta', 'getPage', 'getSupportData', 'bearer-token'],
    trailingSlash: true
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    transpile: ['gsap'],

    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          options: 'fix',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'))

      svgRule.test = /\.(png|jpe?g|gif|webp)$/

      config.module.rules.push({
        test: /\.svg$/,
        use: ['babel-loader', 'vue-svg-loader']
      })
    }

    /* extractCSS: true,
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true
          }
        }
      }
    }
    */
  }
}
