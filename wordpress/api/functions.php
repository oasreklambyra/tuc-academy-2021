<?php
/*
 * Plugin Name: Vue api
 * Version: 1.0
 * Description: 
 * Author: Pierre Norrbrink, Web developer OAS
 * Author URI: https://OAS.nu/
 *
 */

require_once 'wordpress-options.php';
require_once 'acf.php';
require_once 'rest-api.php';
require_once 'cache.php';