<?php

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
    acf_add_options_sub_page('Header');
    acf_add_options_sub_page('Footer');

    acf_add_options_sub_page('Other');
}


function wprc_add_acf_posts_endpoint( $allowed_endpoints ) {
    if ( !isset( $allowed_endpoints[ 'core' ] )) {

        $allowed_endpoints['core'] = array();

        $endpoints = array('page', 'cpt', 'options', 'sitemap', 'contactform');

        foreach ($endpoints as $endpoint) {
            if(!in_array( $endpoint, $allowed_endpoints[ 'core' ] ) ) {
                 $allowed_endpoints[ 'core' ][] = $endpoint;
            }
        }
    } 

    if ( !isset( $allowed_endpoints[ 'yoast/v1' ] )) {

        $allowed_endpoints['yoast/v1'] = array();

        $endpoints = array('get_head');

        foreach ($endpoints as $endpoint) {
            if(!in_array( $endpoint, $allowed_endpoints[ 'yoast/v1' ] ) ) {
                 $allowed_endpoints[ 'yoast/v1' ][] = $endpoint;
            }
        }
    } 

    return $allowed_endpoints;
}

add_filter( 'wp_rest_cache/allowed_endpoints', 'wprc_add_acf_posts_endpoint', 10, 1);


function wprc_uncach_parameters( $uncached_parameters ) {
    $uncached_parameters[] = 'preview';
    return $uncached_parameters;
}
add_filter( 'wp_rest_cache/uncached_parameters', 'wprc_uncach_parameters', 10, 1);


add_filter('acf/load_value/key=field_5f29124b03632', 'default_course_layout', 10, 3);
  function  default_course_layout($value, $post_id, $field) {
    if ($value !== NULL) {
      // $value will only be NULL on a new post
      return $value;
    }
    // add default layouts
    $value = array(
        array(
            'acf_fc_layout' => 'summary', 
            'field_5f2949493dc58' => array(
            array(
                'field_5f2949f53dc5a' => 'Poäng',
                'field_5f2949fd3dc5b' => ''
            ),
            array(
                'field_5f2949f53dc5a' => 'Antal platser',
                'field_5f2949fd3dc5b' => ''
            ),
            array(
                'field_5f2949f53dc5a' => 'Utbildningsort',
                'field_5f2949fd3dc5b' => ''
            ),
            array(
                'field_5f2949f53dc5a' => 'Kommande utbildningsstart',
                'field_5f2949fd3dc5b' => ''
            ), array(
                'field_5f2949f53dc5a' => 'Sista ansökningsdag',
                'field_5f2949fd3dc5b' => ''
            ), array(
                'field_5f2949f53dc5a' => 'Pågående utbildning',
                'field_5f2949fd3dc5b' => ''
            ), 
            array(
                'field_5f2949f53dc5a' => 'Utbildningen finns även i',
                'field_5f2949fd3dc5b' => ''
            )
        )
      ),
      array(
        'acf_fc_layout' => 'content',
        'field_5f29486fbd1a1' => ''
      ),
      array(
        'acf_fc_layout' => 'responsible'
        )

    );
    return $value;
  }


add_filter('acf/fields/relationship/result', 'acf_fields_relationship_result', 10, 4);
function acf_fields_relationship_result( $text, $post, $field, $post_id ) {
    
    $term_obj_list = get_the_terms( $post->ID, 'programme_course_area' );

    if($term_obj_list) {
        $terms_string = join(', ', wp_list_pluck($term_obj_list, 'name')); 
        
        $text = $text . ' - ' . $terms_string;   
    }
    

    return $text;
}

add_filter('acf/fields/flexible_content/layout_title', 'add_hash_to_title', 10, 4);
function add_hash_to_title( $title, $field, $layout, $i ) {

    //var_dump($field);

    $layout_name = $field['value'][$i]['acf_fc_layout'];

    $layout_hash = '#' . $layout_name . '-' . $i;

    $layout_hash_link = '<a target="_blank" href="' . get_permalink() . $layout_hash . '">' . $layout_hash . '</a>';

    return $title . ' - ' . $layout_hash_link;
}


?>